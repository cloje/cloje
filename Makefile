default: version update-exports test

install:
	@ echo "Which host did you want to install Cloje on?"
	@ echo "CHICKEN:  make -C chicken install"
	@ echo "Racket:   make -C racket install"

test:
	$(MAKE) -C racket test
	$(MAKE) -C chicken test

clean:
	$(MAKE) -C chicken clean
	$(MAKE) -C racket clean

dist-clean:
	$(MAKE) -C chicken dist-clean
	$(MAKE) -C racket dist-clean

demo: demos
.PHONY: demos
demos:
	@ ($(MAKE) -C chicken fail-if-not-installed 2> /dev/null \
	     && $(MAKE) -C chicken demos) \
	  || (echo "Skipping CHICKEN demos because Cloje is not installed there." \
	        && echo "Install with: make -C chicken install" \
	        && echo)
	@ ($(MAKE) -C racket fail-if-not-installed 2> /dev/null \
	     && $(MAKE) -C racket demos) \
	  || (echo "Skipping Racket demos because Cloje is not installed there." \
	        && echo "Install with: make -C racket install" \
	        && echo)


update-exports:
	@ cd scripts && \
	  csi -e "(use srfi-1 srfi-13)" -s ./extract_exports.scm \
	  > ../docs/exports.md
	@ echo "Updated docs/exports.md"

version:
	@ echo Cloje `$(MAKE) version-number`

version-number:
	@ cd scripts && csi -s ./print_version.scm
