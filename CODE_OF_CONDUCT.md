# Cloje Contributor Code of Conduct

As contributors and maintainers of this project, and in the interest
of fostering an open and welcoming community, we pledge to respect all
people who contribute through reporting issues, posting feature
requests, updating documentation, submitting merge requests or
patches, and other activities.

We are committed to making participation in this project a
harassment-free experience for everyone, regardless of level of
experience, gender, gender identity and expression, sexual
orientation, disability, personal appearance, body size, race,
ethnicity, age, religion, or nationality.

Examples of unacceptable behavior by participants include:

- The use of sexualized language or imagery
- Personal attacks
- Trolling or insulting/derogatory comments
- Public or private harassment
- Publishing other's private information, such as physical or
  electronic addresses, without explicit permission
- Other unethical or unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit,
or reject comments, commits, code, wiki edits, issues, and other
contributions that are not aligned to this Code of Conduct. By
adopting this Code of Conduct, project maintainers commit themselves
to fairly and consistently applying these principles to every aspect
of managing this project. Project maintainers who do not follow or
enforce the Code of Conduct may be permanently removed from the
project team.

This code of conduct applies both within project spaces and in public
spaces when an individual is representing the project or its
community.

Instances of abusive, harassing, or otherwise unacceptable behavior
may be reported by contacting a project moderator:

- John Croisant: john+cloje@croisant.net

As a matter of policy, the identities of people who report code of
conduct violations will be kept confidential.

(The above portion of this Code of Conduct is adapted from the
[Contributor Covenant](http://contributor-covenant.org), version
1.2.0, available at http://contributor-covenant.org/version/1/2/0/)


## Technology-centered group rivalry

Technology-centered group rivalry is inappropriate for this project.
This includes rivalries about programming languages, paradigms,
editors, operating systems, etc. Such rivalries distract from
constructive conversations, create a hostile atmosphere (which impedes
learning and collaboration), and alienate community members.

Examples of this kind of behavior include:

- Heated arguments about which technology is superior
- Derogatory remarks about a technology or its users/community
- Pushily trying to "convert" people to a technology.

Comments of this kind may be edited or removed from project spaces.
Individuals who repeatedly or egregiously engage in this kind of
behavior in project spaces, or in public spaces when representing the
project or its community, may be suspended from participation in the
project.

It is okay to describe differences between technologies or offer
constructive criticism. However, such comments should be factual,
specific, and relevant to accomplishing the goals of this project.


## Encouraged behaviors

In the interest of fostering a healthy and inclusive community, and a
positive learning environment for people at many different levels of
experience, project participants are **encouraged** to:

- Ask questions, especially if something is unclear or confusing. You
  are probably not the only person wondering the same thing, and the
  project will end up better (or at least no worse) because you asked.

- Reach out for help when you need it. This is a collaborative
  project, not an endurance competition. There is no reason to suffer
  alone.

- Collaborate with other participants. Together you will be able to
  tackle greater challenges, and find better solutions. Plus, more
  people will learn and gain experience from it.

- Focus on tasks at an appropriate challenge level for you. Leave some
  low-hanging fruit for less experienced participants to work on, so
  they can learn and grow.

- Offer (without pressuring) to mentor or advise people who have less
  or different experience than you. (But remember that the point is to
  *help them* learn and grow, not to show off how much you know.)

- Communicate in a sincere and open manner, and indicate how you feel
  using words or emoticons/emoji. This helps compensate for the
  limitations of written communication, and helps avoid
  miscommunication and misunderstandings.
