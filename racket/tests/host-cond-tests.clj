
(deftest test-host-cond
  (is (test/syntax-error? (host-cond)))
  (is (nil? (host-cond (chicken (/ 1 0)) (else nil))))
  (is (nil? (host-cond (java (/ 1 0)) (else nil))))

  (is (= 'something-else
         (host-cond
          (chicken 'chicken)
          (java 'java)
          (else 'something-else))))

  (is (true? (host-cond (racket true))))
  (is (true? (host-cond (racket true)
                        (chicken (/ 1 0)))))
  (is (true? (host-cond (chicken (/ 1 0))
                        (racket true))))

  (is (nil? (host-cond (racket) (scheme 'scheme))))

  (with-out-str
    (is (= 3 (host-cond (racket (pr 1) (pr 2) 3)))))
  (is (= "12" (with-out-str
                (host-cond (racket (pr 1) (pr 2) 3)))))

  (is (= 'racket (host-cond (racket 'racket)
                            (scheme 'scheme))))
  (is (= 'scheme (host-cond (scheme 'scheme)
                            (racket 'racket))))

  (is (true? (host-cond ((or foo racket) true))))

  (is (true? (host-cond ((and scheme racket) true))))
  (is (nil? (host-cond ((and foo racket) true) (else nil))))

  (is (true? (host-cond ((not foo) true))))

  (is (true? (host-cond ((or foo (and scheme (not chicken))) true)))))
