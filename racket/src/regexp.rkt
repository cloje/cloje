;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn re-pattern [s]
  (if (string? s)
      (host/regexp s)
      (_raise-type-error 're-pattern "string?" s)))


(def re-pattern? host/regexp?)


(host/struct _re-matcher
  (pattern input (start #:mutable) (last-match #:mutable))
  #:constructor-name _make-re-matcher
  #:reflection-name 're-matcher
  #:omit-define-syntaxes)


(def re-matcher? _re-matcher?)


(host/struct _re-match
  (ranges input)
  #:constructor-name _make-re-match
  #:reflection-name 're-match
  #:omit-define-syntaxes)


(defn _re-match-num-groups [match]
  (count (_re-match-ranges match)))

(defn _re-matcher-next-start [matcher match]
  (scheme/cdr (first (_re-match-ranges match))))


(defn _re-find [pattern input start]
  (if-let [ranges (host/regexp-match-positions pattern input start)]
    (_make-re-match ranges input)
    nil))


(defn _re-groups [last-match]
  (let [input (_re-match-input last-match)]
    (scheme/list->vector
     (map (fn [range]
            (if range
              (subs input (scheme/car range) (scheme/cdr range))
              nil))
          (_re-match-ranges last-match)))))


(defn _re-matches [pattern input]
  (if-let [match (_re-find pattern input 0)]
    (let [range (first (_re-match-ranges match))]
      (if (and range
               (= (scheme/car range) 0)
               (= (scheme/cdr range) (scheme/string-length input)))
        match
        nil))
    nil))
