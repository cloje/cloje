;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(define-syntax _raise-syntax-error
  (syntax-rules ()
    ((_raise-syntax-error fn-name message)
     (host/raise-syntax-error fn-name message))))


(scheme/define _arity-at-least host/arity-at-least)
(scheme/define _normalize-arity host/normalize-arity)

(define-syntax _raise-arity-error
  (syntax-rules ()
    ((_raise-arity-error fn-name expected-arity args)
     (host/raise-arity-error fn-name expected-arity args))))


(define-syntax _raise-type-error
  (syntax-rules ()
    ((_raise-type-error fn-name expectation actual)
     ((host/raise-argument-error fn-name expectation actual)))))


(define-syntax _raise-bounds-error
  (syntax-rules ()
    ((_raise-bounds-error fn-name type-desc index-prefix index
                          coll lower upper alt-lower)
     (host/raise-range-error fn-name type-desc index-prefix index
                             coll lower upper alt-lower))))


(define-syntax _raise-generic-error
  (syntax-rules ()
    ((_raise-generic-error fn-name message)
     ((host/error (_format "Error in ~a:~n  ~a" fn-name message))))))


(define-syntax _catch-errors
  (syntax-rules ()
    ((_catch-errors ((err-var) error-body ...) body ...)
     (host/with-handlers ([host/exn:fail?
                           (lambda (err-var) error-body ...)])
       body ...))))
