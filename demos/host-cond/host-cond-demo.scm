;;;
;;; This file demonstrates workarounds for using host-specific syntax,
;;; as described in docs/host-cond.md.
;;;
;;; Usage:
;;;
;;; CHICKEN (compiled):
;;;   csc -R cloje ./host-cond-demo.scm   # compile
;;;   ./host-cond-demo                    # run
;;;
;;; CHICKEN (interpreted):
;;;   csi -R cloje -s ./host-cond-demo.scm
;;;
;;; Racket:
;;;   racket -e "(require cloje)" -r ./host-cond-demo.scm
;;;


(host-cond
 (chicken
  (printf "You are using CHICKEN~n"))
 (racket
  (displayln "You are using Racket"))
 (scheme
  (display "You are using a Scheme")
  (newline))
 (else
  (println "You are using some other host language")))


;;; Helper macro. Usage: (eval-str "foo")
(host-cond
 (scheme
  (define-syntax eval-str
    (syntax-rules ()
      ((eval-str s)
       (eval (with-in-str s (read))))))))


(host-cond
 (chicken
  (println "Workaround 1: Avoid host-specific syntax")
  (prn (blob->string (string->blob "cloje")))
  (println "Workaround 2: Read/eval a string")
  (prn (blob->string (eval (with-in-str "#${636c6f6a65}" (read)))))
  (println "Workaround 2 (using eval-str helper)")
  (prn (blob->string (eval-str "#${636c6f6a65}")))
  (println "Workaround 3: Load a file")
  (load "chicken-specific-code.scm"))

 (racket
  (println "Workaround 1: Avoid host-specific syntax")
  (prn (regexp-match (regexp "cloje") "cloje"))
  (println "Workaround 2: Read/eval a string")
  (prn
   (regexp-match (eval (with-in-str "#rx\"cloje\"" (read))) "cloje"))
  (println "Workaround 2 (using eval-str helper)")
  (prn (regexp-match (eval-str "#rx\"cloje\"") "cloje"))
  (println "Workaround 3: Load a file")
  (load "racket-specific-code.rkt"))

 (else
  (println "You are using some other host language.")))
