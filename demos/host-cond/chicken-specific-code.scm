;;; This is a file that uses CHICKEN-specific syntax extensions. It is
;;; loaded from host-cond-demo.shared.scm to demonstrate a workaround
;;; for using host-specific syntax via host-cond.

;;; This is needed if the file is loaded from a compiled executable
;;; and cloje features (like prn) are used.
(use cloje)

#| Multiline
 | block
 | comment |#

#<<END
This is a string constant that spans
multiple lines and has "quotes" in it.
END

(prn (blob->string #${636c6f6a65}))
