# Cloje

A clone of Clojure built atop Scheme/Lisp.

- Version: 0.3.0-dev
- Homepage: https://gitlab.com/cloje/cloje
- Issues: https://gitlab.com/cloje/cloje/issues
- Blog/News: http://blog.jacius.info/tag/cloje/
- Author: John Croisant (john+cloje at croisant dot net)

Cloje is copyright &copy; 2015 John Croisant.

Cloje's design is inspired by and based on that of Clojure, which was
created by Rich Hickey et al. Cloje's code was entirely written from
scratch, guided by Clojure's public documentation and observable
run-time behavior. None of Clojure's source code has been copied,
studied, or consulted at any point in the creation of Cloje.

Cloje is free software offered under the terms of the
Eclipse Public License (v 1.0). See
[LICENSE.txt](https://gitlab.com/cloje/cloje/blob/main/LICENSE.txt)
for details.


## Goals and Rationale

The primary goal of the Cloje software is to bring the best aspects of
Clojure to Scheme/Lisp.

Cloje will be a layer of data structures, functions, macros, and
reader syntax on top of Scheme/Lisp. Importing all of Cloje will allow
you to write code in a language very similar to Clojure. Or if you
prefer, you will be able to pick and choose which pieces of Cloje to
import, so that you are still mostly writing code in Scheme/Lisp, but
with some added features from Clojure.

By bringing the best aspects of Clojure to Scheme/Lisp, we can have
the desirable features of Clojure, while leveraging the portability
and individual strengths of the many available Scheme/Lisp
implementations.

Plus, it'll be fun and we'll learn a lot!

For more information about Cloje's goals, rationale, philosophy, and
development approach, see
[docs/goals.md](https://gitlab.com/cloje/cloje/blob/main/docs/goals.md)


## Status

- As of July 2015, Cloje is not yet considered ready for "serious
  use". It only implements a small subset of Clojure, and not much
  effort has been put into optimizing performance yet. But, it works,
  and you can play around with it if you like.

- Cloje is considered API-unstable until version 1.0 is released. That
  means functions, macros, types, etc. may be changed or removed with
  no advance warning or deprecation period.

- Cloje currently supports [CHICKEN](http://call-cc.org/) and
  [Racket](http://racket-lang.org/). Other Schemes and Lisps
  ([such as Common Lisp](https://gitlab.com/cloje/cloje/issues/39))
  may be supported in the future, if there are volunteers.

- For a list of which functions, macros, and variables have been
  implemented in Cloje so far, see
  [docs/exports.md](https://gitlab.com/cloje/cloje/blob/main/docs/exports.md).

- There are some known differences between Cloje and Clojure. See
  [docs/discrepancies.md](https://gitlab.com/cloje/cloje/blob/main/docs/discrepancies.md)
  for details.

- Cloje has some extra functionality compared to Clojure. See
  [docs/additions.md](https://gitlab.com/cloje/cloje/blob/main/docs/additions.md)
  for details.

- For information about what changed in each release of Cloje, see
  [CHANGES.md](https://gitlab.com/cloje/cloje/blob/main/CHANGES.md)

- For information about current plans for the future, see the
  [project milestones](https://gitlab.com/cloje/cloje/milestones)


## Helping Out / Contributing

There are many ways to help out with Cloje. Some are easy, some are
hard. Some require a lot of experience, some require little. All
contributions are valuable and appreciated. For more information about
ways you can contribute, please see
[CONTRIBUTING.md](https://gitlab.com/cloje/cloje/blob/main/CONTRIBUTING.md).

In order to foster a healthy, safe, and inclusive community and
learning environment, all participants in this project are expected to
abide by the Cloje Contributor Code of Conduct, described in
[CODE_OF_CONDUCT.md](https://gitlab.com/cloje/cloje/blob/main/CODE_OF_CONDUCT.md).


## Installation From Source

(More detailed installation instructions are
[planned for the future](https://gitlab.com/cloje/cloje/issues/54). In
the meantime, please email a project maintainer if you run into
trouble with these instructions.)

(All commands in this section assume you are in a terminal window in
the main `cloje` directory.)

- CHICKEN: `make -C chicken install`
- Racket: `make -C racket install`
    - Note: You may see many messages during installation like
      "load-handler: expected a \`module' declaration for \`...' in
      #\<path:...\>, but found something else". These warnings don't
      necessarily mean installation failed. To test whether cloje was
      installed, run `racket -l cloje` or run a demo (described
      below). If no errors are printed, cloje was installed
      successfully. (The warnings will be addressed in the future.)

### Running Tests

- All host languages: `make test`
- CHICKEN: `make -C chicken test`
- Racket: `make -C racket test`

### Dependencies

On CHICKEN, Cloje depends on several eggs. They will be installed for
you automatically if you follow the installation instructions above.
If you wish to develop or play with Cloje without installing it, you
will need to install the dependencies: `make -C chicken install-deps`
(This will also build Cloje, but not install it. It's the easiest way
to install the dependencies.)

On Racket, Cloje currently has no dependencies other than standard
libraries that come with Racket.


## Usage

### Launching a REPL (Interactive Command Line)

If you just want to play around with Cloje, you can launch a REPL.
Installation of Cloje is not necessary, but you may need to install
some dependencies, described above.

- CHICKEN: `make -C chicken repl`
    -  Then type `(import cloje)` as the first command.
- Racket: `make -C racket repl`

If you have [rlwrap](http://utopia.knoware.nl/~hlub/rlwrap/)
installed, you can launch a REPL with a command history and
line-editing:

- CHICKEN: `make -C chicken rlrepl`
    - The command line will be pre-populated with `(import cloje)`, so
      you only need to press Enter.
- Racket: `make -C racket rlrepl`

### Real Usage

#### CHICKEN

After you have installed Cloje (see above), you can use it in your
code. For example, create a file named `demo.scm` with these contents:

```clojure
(use cloje)

(defn add-one [x] (+ 1 x))
(println "1 + 2 =" (add-one 2))
```

You can then run it in the CHICKEN interpreter: `csi -s ./demo.scm`

Or compile it and run it: `csc ./demo.scm && ./demo`

#### Racket

After you have installed Cloje (see above), you can use it in your
code. For example, create a file named `demo.rkt` with these contents:

```clojure
(require cloje)

(defn add-one [x] (+ 1 x))
(println "1 + 2 =" (add-one 2))
```

You can then run it in the Racket interpreter: `racket -r ./demo.rkt`
