
;;; CHICKEN usage:
;;;   csi -s ./print_version.scm
;;;
;;; Racket usage:
;;;   racket -r ./print_version.scm

(define version-file-path "../shared/src/version.clj")

(define-syntax def
  (syntax-rules ()
    ((def more ...)
     (define more ...))))

;;; NOTE: this only reads/evals the first expression of the file,
;;; which will look like:
;;;    (def _cloje-version (list 1 2 3))
;;; There may or may not be a string as the fourth list item.
(eval (with-input-from-file version-file-path read))


(display (list-ref _cloje-version 0))
(display ".")
(display (list-ref _cloje-version 1))
(display ".")
(display (list-ref _cloje-version 2))

(if (> (length _cloje-version) 3)
    (begin
      (display "-")
      (display (list-ref _cloje-version 3))))

(newline)
