
;;; CHICKEN usage:
;;;   csi -e "(use srfi-1 srfi-13)" -s ./extract_exports.scm
;;;
;;; Racket usage:
;;;   racket -e "(require srfi/1 srfi/13)" -r ./extract_exports.scm


(define (exports-from-file file)
  (let* ((module-definition (with-input-from-file file read))
         (export-forms (filter (lambda (form)
                                 (and (list? form)
                                      (not (null? form))
                                      (or (eq? 'export (car form))
                                          (eq? 'provide (car form)))))
                               module-definition))
         (export-syms (apply append (map cdr export-forms))))
    (sort (remove hidden-export? (map symbol->string export-syms))
          string<)))


(define (hidden-export? str)
  (or (string-prefix? "_" str)
      (string-prefix? "*_" str)))


(define exports
  (list (cons "CHICKEN"
              (exports-from-file "../chicken/cloje.scm"))
        (cons "Racket"
              (exports-from-file "../racket/main.rkt"))))


(define all-exports
  (sort (apply lset-union string= (map cdr exports))
        string<))


(define missing-exports
  (map (lambda (export)
         (cons (car export)
               (lset-difference string= all-exports (cdr export))))
       exports))


(define (print-preface)
  (display "This is an automatically-generated list of functions, macros,
and variables that are exported/provided by Cloje. Some items in this
list may only be partially implemented, or may have discrepancies.
Be sure to check
  [docs/discrepancies.md](https://gitlab.com/cloje/cloje/blob/main/docs/discrepancies.md)
for details.")
  (newline))


(define (print-exports-list exports)
  (for-each
   (lambda (export)
     (newline)
     (display "- `") (display export) (display "`"))
   exports))


(define (print-all-exports)
  (newline)
  (display "## Exports")
  (display " (") (display (length all-exports)) (display ")")
  (newline)
  (print-exports-list all-exports)
  (newline))


(define (print-missing-exports)
  (for-each
   (lambda (export-group)
     (let ((host (car export-group))
           (missing (cdr export-group)))
       (when (not (null? missing))
         (newline)
         (display "## Not available on ") (display host)
         (display " (") (display (length missing)) (display ")")
         (newline)
         (print-exports-list missing)
         (newline))))
   missing-exports))


(define (print-exports)
  (print-preface)
  (print-all-exports)
  (print-missing-exports)
  (newline))


(print-exports)
