# Cloje Release History

## Cloje 0.2.0

Release date: 2015-07-07

Cloje 0.2.0 introduces a changed project goal; the hash set data
structure; set operations based on clojure.set; and a number of minor
additions and improvements.

### Changed project goal

Cloje was originally intended to be as faithful and complete a clone
of Clojure as was possible. However, due to some issues that make
Clojure very difficult to clone faithfully, the goal has changed. The
new goal is "to bring the best aspects of Clojure to Scheme/Lisp".

For information about the reasons for this change, and details about
Cloje's goals, rationale, philosophy, and development approach, see
[docs/goals.md](https://gitlab.com/cloje/cloje/blob/main/docs/goals.md)

### Hash set data structure

Cloje 0.2 adds the hash set data structure, an unordered collection of
distinct elements.

- Cloned `hash-set`, which constructs a new hash set containing the
  given elements.
- Cloned `set`, which converts a collection to a (hash) set.
- Cloned `set?`, which returns true if the object is a (hash) set.
- Updated many existing functions, such as generic collection
  operations, to work with hash sets.

### Set operations

Cloje 0.2 adds a number of set operations, based on the clojure.set
standard library. However, unlike their counterparts in clojure.set,
these functions are defined to work with arguments of any seqable
collection type (list, vector, map, set, string), sequence type, or
nil.

- Cloned `set/union`, which returns a hash set containing all distinct
  elements from the given collections.
- Cloned `set/difference`, which returns a hash set containing the
  elements that exist in the first collection but not in any of the
  other collections.
- Cloned `set/intersection`, which returns a hash set containing the
  elements that exist in all of the given collections.
- Cloned `set/select`, which returns a hash set containing the
  elements of the given collection that pass the predicate.
- Cloned `set/subset?`, which returns true if every element in the
  first collection also exists in the second collection.
- Cloned `set/superset?`, which returns true if every element in the
  second collection also exists in the first collection.

### Other changes

- Added `host-boolean`, which implements Cloje's boolean conversion
  logic in terms of the host language's boolean type.
- Cloned `hash`, which computes a hash value for the given object.
  This is used in hash maps and hash sets.
- Hash maps now use `hash` and `=` (from Cloje) to distinguish between
  keys. Before, they used functions provided by the host language.
  This change is needed to support using hash sets (and other new
  types in the future) as keys.
- Cloned `reduce`, a fundamental operation for processing collections.



## Cloje 0.1.0

Release date: 2015-05-03

Cloje 0.1.0 is the first release of Cloje. It provides a variety of
functions and macros cloned from Clojure; a few functions similar to
Java methods commonly used in Clojure code; and some extra functions,
macros, and aliases novel to Cloje.

### Cloned from Clojure

Cloje 0.1 provides the following functions and macros, cloned from
Clojure. Refer to the Clojure docs for more information about each.

Some behaviors differ from Clojure. See
[docs/discrepancies.md](https://gitlab.com/cloje/cloje/blob/main/docs/discrepancies.md)
for details.

- Defining top-level variables: `def`

- Functions: `defn`, `fn`, `fn?`

- Flow control: `let`, `if`, `if-not`, `if-let`, `when`, `when-not`,
  `cond`, `condp`, `case`, `do`, `comment`

- Equality: `=`, `not=`, `identical?`

- Boolean logic: `true`, `true?`, `false`, `false?`, `nil`, `nil?`,
  `boolean` `and`, `or`, `not`

- Math:
  - `+`, `-`, `*`, `/`, `inc`, `dec`, `quot`, `rem`, `mod`, `max`,
    `min`
  - `<`, `<=`, `>=`, `>`
  - `rand`, `rand-int`
  - `pos?`, `neg?`, `zero?`, `even?`, `odd?`, `number?`, `integer?`,
    `float?`

- Symbols and keywords: `symbol`, `symbol?`, `gensym`, `keyword`,
  `keyword?`

- Generic collections:
  - `coll?`, `counted?`, `sequential?`, `sorted?`, `reversible?`
  - `assoc`, `count`, `distinct?`, `empty`, `empty?`, `every?`, `get`,
    `not-any?`, `not-empty`, `not-every?`, `some`, `contains?`

- Generic sequences:
  - `first`, `next`, `rest`, `last`, `nth`, `ffirst`,
    `fnext`, `nfirst`, `nnext`
  - `apply`, `conj`, `cons`, `into`, `map`, `peek`, `pop`

- Seqs: `seq`, `seq?`
- Lists: `list`, `list*`, `list?`
- Vectors: `vector`, `vector?`, `subvec`
- Hash maps: `hash-map`, `map?`

- Strings:
  - `str`, `string?`, `char`, `char?`, `subs`
  - Like clojure.string: `string/blank?`,
    `string/upper-case`, `string/lower-case`, `string/capitalize`,
    `string/join`, `string/reverse`, `string/re-quote-replacement`,
    `string/replace`, `string/replace-first`,
    `string/trim`, `string/triml`, `string/trimr`,
    `string/trim-newline`

- Input/Output:
  - `newline`, `print`, `print-str`, `println`, `println-str`, `pr`,
    `pr-str`, `prn`, `prn-str`
  - `with-in-str`, `with-out-str`

- Regular expressions (regexps): `re-pattern`, `re-matcher`,
  `re-find`, `re-groups`, `re-matches`, `re-seq`

- Testing framework (like clojure.test): `test/deftest`,
  `test/testing`, `test/is`, `test/run-all-tests`, `test/success?`

### Cloned from Java

Cloje 0.1 provides a few functions intended to emulate Java methods
commonly used in Clojure code.

- `.contains` (note: **not** an alias for `contains?`)
- `.indexOf` (aka `index-of`), `.lastIndexOf` (aka `last-index-of`)
- `.startsWith` (aka `string/starts-with?`), `.endsWith` (aka
  `string/ends-with?`)

### Extra functionality

Cloje 0.1 provides the following extra functionality beyond what
exists in Clojure. See
[docs/additions.md](https://gitlab.com/cloje/cloje/blob/main/docs/additions.md)
for details.

- `cloje-version`, `*cloje-version*`
- `host-cond` (see [docs/host-cond.md](https://gitlab.com/cloje/cloje/blob/main/docs/host-cond.md))
- `index-of` (aka `.indexOf`), `last-index-of` (aka `.lastIndexOf`)
- `string/index-of`, `string/last-index-of`
- `string/starts-with?` (aka `.startsWith`),
  `string/ends-with?` (aka `.endsWith`)
- `seqable?`
- `re-pattern?`, `re-matcher?`
