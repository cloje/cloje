
(deftest test-char?
  (is (true? (char? #\c)))
  (is (true? (char? #\1)))

  (is (false? (char? "cloje")))
  (is (false? (char? "")))
  (is (false? (char? true)))
  (is (false? (char? false)))
  (is (false? (char? nil)))
  (is (false? (char? 1)))
  (is (false? (char? 'cloje))))



(deftest test-string?
  (is (true? (string? "cloje")))
  (is (true? (string? "")))

  (is (false? (string? #\c)))
  (is (false? (string? true)))
  (is (false? (string? false)))
  (is (false? (string? nil)))
  (is (false? (string? 1)))
  (is (false? (string? 'cloje))))



(deftest test-char
  ;; TODO: More thorough way of testing? This merely establishes that
  ;; we're probably using UTF-8.
  (let [examples
        (hash-map
         9   "\t"  10 "\n"  32 " "  33  "!"
         47  "/"   48 "0"   57 "9"  58  ":"   64 "@"
         65  "A"   66 "B"   89 "Y"  90  "Z"   91 "["   96 "`"
         97  "a"   98 "b"  121 "y"  122 "z"  123 "{"  126 "~"
         199 "Ç"  321 "Ł"  214 "Ö"  308 "Ĵ"  201 "É"  923 "Λ"
         231 "ç"  322 "ł"  246 "ö"  309 "ĵ"  233 "é"  955 "λ")]
    ;; TODO: use doseq instead of map
    (map (fn [kv]
           (let [x (first kv)  s (fnext kv)  c (first s)]
             (is (char? (char x))
                 (str c " " (list x)))
             (is (= c (char x))
                 (str c " " (list x)))))
         examples))

  (is (test/arity-error? (char)))
  (is (test/arity-error? (char 0 1)))

  (is (test/error? (char -1)))
  (is (char? (char 0)))
  (is (char? (char 65535)))
  (is (test/error? (char 65536)))

  (is (test/type-error? (char "a")))
  (is (test/type-error? (char 'a)))
  (is (test/type-error? (char true)))
  (is (test/type-error? (char false)))
  (is (test/type-error? (char nil)))

  (is (= (char 65) (char (char 65)))
      "should accept and return chars as-is")

  ;; Sigh, Clojure.
  (is (= (char 65) (char 65.0)))
  (is (= (char 65) (char 65.9)))
  (is (= (char 0) (char 0.0)))
  (is (= (char 0) (char 0.5)))
  (is (test/error? (char -0.1)))
  (is (= (char 65535) (char 65535.9)))
  (is (test/error? (char 65536.0))))



(deftest test-str
  (is (= "" (str)))
  (is (= "" (str nil)))
  (is (= "a" (str 'a)))
  (is (= "a" (str "a")))
  (is (= "1" (str 1)))
  (is (= "a1b2" (str 'a 1 "b" 2)))
  (is (= "(a)(1 2)" (str '(a) '(1 2)))))



(deftest test-subs
  (is (test/arity-error? (subs)))
  (is (test/arity-error? (subs "cloje")))
  (is (test/arity-error? (subs "cloje" 1 2 3)))

  (is (= "cloje" (subs "cloje" 0)))
  (is (= "cloje" (subs "cloje" 0 5)))
  (is (= "cloj" (subs "cloje" 0 4)))
  (is (= "loje" (subs "cloje" 1)))
  (is (= "loje" (subs "cloje" 1 5)))
  (is (= "loj" (subs "cloje" 1 4)))
  (is (= "" (subs "cloje" 5)))

  (is (= "λ" (subs "λ!" 0 1)))
  (is (= "!" (subs "λ!" 1)))

  (is (= "" (subs "" 0)))
  (is (test/bounds-error? (subs "" 1)))

  (is (test/bounds-error? (subs "cloje" 6)))
  (is (test/bounds-error? (subs "cloje" 0 6)))
  (is (test/bounds-error? (subs "cloje" -1 5))))



(deftest test-string/blank?
  (is (true? (string/blank? nil)))
  (is (true? (string/blank? "")))
  (is (true? (string/blank? " ")))
  (is (true? (string/blank? " \t\n\r")))

  (is (false? (string/blank? "a")))
  (is (false? (string/blank? " a ")))
  (is (false? (string/blank? " \t\n\ra \t\n\r")))

  (is (test/type-error? (string/blank? true)))
  (is (test/type-error? (string/blank? false)))
  (is (test/type-error? (string/blank? #\space)))
  (is (test/type-error? (string/blank? 'a)))
  (is (test/type-error? (string/blank? '#:a)))
  (is (test/type-error? (string/blank? 1))))



(deftest test-string/capitalize
  (is (= "Cloje is cool!" (string/capitalize "ClOjE iS cOoL!")))
  (is (= "Çłöĵé" (string/capitalize "çłöĵé")))
  (is (= "Λλ" (string/capitalize "λλ")))

  (is (= "Cloje" (string/capitalize 'ClOjE)))
  (is (= "(cloje is cool)" (string/capitalize '(ClOjE Is CoOl))))
  (is (= (string/capitalize (str '#:ClOjE))
         (string/capitalize '#:ClOjE)))

  (is (= "1" (string/capitalize 1)))
  (is (= (string/capitalize (str true))
         (string/capitalize true)))
  (is (= (string/capitalize (str false))
         (string/capitalize false)))

  ;; Sigh, Clojure
  (is (test/type-error? (string/capitalize nil)))

  (is (test/arity-error? (string/capitalize)))
  (is (test/arity-error? (string/capitalize "too" "many"))))



(deftest test-string/index-of
  (is (= 0 (string/index-of "" "")))
  (is (= 0 (string/index-of "foo" "foo")))
  (is (= 0 (string/index-of "foo" "")))
  (is (= 0 (string/index-of "foo" "f")))
  (is (= 1 (string/index-of "foo" "o")))
  (is (= 1 (string/index-of "foo" "oo")))

  (is (= -1 (string/index-of "foo" "of")))
  (is (= -1 (string/index-of "foo" "bar")))
  (is (= -1 (string/index-of "" "bar")))

  (testing "with from-index"
    (is (=  2 (string/index-of "bananas" "na" -2)))
    (is (=  2 (string/index-of "bananas" "na" -1)))
    (is (=  2 (string/index-of "bananas" "na" 0)))
    (is (=  2 (string/index-of "bananas" "na" 1)))
    (is (=  2 (string/index-of "bananas" "na" 2)))
    (is (=  4 (string/index-of "bananas" "na" 3)))
    (is (=  4 (string/index-of "bananas" "na" 4)))
    (is (= -1 (string/index-of "bananas" "na" 5)))
    (is (= -1 (string/index-of "bananas" "na" 6)))
    (is (= -1 (string/index-of "bananas" "na" 7)))

    (is (=  0 (string/index-of "bananas" "" -1)))
    (is (=  0 (string/index-of "bananas" "" 0)))
    (is (=  1 (string/index-of "bananas" "" 1))))

  (is (test/arity-error? (string/index-of)))
  (is (test/arity-error? (string/index-of "foo")))
  (is (test/arity-error? (string/index-of "too" "many" "args" "here")))

  (is (test/type-error? (string/index-of "foo" (seq "f"))))
  (is (test/type-error? (string/index-of "foo" #\f)))
  (is (test/type-error? (string/index-of "foo" 'f)))
  (is (test/type-error? (string/index-of "foo" nil)))

  (is (test/type-error? (string/index-of (seq "f") "foo")))
  (is (test/type-error? (string/index-of #\f "foo")))
  (is (test/type-error? (string/index-of 'f "foo")))
  (is (test/type-error? (string/index-of nil "foo")))

  (is (test/type-error? (string/index-of "foo" "o" "1")))
  (is (test/type-error? (string/index-of "foo" "o" 1.0))))



(deftest test-string/last-index-of
  (is (= 0 (string/last-index-of "" "")))
  (is (= 0 (string/last-index-of "foo" "foo")))
  (is (= 3 (string/last-index-of "foo" "")))
  (is (= 0 (string/last-index-of "foo" "f")))
  (is (= 2 (string/last-index-of "foo" "o")))
  (is (= 1 (string/last-index-of "foo" "oo")))

  (is (= -1 (string/last-index-of "foo" "of")))
  (is (= -1 (string/last-index-of "foo" "bar")))
  (is (= -1 (string/last-index-of "" "bar")))

  (testing "with from-index"
    (is (= -1 (string/last-index-of "bananas" "na" 0)))
    (is (= -1 (string/last-index-of "bananas" "na" 1)))
    (is (=  2 (string/last-index-of "bananas" "na" 2)))
    (is (=  2 (string/last-index-of "bananas" "na" 3)))
    (is (=  4 (string/last-index-of "bananas" "na" 4)))
    (is (=  4 (string/last-index-of "bananas" "na" 5)))
    (is (=  4 (string/last-index-of "bananas" "na" 6)))
    (is (=  4 (string/last-index-of "bananas" "na" 7)))
    (is (=  4 (string/last-index-of "bananas" "na" 8)))

    (is (=  6 (string/last-index-of "bananas" "" 6)))
    (is (=  7 (string/last-index-of "bananas" "" 7)))
    (is (=  7 (string/last-index-of "bananas" "" 8))))

  (is (test/arity-error? (string/last-index-of)))
  (is (test/arity-error? (string/last-index-of "foo")))
  (is (test/arity-error? (string/last-index-of "too" "many" "args" "here")))

  (is (test/type-error? (string/last-index-of "foo" (seq "f"))))
  (is (test/type-error? (string/last-index-of "foo" #\f)))
  (is (test/type-error? (string/last-index-of "foo" 'f)))
  (is (test/type-error? (string/last-index-of "foo" nil)))

  (is (test/type-error? (string/last-index-of (seq "f") "foo")))
  (is (test/type-error? (string/last-index-of #\f "foo")))
  (is (test/type-error? (string/last-index-of 'f "foo")))
  (is (test/type-error? (string/last-index-of nil "foo")))

  (is (test/type-error? (string/last-index-of "foo" "o" "1")))
  (is (test/type-error? (string/last-index-of "foo" "o" 1.0))))



(deftest test-string/join
  (is (= "a1b2" (string/join '("a" 1 "b" 2))))
  (is (= "a1b2" (string/join (vector "a" 1 "b" 2))))
  (is (= "a1b2" (string/join "a1b2")))
  (let [s (string/join (hash-map "a" 1 "b" 2))]
    (is (or (= s (string/join (list (vector "a" 1)
                                    (vector "b" 2))))
            (= s (string/join (list (vector "b" 2)
                                    (vector "a" 1)))))))

  (is (= "a, 1, b, 2" (string/join ", " '("a" 1 "b" 2))))
  (is (= "a, 1, b, 2" (string/join ", " (vector "a" 1 "b" 2))))
  (is (= "a, 1, b, 2" (string/join ", " "a1b2")))
  (let [s (string/join ", " (hash-map "a" 1 "b" 2))]
    (is (or (= s (string/join ", " (list (vector "a" 1)
                                         (vector "b" 2))))
            (= s (string/join ", " (list (vector "b" 2)
                                         (vector "a" 1)))))))

  (is (= "a1b1c" (string/join 1 '(a b c))))
  (is (= "a b c" (string/join #\space '(a b c))))
  (is (= "abc" (string/join nil '(a b c))))
  (is (= "afoobfooc" (string/join 'foo '(a b c))))

  (is (= "" (string/join nil)))
  (is (= "" (string/join ", " nil)))

  (is (test/arity-error? (string/join)))
  (is (test/arity-error? (string/join "too" "many" "args")))

  (is (test/type-error? (string/join 1)))
  (is (test/type-error? (string/join 1 2))))



(deftest test-string/lower-case
  (is (= "cloje!" (string/lower-case "ClOjE!")))
  (is (= "λ¡çłöĵé!λ" (string/lower-case "Λ¡ÇŁÖĴÉ!Λ")))

  (is (= "cloje" (string/lower-case 'ClOjE)))
  (is (= "(cloje is cool)" (string/lower-case '(ClOjE Is CoOl))))
  (is (= (string/lower-case (str '#:ClOjE))
         (string/lower-case '#:ClOjE)))

  (is (= "1" (string/lower-case 1)))
  (is (= (string/lower-case (str true))
         (string/lower-case true)))
  (is (= (string/lower-case (str false))
         (string/lower-case false)))

  ;; Sigh, Clojure
  (is (test/type-error? (string/lower-case nil)))

  (is (test/arity-error? (string/lower-case)))
  (is (test/arity-error? (string/lower-case "too" "many"))))



(deftest test-string/reverse
  (is (= "" (string/reverse "")))
  (is (= "ejolc" (string/reverse "cloje")))

  (is (test/arity-error? (string/reverse)))
  (is (test/arity-error? (string/reverse "foo" "bar")))

  (is (test/type-error? (string/reverse (seq "sigh, Clojure"))))
  (is (test/type-error? (string/reverse nil)))
  (is (test/type-error? (string/reverse 'foo)))
  (is (test/type-error? (string/reverse '#:foo))))



(deftest test-string/starts-with?
  (is (identical? string/starts-with? .startsWith))

  (testing "without offset"
   (is (true? (string/starts-with? "" "")))
   (is (true? (string/starts-with? "cloje" "")))
   (is (true? (string/starts-with? "cloje" "c")))
   (is (true? (string/starts-with? "cloje" "clo")))
   (is (true? (string/starts-with? "cloje" "cloje")))

   (is (false? (string/starts-with? "cloje" "l")))
   (is (false? (string/starts-with? "cloje" "lo")))
   (is (false? (string/starts-with? "cloje" "loje")))

   (is (false? (string/starts-with? "cloje" "z")))
   (is (false? (string/starts-with? "" "z"))))

  (testing "with offset"
    (is (true? (string/starts-with? "" "" 0)))
    (is (true? (string/starts-with? "cloje" "" 0)))
    (is (true? (string/starts-with? "cloje" "c" 0)))
    (is (true? (string/starts-with? "cloje" "clo" 0)))
    (is (true? (string/starts-with? "cloje" "cloje" 0)))

    (is (true? (string/starts-with? "cloje" "l" 1)))
    (is (true? (string/starts-with? "cloje" "lo" 1)))
    (is (true? (string/starts-with? "cloje" "loje" 1)))

    (is (false? (string/starts-with? "cloje" "c" 1)))
    (is (false? (string/starts-with? "cloje" "clo" 1)))
    (is (false? (string/starts-with? "cloje" "cloje" 1)))

    (is (false? (string/starts-with? "cloje" "z" 0)))
    (is (false? (string/starts-with? "" "z" 0)))
    (is (false? (string/starts-with? "" "" 1)))

    (is (false? (string/starts-with? "abc" "" -1)))
    (is (true?  (string/starts-with? "abc" ""  0)))
    (is (true?  (string/starts-with? "abc" ""  1)))
    (is (true?  (string/starts-with? "abc" ""  2)))
    (is (true?  (string/starts-with? "abc" ""  3)))
    (is (false? (string/starts-with? "abc" ""  4)))

    ;; Sigh, Clojure.
    (is (true? (string/starts-with? "cloje" "c" 0.9)))
    (is (true? (string/starts-with? "cloje" "l" 1.0)))
    (is (true? (string/starts-with? "cloje" "l" 1.9))))

  (is (test/arity-error? (string/starts-with?)))
  (is (test/arity-error? (string/starts-with? "a")))
  (is (test/arity-error? (string/starts-with? "a" "b" 1 "foo")))

  (is (test/type-error? (string/starts-with? "cloje" 'c)))
  (is (test/type-error? (string/starts-with? "cloje" #\c)))
  (is (test/type-error? (string/starts-with? "cloje" nil)))

  (is (test/type-error? (string/starts-with? 'cloje "c")))
  (is (test/type-error? (string/starts-with? #\c "c")))
  (is (test/type-error? (string/starts-with? nil "c")))

  (is (test/type-error? (string/starts-with? "cloje" "c" nil)))
  (is (test/type-error? (string/starts-with? "cloje" "c" "")))
  (is (test/type-error? (string/starts-with? "cloje" "c" "0"))))



(deftest test-string/ends-with?
  (is (identical? string/ends-with? .endsWith))

  (testing "without offset"
   (is (true? (string/ends-with? "" "")))
   (is (true? (string/ends-with? "cloje" "")))
   (is (true? (string/ends-with? "cloje" "e")))
   (is (true? (string/ends-with? "cloje" "oje")))
   (is (true? (string/ends-with? "cloje" "cloje")))

   (is (false? (string/ends-with? "cloje" "j")))
   (is (false? (string/ends-with? "cloje" "oj")))

   (is (false? (string/ends-with? "cloje" "z")))
   (is (false? (string/ends-with? "" "z"))))

  (is (test/arity-error? (string/ends-with?)))
  (is (test/arity-error? (string/ends-with? "foo")))
  (is (test/arity-error? (string/ends-with? "too" "many" "args")))

  (is (test/type-error? (string/ends-with? "cloje" 'c)))
  (is (test/type-error? (string/ends-with? "cloje" #\c)))
  (is (test/type-error? (string/ends-with? "cloje" nil)))

  (is (test/type-error? (string/ends-with? 'cloje "c")))
  (is (test/type-error? (string/ends-with? #\c "c")))
  (is (test/type-error? (string/ends-with? nil "c"))))



(deftest test-string/trim
  (is (= "" (string/trim "")))
  (is (= "" (string/trim " ")))
  (is (= "" (string/trim "\t")))
  (is (= "" (string/trim " \t ")))

  (is (= "cloje" (string/trim "cloje")))
  (is (= "cloje" (string/trim " cloje ")))
  (is (= "cloje" (string/trim "\tcloje\t")))
  (is (= "cloje" (string/trim " \tcloje\t ")))
  (is (= "cloje" (string/trim "\n\r\t cloje \t\r\n")))

  (is (test/type-error? (string/trim nil)))
  (is (test/type-error? (string/trim true)))
  (is (test/type-error? (string/trim false)))
  (is (test/type-error? (string/trim 'a)))
  (is (test/type-error? (string/trim '#:a)))
  (is (test/type-error? (string/trim 1))))



(deftest test-string/triml
  (is (= "" (string/triml "")))
  (is (= "" (string/triml " ")))
  (is (= "" (string/triml "\t")))
  (is (= "" (string/triml " \t ")))

  (is (= "cloje" (string/triml "cloje")))
  (is (= "cloje " (string/triml " cloje ")))
  (is (= "cloje\t" (string/triml "\tcloje\t")))
  (is (= "cloje\t " (string/triml " \tcloje\t ")))
  (is (= "cloje \t\r\n" (string/triml "\n\r\t cloje \t\r\n")))

  (is (test/type-error? (string/triml nil)))
  (is (test/type-error? (string/triml true)))
  (is (test/type-error? (string/triml false)))
  (is (test/type-error? (string/triml 'a)))
  (is (test/type-error? (string/triml '#:a)))
  (is (test/type-error? (string/triml 1))))



(deftest test-string/trimr
  (is (= "" (string/trimr "")))
  (is (= "" (string/trimr " ")))
  (is (= "" (string/trimr "\t")))
  (is (= "" (string/trimr " \t ")))

  (is (= "cloje" (string/trimr "cloje")))
  (is (= " cloje" (string/trimr " cloje ")))
  (is (= "\tcloje" (string/trimr "\tcloje\t")))
  (is (= " \tcloje" (string/trimr " \tcloje\t ")))
  (is (= "\n\r\t cloje" (string/trimr "\n\r\t cloje \t\r\n")))

  (is (test/type-error? (string/trimr nil)))
  (is (test/type-error? (string/trimr true)))
  (is (test/type-error? (string/trimr false)))
  (is (test/type-error? (string/trimr 'a)))
  (is (test/type-error? (string/trimr '#:a)))
  (is (test/type-error? (string/trimr 1))))



(deftest test-string/trim-newline
  (is (= "cloje" (string/trim-newline "cloje\n")))
  (is (= "cloje" (string/trim-newline "cloje\r")))
  (is (= "cloje" (string/trim-newline "cloje\n\r")))
  (is (= "cloje" (string/trim-newline "cloje\n\r\n\r\n\r")))

  (is (= "\n\rcloje" (string/trim-newline "\n\rcloje")))
  (is (= "\n\rcloje" (string/trim-newline "\n\rcloje\n\r")))

  (testing "doesn't trim tabs or spaces"
    (is (= "cloje\n " (string/trim-newline "cloje\n \n")))
    (is (= "cloje\n\t" (string/trim-newline "cloje\n\t\n")))
    (is (= "cloje" (string/trim-newline "cloje")))
    (is (= " cloje " (string/trim-newline " cloje ")))
    (is (= "\tcloje\t" (string/trim-newline "\tcloje\t")))
    (is (= " \tcloje\t " (string/trim-newline " \tcloje\t "))))

  (is (test/type-error? (string/trim-newline nil)))
  (is (test/type-error? (string/trim-newline true)))
  (is (test/type-error? (string/trim-newline false)))
  (is (test/type-error? (string/trim-newline 'a)))
  (is (test/type-error? (string/trim-newline '#:a)))
  (is (test/type-error? (string/trim-newline 1))))



(deftest test-string/upper-case
  (is (= "CLOJE!" (string/upper-case "ClOjE!")))
  (is (= "Λ¡ÇŁÖĴÉ!Λ" (string/upper-case "λ¡çłöĵé!λ")))

  (is (= "CLOJE" (string/upper-case 'ClOjE)))
  (is (= "(CLOJE IS COOL)" (string/upper-case '(ClOjE Is CoOl))))
  (is (= (string/upper-case (str '#:ClOjE))
         (string/upper-case '#:ClOjE)))

  (is (= "1" (string/upper-case 1)))
  (is (= (string/upper-case (str true))
         (string/upper-case true)))
  (is (= (string/upper-case (str false))
         (string/upper-case false)))

  ;; Sigh, Clojure
  (is (test/type-error? (string/upper-case nil)))

  (is (test/arity-error? (string/upper-case)))
  (is (test/arity-error? (string/upper-case "too" "many"))))
