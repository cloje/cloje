
(deftest test-comment
  (is (nil? (comment)))
  (is (nil? (comment (/ 1 0))))
  (is (nil? (comment 1 2 3 4 5 6 7 8 9 10))))


(deftest test-let
  (is (nil? (let [])))
  (is (nil? (let [a 1])))
  (is (= 2 (let [a 1] (+ a 1))))
  (is (= 3 (let [a 1 b 2] (+ a b))))
  (is (= 4 (let [a 1 b (+ a 1) c (+ b 2)] c)))
  (is (nil? (let [a 1 b (+ a 1)])))

  (is (test/syntax-error? (let [a] a)))
  (is (test/syntax-error? (let [a 1 b] a))))


(deftest test-do
  (is (nil? (do)))
  (is (= 'e (do 'a 'b 'c 'd 'e)))
  (let [side-effect false]
    (is (= 'c (do (scheme/set! side-effect true) 'a 'b 'c)))
    (is (= side-effect true))))


(deftest test-if
  (is (= 'a (if (< 1 2) 'a)))
  (is (nil? (if (> 1 2) 'a)))

  (is (= 'a (if (< 1 2) 'a 'b)))
  (is (= 'b (if (> 1 2) 'a 'b)))

  (is (test/arity-error? (if)))
  (is (test/arity-error? (if true)))
  (is (test/arity-error? (if true 'a 'b 'c))))


(deftest test-if-not
  (is (= 'a (if-not (> 1 2) 'a)))
  (is (nil? (if-not (< 1 2) 'a)))

  (is (= 'a (if-not (> 1 2) 'a 'b)))
  (is (= 'b (if-not (< 1 2) 'a 'b)))

  (is (test/arity-error? (if-not)))
  (is (test/arity-error? (if-not true)))
  (is (test/arity-error? (if-not true 'a 'b 'c))))


(deftest test-when
  (let [side-effect false]
    (is (= 'c (when (= 1 1) (scheme/set! side-effect true) 'a 'b 'c)))
    (is (= side-effect true)))

  (let [side-effect false]
    (is (nil? (when (= 1 2) (scheme/set! side-effect true) 'a 'b 'c)))
    (is (= side-effect false)))

  (is (nil? (when (= 1 1))))
  (is (nil? (when (= 1 2))))

  (is (test/arity-error? (when))))


(deftest test-when-not
  (let [side-effect false]
    (is (= 'c (when-not (= 1 2) (scheme/set! side-effect true) 'a 'b 'c)))
    (is (= side-effect true)))

  (let [side-effect false]
    (is (nil? (when-not (= 1 1) (scheme/set! side-effect true) 'a 'b 'c)))
    (is (= side-effect false)))

  (is (nil? (when-not (= 1 2))))
  (is (nil? (when-not (= 1 1))))

  (is (test/arity-error? (when-not))))


(deftest test-cond
  (is (nil? (cond)))
  (is (= 'a (cond (= 1 1) 'a)))
  (is (= 'a (cond (= 1 1) 'a (= 2 2) 'b)))
  (is (= 'b (cond (= 1 2) 'a (= 2 2) 'b)))
  (is (= 'b (cond (= 1 2) 'a (= 2 2) 'b)))
  (is (= 'b (cond (= 1 2) 'a (= 2 2) 'b 'else 'c)))
  (is (= 'c (cond (= 1 2) 'a (= 2 3) 'b 'else 'c)))
  (is (= 'c (cond 'else 'c)))
  (is (= 'c (cond 'else 'c (= 1 1) 'a)))

  (is (test/syntax-error? (cond true)))
  (is (test/syntax-error? (cond true 'a true)))
  (is (test/syntax-error? (cond true 'a true 'b true))))


(deftest test-condp
  (let [f (fn [n]
            (condp < n
              10 "< 10"
              20 "< 20"
              ">= 20"))]
    (is (= "< 10" (f 9)))
    (is (= "< 20" (f 10)))
    (is (= "< 20" (f 19)))
    (is (= ">= 20" (f 20))))

  (let [test-fn (fn [a b]
                  (if (>= (* a b) 100)
                    (* a b)))
        f2 (fn [n]
             (condp test-fn n
               10 '#:>> (fn [x] (list 10 x))
               20 '#:>> (fn [x] (list 20 x))
               42))]
    (is (= '(10 110) (f2 11)))
    (is (= '(10 100) (f2 10)))
    (is (= '(20 180) (f2 9)))
    (is (= 42 (f2 0)))))


(deftest test-case
  (let [f (fn [e]
              (case e
                1 'a
                bee 'b
                (3 4 5) 'c
                ((6 7)) 'd
                'z))]
    (is (= 'a (f 1)))
    (is (= 'b (f 'bee)))
    (is (= 'c (f 3)))
    (is (= 'c (f 4)))
    (is (= 'c (f 5)))
    (is (= 'd (f '(6 7))))
    (is (= 'z (f 6)))
    (is (= 'z (f 7)))
    (is (= 'z (f "foo"))))

  (is (= 'a (case 1 'a)))
  (is (= 'a (case 1 1 'a)))

  (is (test/arity-error? (case)))
  (is (test/error? (case 1)))
  (is (test/error? (case 2 1 'a))))


(deftest test-if-let
  (is (= 3 (if-let [x 2] (+ 1 x) -1)))
  (is (= -1 (if-let [x false] (+ 1 x) -1)))
  (is (nil? (if-let [x false] (+ 1 x))))

  (is (test/syntax-error? (if-let [x 1 y 2] 1 2))) ; too many binding forms
  (is (test/arity-error? (if-let [x 1])))
  (is (test/arity-error? (if-let)))

  (testing "YOVO (You Only eValuate Once)"
    (is (= "evaluated"
           (with-out-str
             (if-let [x (or (pr 'evaluated) true)]
               (list x x)
               nil))))))
