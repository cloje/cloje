
(define truthy-examples
  (list
   true #t
   'a 'true 't 'f
   '#:a '#:true '#:t '#:false '#:f '#:nil
   "a" "true" "t" "false" "f" "nil" ""
   -1 0 1 2
   -1.0 -0.5 -0.0 0.0 0.5 1.0 2.0))

(define falsey-examples
  (list
   nil   ;; 'nil
   false ;; 'false
   #f))



(deftest test-boolean
  (testing "truthy values return true"
    (scheme/for-each
     (fn [example]
       (is (true? (boolean example))
           (host/format "(true? (boolean ~s))" example)))
     truthy-examples))
  (testing "falsey values return false"
    (scheme/for-each
     (fn [example]
       (is (false? (boolean example))
           (host/format "(false? (boolean ~s))" example)))
     falsey-examples)))



(deftest test-host-boolean
  (testing "truthy values return #t"
    (scheme/for-each
     (fn [example]
       (is (identical? #t (host-boolean example))
           (host/format "(identical? #t ~a)" example)))
     truthy-examples))
  (testing "falsey values return #f"
    (scheme/for-each
     (fn [example]
       (is (identical? #f (host-boolean example))
           (host/format "(identical? #f ~a)" example)))
     falsey-examples)))



(deftest test-not
  (testing "truthy values return false"
    (scheme/for-each
     (fn [example]
       (is (false? (not example))
           (host/format "(false? (not ~s))" example)))
     truthy-examples))
  (testing "falsey values return true"
    (scheme/for-each
     (fn [example]
       (is (true? (not example))
           (host/format "(true? (not ~s))" example)))
     falsey-examples)))



(deftest test-and
  (is (true? (and)))

  (is (true? (and true)))
  (is (true? (and 42 true)))
  (is (= 42 (and 42)))
  (is (= 42 (and true 42)))

  (is (false? (and false)))
  (is (false? (and false true)))
  (is (false? (and true false)))
  (is (false? (and true false nil)))
  (is (false? (and true false 42)))

  (is (nil? (and nil)))
  (is (nil? (and nil true)))
  (is (nil? (and true nil)))
  (is (nil? (and true nil false)))
  (is (nil? (and true nil 42)))

  (is (nil? (and 42 nil (/ 1 0)))
      "(and) should short-circuit on first falsey found"))



(deftest test-or
  (is (nil? (or)))

  (is (true? (or true)))
  (is (true? (or true 42)))
  (is (= 42 (or 42 true)))
  (is (= 42 (or 42)))

  (is (false? (or false)))
  (is (true? (or false true)))
  (is (true? (or true false)))
  (is (true? (or true false nil)))
  (is (= 42 (or 42 false true)))

  (is (nil? (or nil)))
  (is (true? (or nil true)))
  (is (true? (or true nil)))
  (is (true? (or true nil false)))
  (is (= 42 (or 42 nil true)))

  (is (= 42 (or nil 42 (/ 1 0)))
      "(or) should short-circuit on first truthy found"))
