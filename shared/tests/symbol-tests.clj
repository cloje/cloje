
(deftest test-gensym
  (let [g1 (gensym)
        g2 (gensym)]
    (is (symbol? g1))
    (is (symbol? g2))
    (is (not= g1 g2))
    (is (test/starts-with? (str g1) "G__"))
    (is (test/starts-with? (str g2) "G__")))

  (let [foo1 (gensym "foo")
        foo2 (gensym "foo")]
    (is (symbol? foo1))
    (is (symbol? foo2))
    (is (not= foo1 foo2))
    (is (test/starts-with? (str foo1) "foo"))
    (is (test/starts-with? (str foo2) "foo")))

  (let [bar1 (gensym 'bar)
        bar2 (gensym 'bar)]
    (is (symbol? bar1))
    (is (symbol? bar2))
    (is (not= bar1 bar2))
    (is (test/starts-with? (str bar1) "bar"))
    (is (test/starts-with? (str bar2) "bar")))

  (is (symbol? (gensym 1337)))
  (is (test/starts-with? (str (gensym 1337)) "1337"))

  (is (symbol? (gensym nil)))
  (is (not (test/starts-with? (str (gensym nil)) "nil")))

  (is (symbol? (gensym true)))
  (is (not (test/starts-with? (str (gensym true)) "true"))
      "update this test once true starts being printed as \"true\"")

  (is (symbol? (gensym false)))
  (is (not (test/starts-with? (str (gensym false)) "false"))
      "update this test once false starts being printed as \"false\"")

  (is (symbol? (gensym '(why (would (you) do) this?))))
  (is (test/starts-with? (str (gensym '(why (would (you) do) this?)))
                         "(why (would (you) do) this?)")))



(deftest test-symbol
  (is (= 'foo (symbol "foo")))
  (is (= 'foo (symbol 'foo)))

  (is (test/type-error? (symbol '#:foo)))

  (is (symbol? (symbol "2")))
  (is (not= '2 (symbol "2")))
  (is (test/type-error? (symbol 2)))

  (is (not= nil (symbol "nil")))
  ;; (is (not= 'nil (symbol "nil")))
  (is (test/type-error? (symbol nil)))

  (is (not= true (symbol "true")))
  ;; (is (not= 'true (symbol "true")))
  (is (test/type-error? (symbol true)))

  (is (not= false (symbol "false")))
  ;; (is (not= 'false (symbol "false")))
  (is (test/type-error? (symbol false)))

  (is (test/type-error? (symbol (list 'a))))
  (is (test/type-error? (symbol (vector 'a))))
  (is (test/type-error? (symbol (hash-map 'a 1)))))



(deftest test-symbol?
  (is (true? (symbol? 'foo)))

  (is (false? (symbol? '#:foo)))
  (is (false? (symbol? "foo")))
  (is (false? (symbol? '(foo))))
  (is (false? (symbol? nil)))
  (is (false? (symbol? true)))
  (is (false? (symbol? false))))



(deftest test-keyword
  (is (= '#:foo (keyword "foo")))
  (is (= '#:foo (keyword 'foo)))
  (is (= '#:foo (keyword '#:foo)))

  (is (nil? (keyword 2)))
  (is (nil? (keyword nil)))
  (is (nil? (keyword true)))
  (is (nil? (keyword false)))
  (is (nil? (keyword (list "foo"))))
  (is (nil? (keyword (vector "foo")))))



(deftest test-keyword?
  (is (true? (keyword? '#:foo)))

  (is (false? (keyword? 'foo)))
  (is (false? (keyword? "foo")))
  (is (false? (keyword? '(foo))))
  (is (false? (keyword? nil)))
  (is (false? (keyword? true)))
  (is (false? (keyword? false))))
