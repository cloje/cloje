
(deftest test-map?
  (is (true? (map? (hash-map))))
  (is (true? (map? (hash-map 'a 1 'b 2))))
  (is (false? (map? (list))))
  (is (false? (map? nil))))


(deftest test-hash-map
  (let [h (hash-map 'a 1 '#:b 2 "c" 3)]
    (is (= 1 (get h 'a)))
    (is (= 2 (get h '#:b)))
    (is (= 3 (get h "c")))))
