
(deftest test-vector
  (is (vector? (vector)))

  (let [v (vector 1 2 'a 'b "foo")]
    (is (vector? v))
    (is (= 1 (scheme/vector-ref v 0)))
    (is (= 2 (scheme/vector-ref v 1)))
    (is (= 'a (scheme/vector-ref v 2)))
    (is (= 'b (scheme/vector-ref v 3)))
    (is (= "foo" (scheme/vector-ref v 4)))))


(deftest test-vector?
  (is (true? (vector? (vector))))
  (is (true? (vector? (vector 1 2 3))))
  (is (false? (vector? '(1 2 3))))
  (is (false? (vector? nil))))


(deftest test-subvec
  (is (test/arity-error? (subvec)))
  (is (test/arity-error? (subvec (vector 1 2))))
  (is (test/arity-error? (subvec (vector 1 2) 1 2 3)))

  (is (test/bounds-error? (subvec (vector) 1)))

  (let [v (vector 1 2 3)]
    (is (= (vector 1 2 3) (subvec v 0)))
    (is (= (vector 2 3) (subvec v 1)))

    (is (= (vector 1 2 3) (subvec v 0 3)))
    (is (= (vector 2 3) (subvec v 1 3)))

    (is (= (vector 1 2) (subvec v 0 2)))
    (is (= (vector 2) (subvec v 1 2)))

    (is (= (vector) (subvec v 0 0)))
    (is (= (vector) (subvec v 1 1)))
    (is (= (vector) (subvec v 2 2)))

    (is (test/bounds-error? (subvec v 4)))
    (is (test/bounds-error? (subvec v 0 4)))
    (is (test/bounds-error? (subvec v -1 3)))))
