
(def myvar 1)
(def myvar2 "My variable" 2)

(deftest test-def
  (is (= 1 myvar))
  (is (= 2 myvar2)))
