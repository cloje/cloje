
(deftest test-string/replace
  (is (= "zzzzz" (string/replace "aaaaa" "a" "z")))
  (is (= "cloje" (string/replace "clourjureur" "ur" "")))
  (is (= "cloje" (string/replace "cloje" "foo" "bar")))
  (is (= "muamuamua" (string/replace "hahaha" "h" "mu")))

  (is (= "yay-yay" (string/replace 'oy-oy "o" "ya")))
  (is (= (string/replace (str '#:oy-oy) "o" "ya")
         (string/replace '#:oy-oy "o" "ya")))
  (is (= "42" (string/replace 86486286 "86" "")))

  (is (= "ey-ey" (string/replace "oy-oy" #\o #\e)))
  (is (test/type-error? (string/replace "oy" #\o "e")))
  (is (test/type-error? (string/replace "oy" "o" #\e)))

  (is (test/type-error? (string/replace "oy" 'o "no")))
  (is (test/type-error? (string/replace "oy" #:o "no")))
  (is (test/type-error? (string/replace "oy" nil "no")))

  (is (test/type-error? (string/replace "oy" "o" 'no)))
  (is (test/type-error? (string/replace "oy" "o" '#:no)))
  (is (test/type-error? (string/replace "oy" "o" nil)))

  (is (test/arity-error? (string/replace "foo")))
  (is (test/arity-error? (string/replace "foo" "bar")))
  (is (test/arity-error? (string/replace "foo" "bar" "baz" "quz")))

  (testing "regexp and string"
    (is (= "zzzz" (string/replace
            "abcd" (re-pattern "[abcd]") "z")))
    (is (= "Cl~x~j~x~"
           (string/replace
            "Cloje" (re-pattern "[aeiou]") "~x~")))
    (is (= "Cl~o~j~e~"
           (string/replace
            "Cloje" (re-pattern "([aeiou])") "~$1~")))

    (is (test/error?
         (string/replace "Cloje" (re-pattern "([aeiou])") "~$2~")))

    (is (= "Cl~o0~j~e0~"
           (string/replace "Cloje" (re-pattern "([aeiou])") "~$10~"))
        "$10 should be treated as $1 followed by 0")

    (is (= "abcdefghi ihgfedcba a1b2c3d4e5f6g7h8i9, jklmnopqr rqponmlkj j1k2l3m4n5o6p7q8r9"
           (string/replace
            "a1b2c3d4e5f6g7h8i9, j1k2l3m4n5o6p7q8r9"
            (re-pattern "(.)1(.)2(.)3(.)4(.)5(.)6(.)7(.)8(.)9")
            "$1$2$3$4$5$6$7$8$9 $9$8$7$6$5$4$3$2$1 $0")))

    (testing "escapes"
      (is (test/error? (string/replace
                        "Cloje" (re-pattern "[aeiou]") "\\"))
          "unescaped backslash at end of replacement string is an error")
      (is (test/error? (string/replace
                        "Cloje" (re-pattern "[aeiou]") "v\\"))
          "unescaped backslash at end of replacement string is an error")
      (is (= "Cl\\j\\"
             (string/replace
              "Cloje" (re-pattern "[aeiou]") "\\\\")))
      (is (= "Clv\\jv\\"
             (string/replace
              "Cloje" (re-pattern "[aeiou]") "v\\\\")))

      (is (= "Clvjv"
             (string/replace "Cloje" (re-pattern "[aeiou]") "\\v")))
      (is (= "Clvjv"
             (string/replace
              "Cloje" (re-pattern "(.)([aeiou])") "$1\\v")))
      (is (= "Cl\\vj\\v"
             (string/replace
              "Cloje" (re-pattern "(.)([aeiou])") "$1\\\\v")))

      (is (= "Cloje"
             (string/replace
              "Cloje" (re-pattern "(.)([aeiou])") "$1$2")))
      (is (= "Cl$2j$2"
             (string/replace
              "Cloje" (re-pattern "(.)([aeiou])") "$1\\$2")))

      (is (= "C$1$2$1$2"
             (string/replace
              "Cloje" (re-pattern "(.)([aeiou])") "\\$1\\$2")))
      (is (= "C\\l\\o\\j\\e"
             (string/replace
              "Cloje" (re-pattern "(.)([aeiou])") "\\\\$1\\\\$2")))
      (is (= "C\\$1\\$2\\$1\\$2"
             (string/replace
              "Cloje" (re-pattern "(.)([aeiou])") "\\\\\\$1\\\\\\$2")))))

  (testing "regexp and fn"
    (is (= "A clvnv of Clvjvrv"
           (string/replace
            "A clone of Clojure"
            (re-pattern "[Cc]lo[^ ]+")
            (fn [match]
              (string/replace match (re-pattern "[aeiou]") "v")))))

    (is (= "A clonv (clone) of Clojvrv (Clojure)"
           (string/replace
            "A clone of Clojure"
            (re-pattern "([Cc]lo)([^ ]+)")
            (fn [groups]
              (str (nth groups 1)
                   (string/replace
                    (nth groups 2)
                    (re-pattern "[aeiou]") "v")
                   " ("
                   (nth groups 0)
                   ")")))))))



(deftest test-string/replace-first
  (is (= "zaaaa" (string/replace-first "aaaaa" "a" "z")))
  (is (= "cloje" (string/replace-first "clojure" "ur" "")))
  (is (= "cloje" (string/replace-first "cloje" "foo" "bar")))
  (is (= "muahaha" (string/replace-first "hahaha" "h" "mu")))

  (is (= "yay" (string/replace-first 'oy "o" "ya")))
  (is (= (string/replace-first (str '#:oy) "o" "ya")
         (string/replace-first '#:oy "o" "ya")))
  (is (= "42" (string/replace-first 8642 "86" "")))

  (is (= "ey-oy" (string/replace-first "oy-oy" #\o #\e)))
  (is (test/type-error? (string/replace-first "oy" #\o "e")))
  (is (test/type-error? (string/replace-first "oy" "o" #\e)))

  (is (test/type-error? (string/replace-first "oy" 'o "no")))
  (is (test/type-error? (string/replace-first "oy" #:o "no")))
  (is (test/type-error? (string/replace-first "oy" nil "no")))

  (is (test/type-error? (string/replace-first "oy" "o" 'no)))
  (is (test/type-error? (string/replace-first "oy" "o" '#:no)))
  (is (test/type-error? (string/replace-first "oy" "o" nil)))

  (is (test/arity-error? (string/replace-first "foo")))
  (is (test/arity-error? (string/replace-first "foo" "bar")))
  (is (test/arity-error? (string/replace-first "foo" "bar" "baz" "quz")))

  (testing "regexp and string"
    (is (= "zbcd"
           (string/replace-first
            "abcd" (re-pattern "[abcd]") "z")))
    (is (= "Cl~x~je"
           (string/replace-first
            "Cloje" (re-pattern "[aeiou]") "~x~")))
    (is (= "Cl~o~je"
           (string/replace-first
            "Cloje" (re-pattern "([aeiou])") "~$1~")))

    (is (test/error?
         (string/replace-first "Cloje" (re-pattern "([aeiou])") "~$2~")))

    (is (= "Cl~o0~je"
           (string/replace-first "Cloje" (re-pattern "([aeiou])") "~$10~"))
        "$10 should be treated as $1 followed by 0")

    (is (= "abcdefghi ihgfedcba a1b2c3d4e5f6g7h8i9, j1k2l3m4n5o6p7q8r9"
           (string/replace-first
            "a1b2c3d4e5f6g7h8i9, j1k2l3m4n5o6p7q8r9"
            (re-pattern "(.)1(.)2(.)3(.)4(.)5(.)6(.)7(.)8(.)9")
            "$1$2$3$4$5$6$7$8$9 $9$8$7$6$5$4$3$2$1 $0")))

    (testing "escapes"
      (is (test/error? (string/replace-first
                        "Cloje" (re-pattern "[aeiou]") "\\"))
          "lone backslash at end of replacement string is an error")
      (is (test/error? (string/replace-first
                        "Cloje" (re-pattern "[aeiou]") "v\\"))
          "lone backslash at end of replacement string is an error")
      (is (= "Cl\\je"
             (string/replace-first
              "Cloje" (re-pattern "[aeiou]") "\\\\")))
      (is (= "Clv\\je"
             (string/replace-first
              "Cloje" (re-pattern "[aeiou]") "v\\\\")))

      (is (= "Clvje"
             (string/replace-first
              "Cloje" (re-pattern "[aeiou]") "\\v")))
      (is (= "Clvje"
             (string/replace-first
              "Cloje" (re-pattern "(.)([aeiou])") "$1\\v")))
      (is (= "Cl\\vje"
             (string/replace-first
              "Cloje" (re-pattern "(.)([aeiou])") "$1\\\\v")))

      (is (= "Cloje"
             (string/replace-first
              "Cloje" (re-pattern "(.)([aeiou])") "$1$2")))
      (is (= "Cl$2je"
             (string/replace-first
              "Cloje" (re-pattern "(.)([aeiou])") "$1\\$2")))

      (is (= "C$1$2je"
             (string/replace-first
              "Cloje" (re-pattern "(.)([aeiou])") "\\$1\\$2")))
      (is (= "C\\l\\oje"
             (string/replace-first
              "Cloje" (re-pattern "(.)([aeiou])") "\\\\$1\\\\$2")))
      (is (= "C\\$1\\$2je"
             (string/replace-first
              "Cloje" (re-pattern "(.)([aeiou])") "\\\\\\$1\\\\\\$2")))))

  (testing "regexp and fn"
    (is (= "A clvnv of Clojure"
           (string/replace-first
            "A clone of Clojure"
            (re-pattern "[Cc]lo[^ ]+")
            (fn [match]
              (string/replace match (re-pattern "[aeiou]") "v")))))

    (is (= "A clonv (clone) of Clojure"
           (string/replace-first
            "A clone of Clojure"
            (re-pattern "([Cc]lo)([^ ]+)")
            (fn [groups]
              (str (nth groups 1)
                   (string/replace
                    (nth groups 2)
                    (re-pattern "[aeiou]") "v")
                   " ("
                   (nth groups 0)
                   ")")))))))



(deftest test-string/re-quote-replacement
  (is (= "\\\\" (string/re-quote-replacement "\\")))
  (is (= "\\$" (string/re-quote-replacement "$")))
  (is (= "\\\\\\$" (string/re-quote-replacement "\\$")))

  (is (= "Cl\\j\\"
         (string/replace "Cloje" (re-pattern "([aeiou])")
                         (string/re-quote-replacement "\\"))))
  (is (= "Cl$1j$1"
         (string/replace "Cloje" (re-pattern "([aeiou])")
                         (string/re-quote-replacement "$1"))))
  (is (= "Cl\\$1j\\$1"
         (string/replace "Cloje" (re-pattern "([aeiou])")
                         (string/re-quote-replacement "\\$1"))))

  (is (= "abc" (string/re-quote-replacement "abc")))
  (is (= "a\\\\bc" (string/re-quote-replacement "a\\bc")))
  (is (= "\n" (string/re-quote-replacement "\n")))
  (is (= "\\\\\n" (string/re-quote-replacement "\\\n")))

  (is (test/arity-error? (string/re-quote-replacement)))
  (is (test/arity-error? (string/re-quote-replacement "too" "many")))

  (is (= "1" (string/re-quote-replacement 1)))
  (is (= "\\\\" (string/re-quote-replacement '\\)))
  (is (= "\\\\" (string/re-quote-replacement #\\)))
  (is (= "\\$1" (string/re-quote-replacement '$1)))
  (is (= (string/re-quote-replacement (str '#:$1))
         (string/re-quote-replacement '#:$1)))
  (is (= (string/re-quote-replacement (str true))
         (string/re-quote-replacement true)))
  (is (= (string/re-quote-replacement (str false))
         (string/re-quote-replacement false)))

  (is (test/type-error? (string/re-quote-replacement nil))))
