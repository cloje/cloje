
(deftest test-hash
  (testing "booleans"
    (is (= (hash true)  (hash true)))
    (is (= (hash false) (hash false)))
    (is (= (hash nil)   (hash nil)))

    (is (not= (hash true)  (hash false)))
    (is (not= (hash true)  (hash nil)))
    (is (not= (hash false) (hash nil))))

  (testing "numbers"
    (is (= (hash (* 2 1234567890))
           (hash (* 2 1234567890))))
    (is (= (hash (* 2 1234567890.0))
           (hash (* 2 1234567890.0))))

    (is (not= (hash (* 2 1234567890))
              (hash (* 9 1234567890))))
    (is (not= (hash (* 2 1234567890.0))
              (hash (* 9 1234567890.0)))))

  (testing "symbols"
    (is (= (hash 'cloje) (hash (symbol "cloje"))))
    (is (not= (hash 'cloje) (hash (symbol "clojure")))))

  (testing "keywords"
    (is (= (hash (keyword "cloje")) (hash (keyword "cloje"))))
    (is (not= (hash (keyword "cloje")) (hash (keyword "clojure")))))

  (testing "functions"
    (is (= (hash hash) (hash hash)))
    (is (not= (hash hash) (hash not)))
    (let [f (fn [x] x)]
      (is (= (hash f) (hash f)))))

  (testing "strings"
    (is (= (hash "abc") (hash "abc")))
    (is (= (hash "abc") (hash (str 'abc))))
    (is (not= (hash "abc") (hash "ABC")))
    (is (not= (hash "abc") (hash "cab"))))

  (testing "lists"
    (is (= (hash (list)) (hash (list))))
    (is (= (hash (list 1 2 3)) (hash (list 1 2 3))))
    (is (= (hash (list 1 (list 2 (list 3))))
           (hash (list 1 (list 2 (list 3))))))

    (is (not= (hash (list 1 2 3)) (hash (list))))
    (is (not= (hash (list 1 2 3)) (hash (list 1 2 4)))))

  (testing "vectors"
    (is (= (hash (vector)) (hash (vector))))
    (is (= (hash (vector 1 2 3)) (hash (vector 1 2 3))))

    (is (not= (hash (vector 1 2 3)) (hash (vector))))
    (is (not= (hash (vector 1 2 3)) (hash (vector 1 2 4)))))

  (testing "hash maps"
    (let [h0 (hash-map)
          h1 (hash-map 'a 1)
          h2 (hash-map 'a 1 'b 2)
          h3 (hash-map 'a 1 'b 2 'c 3)]
      (is (= (hash h0) (hash h0)))
      (is (= (hash h0) (hash (hash-map))))
      (is (= (hash h1) (hash h1)))
      (is (= (hash h1) (hash (hash-map 'a 1))))
      (is (= (hash h2) (hash (hash-map 'a 1 'b 2))))
      (is (= (hash h3) (hash (hash-map 'a 1 'b 2 'c 3))))

      (is (not= (hash h1) (hash h2)))
      (is (not= (hash h1) (hash h3)))
      (is (not= (hash h2) (hash h3)))))

  (testing "hash sets"
    (let [hs0 (hash-set)
          hs1 (hash-set 1)
          hs2 (hash-set 1 2 3)
          hs3 (hash-set 3 2 1)]

      (is (= (hash hs0) (hash (hash-set))))
      (is (= (hash (hash-set 1 2 3))
             (hash (hash-set 3 2 1))))

      (is (not= (hash (hash-set 1 2 3))
                (hash (hash-set 1 2))))
      (is (not= (hash (hash-set 1))
                (hash (list 1)))))

    (let [a "a" b "b"]
      (is (= (hash (hash-set a b)) (hash (hash-set a b))))
      (is (not= (hash (hash-set a b))
                (hash (hash-map a a b b)))))))
