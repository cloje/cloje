
(deftest test-identical?
  (is (true? (identical? true true)))
  (is (true? (identical? false false)))
  (is (true? (identical? nil nil)))
  (is (true? (identical? 2 2)))
  (is (true? (identical? 2 (+ 1 1))))
  (let [x 'abc]
   (is (true? (identical? x x))))
  (let [x "abc"]
    (is (true? (identical? x x))))

  (is (false? (identical? true false)))
  (is (false? (identical? false nil)))
  (is (false? (identical? nil true)))
  (is (false? (identical? 1 2)))
  (is (false? (identical? 1 1.0)))
  (is (false? (identical? 'abc 'xyz)))
  (is (false? (identical? "abc" "xyz"))))



(deftest test-=
  (is (true? (= true true true)))
  (is (true? (= false false false)))
  (is (true? (= nil nil nil)))

  (is (true? (= 2 2)))
  (is (true? (= 2 (+ 1 1))))
  (is (true? (= 42 42 42 42 42 42 42 42 42)))

  (is (true? (= 'abc 'abc)))
  (is (true? (= 'abc 'abc 'abc)))
  (let [x 'abc]
    (is (true? (= x x)))
    (is (true? (= x x x x))))

  (is (true? (= '#:abc '#:abc)))
  (is (true? (= '#:abc '#:abc '#:abc)))

  (is (true? (= "abc" "abc")))
  (is (true? (= "abc" "abc" "abc")))
  (let [x "abc"]
    (is (true? (= x x)))
    (is (true? (= x x x x))))

  (is (true? (= '() '())))
  (is (true? (= '(abc xyz) '(abc xyz))))
  (is (true? (= '(abc (xyz)) '(abc (xyz)))))

  (is (true? (= '(a (b (c)))
                '(a (b (c)))
                '(a (b (c))))))

  (is (true? (= (vector) (vector))))
  (is (true? (= (vector 1 2) (vector 1 2))))

  (is (true? (= (hash-map) (hash-map))))
  (is (true? (= (hash-map 'a 1 "b" 2) (hash-map 'a 1 "b" 2))))
  (is (true? (= (hash-map 'a 1 "b" 2) (hash-map "b" 2 'a 1))))

  (is (true? (= (list) (vector))))
  (is (true? (= (vector) (list))))
  (is (true? (= (list 1 2) (vector 1 2))))
  (is (true? (= (list 1 2) (seq (vector 1 2)))))
  (is (true? (= (vector 1 2) (list 1 2))))
  (is (true? (= (seq (vector 1 2)) (list 1 2))))
  (is (true? (= (list #\c #\l #\j) (seq "clj"))))


  (is (false? (= true false)))
  (is (false? (= false nil)))
  (is (false? (= nil true)))
  (is (false? (= nil '())))

  (is (false? (= true true false)))

  (is (false? (= 1 0)))
  (is (false? (= 1 1 0)))
  (is (false? (= 1 1 1 1 1 1 0)))
  (is (false? (= 1 1 1 0 1 1 1)))
  (is (false? (= 0 1 1 1 1 1 1)))

  (is (false? (= 1 1.0)))
  (is (false? (= 1 1 1.0)))

  (is (false? (= 'abc 'xyz)))
  (is (false? (= 'abc 'abc 'xyz)))
  (is (false? (= '#:abc '#:xyz)))
  (is (false? (= '#:abc '#:abc '#:xyz)))
  (is (false? (= "abc" "xyz")))
  (is (false? (= "abc" "abc" "xyz")))
  (is (false? (= 'abc "abc")))
  (is (false? (= 'abc '#:abc)))
  (is (false? (= '#:abc "abc")))

  (is (false? (= '(abc) '(xyz))))
  (is (false? (= '(abc (xyz)) '(abc (foo)))))
  (is (false? (= '(a (b (c)))
                 '(a (b (c)))
                 '(a (b (x))))))

  (is (false? (= (vector 1 2) (vector 1))))
  (is (false? (= (vector 1 2) (vector 2 3))))
  (is (false? (= (vector 1 2) (vector 1 2 3))))

  (is (false? (= (hash-map 'a 1 "b" 2) (hash-map 'a 1 'b 2))))
  (is (false? (= (hash-map 'a 1 "b" 2) (hash-map 'a 2 "b" 1))))

  (is (false? (= (list 1 2) (vector 1 2 3))))
  (is (false? (= (list 1 2) (seq (vector 1 2 3)))))
  (is (false? (= (vector 1 2) (list 1 2 3))))
  (is (false? (= (seq (vector 1 2)) (list 1 2 3))))
  (is (false? (= (list #\c #\l #\j) (seq "clje"))))
  (is (false? (= (seq "clje") (list #\c #\l #\j))))

  (is (false? (= (list #\c #\l #\j) "clj")))
  (is (false? (= (vector #\c #\l #\j) "clj")))
  (is (false? (= "clj" (list #\c #\l #\j))))
  (is (false? (= "clj" (vector #\c #\l #\j))))

  (comment "See set-tests.clj for equality tests involving sets."))



(deftest test-not=
  (is (false? (not= true true true)))
  (is (false? (not= false false false)))
  (is (false? (not= nil nil nil)))

  (is (false? (not= 2 2)))
  (is (false? (not= 2 (+ 1 1))))
  (is (false? (not= 42 42 42 42 42 42 42 42 42)))

  (is (false? (not= 'abc 'abc)))
  (is (false? (not= 'abc 'abc 'abc)))
  (let [x 'abc]
    (is (false? (not= x x)))
    (is (false? (not= x x x x))))

  (is (false? (not= '#:abc '#:abc)))
  (is (false? (not= '#:abc '#:abc '#:abc)))

  (is (false? (not= "abc" "abc")))
  (is (false? (not= "abc" "abc" "abc")))
  (let [x "abc"]
    (is (false? (not= x x)))
    (is (false? (not= x x x x))))

  (is (false? (not= '() '())))
  (is (false? (not= '(abc xyz) '(abc xyz))))
  (is (false? (not= '(abc (xyz)) '(abc (xyz)))))

  (is (false? (not= '(a (b (c)))
                    '(a (b (c)))
                    '(a (b (c))))))

  (is (false? (not= (vector) (vector))))
  (is (false? (not= (vector 1 2) (vector 1 2))))

  (is (false? (not= (hash-map) (hash-map))))
  (is (false? (not= (hash-map 'a 1 "b" 2) (hash-map 'a 1 "b" 2))))
  (is (false? (not= (hash-map 'a 1 "b" 2) (hash-map "b" 2 'a 1))))


  (is (true? (not= true false)))
  (is (true? (not= false nil)))
  (is (true? (not= nil true)))

  (is (true? (not= true true false)))

  (is (true? (not= 1 0)))
  (is (true? (not= 1 1 0)))
  (is (true? (not= 1 1 1 1 1 1 0)))
  (is (true? (not= 1 1 1 0 1 1 1)))
  (is (true? (not= 0 1 1 1 1 1 1)))

  (is (true? (not= 1 1.0)))
  (is (true? (not= 1 1 1.0)))

  (is (true? (not= 'abc 'xyz)))
  (is (true? (not= 'abc 'abc 'xyz)))
  (is (true? (not= '#:abc '#:xyz)))
  (is (true? (not= '#:abc '#:abc '#:xyz)))
  (is (true? (not= "abc" "xyz")))
  (is (true? (not= "abc" "abc" "xyz")))
  (is (true? (not= 'abc "abc")))
  (is (true? (not= 'abc '#:abc)))
  (is (true? (not= '#:abc "abc")))

  (is (true? (not= '(abc) '(xyz))))
  (is (true? (not= '(abc (xyz)) '(abc (foo)))))
  (is (true? (not= '(a (b (c)))
                   '(a (b (c)))
                   '(a (b (x))))))

  (is (true? (not= (vector 1 2) (vector 1))))
  (is (true? (not= (vector 1 2) (vector 2 3))))
  (is (true? (not= (vector 1 2) (vector 1 2 3))))

  (is (true? (not= (hash-map 'a 1 "b" 2) (hash-map 'a 1 'b 2))))
  (is (true? (not= (hash-map 'a 1 "b" 2) (hash-map 'a 2 "b" 1)))))
