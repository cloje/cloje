
(deftest test-re-pattern
  (is (re-pattern "cloj(ur)?e"))

  (is (test/arity-error? (re-pattern)))
  (is (test/arity-error? (re-pattern "too" "many")))

  (is (test/type-error? (re-pattern 'a)))
  (is (test/type-error? (re-pattern 1)))
  (is (test/type-error? (re-pattern true)))
  (is (test/type-error? (re-pattern false)))
  (is (test/type-error? (re-pattern nil))))



(deftest test-re-pattern?
  (is (true? (re-pattern? (re-pattern "cloj(ur)?e"))))

  (is (false? (re-pattern? "cloj(ur)?e")))
  (is (false? (re-pattern? 'a)))
  (is (false? (re-pattern? 1)))
  (is (false? (re-pattern? true)))
  (is (false? (re-pattern? false)))
  (is (false? (re-pattern? nil))))



(deftest test-re-matcher
  (is (re-matcher (re-pattern "cloj(ur)?e") "I love cloje"))

  (is (test/arity-error? (re-matcher)))
  (is (test/arity-error? (re-matcher (re-pattern "cloj(ur)?e"))))
  (is (test/arity-error? (re-matcher (re-pattern "too")
                                     "many" "args")))

  (is (test/type-error? (re-matcher "just a string" "foo")))
  (is (test/type-error? (re-matcher (re-pattern "pat") 'a)))
  (is (test/type-error? (re-matcher (re-pattern "pat") 1)))
  (is (test/type-error? (re-matcher (re-pattern "pat") true)))
  (is (test/type-error? (re-matcher (re-pattern "pat") false)))
  (is (test/type-error? (re-matcher (re-pattern "pat") nil))))



(deftest test-re-matcher?
  (is (true? (re-matcher? (re-matcher (re-pattern "cloj(ur)?e")
                                      "I love cloje"))))

  (is (false? (re-matcher? "foo")))
  (is (false? (re-matcher? (re-pattern "foo"))))
  (is (false? (re-matcher? 'a)))
  (is (false? (re-matcher? 1)))
  (is (false? (re-matcher? true)))
  (is (false? (re-matcher? false)))
  (is (false? (re-matcher? nil))))



(deftest test-re-find
  (let [pattern       (re-pattern "[Cc]lo[njur]+e")
        group-pattern (re-pattern "[Cc]lo(n|j(ur)?)e")
        input "Cloje: A clone of Clojure built atop Scheme/Lisp"]

    (testing "pattern and input"
      ;; Always the same result:
      (is (= (re-find pattern input) "Cloje"))
      (is (= (re-find pattern input) "Cloje"))
      (is (= (re-find pattern input) "Cloje"))

      ;; Always the same result:
      (is (= (re-find group-pattern input)
             (vector "Cloje" "j" nil)))
      (is (= (re-find group-pattern input)
             (vector "Cloje" "j" nil)))
      (is (= (re-find group-pattern input)
             (vector "Cloje" "j" nil))))

    (testing "matcher"
      (let [m (re-matcher pattern input)]
        (is (= (re-find m) "Cloje"))
        (is (= (re-find m) "clone"))
        (is (= (re-find m) "Clojure"))
        (is (nil? (re-find m)))
        (is (nil? (re-find m))))

      (let [m (re-matcher group-pattern input)]
        (is (= (re-find m) (vector "Cloje"   "j"   nil)))
        (is (= (re-find m) (vector "clone"   "n"   nil)))
        (is (= (re-find m) (vector "Clojure" "jur" "ur")))
        (is (nil? (re-find m)))
        (is (nil? (re-find m)))))))



(deftest test-re-groups
  (is (test/arity-error? (re-groups)))
  (is (test/arity-error? (re-groups "too" "many")))

  (is (test/type-error? (re-groups "a")))
  (is (test/type-error? (re-groups (re-pattern "a"))))

  (let [pattern       (re-pattern "[Cc]lo[njur]+e")
        group-pattern (re-pattern "[Cc]lo(n|j(ur)?)e")
        input "Cloje: A clone of Clojure built atop Scheme/Lisp"]

    (let [m (re-matcher pattern input)]
      (is (test/error? (re-groups m)))

      (is (= (re-find m)   "Cloje"))
      (is (= (re-groups m) "Cloje"))

      (is (= (re-find m)   "clone"))
      (is (= (re-groups m) "clone"))

      (is (= (re-find m)   "Clojure"))
      (is (= (re-groups m) "Clojure"))

      (is (nil? (re-find m)))
      (is (test/error? (re-groups m)))

      (is (nil? (re-find m)))
      (is (test/error? (re-groups m))))

    (let [m (re-matcher group-pattern input)]
      (is (test/error? (re-groups m)))

      (is (= (re-find m)   (vector "Cloje"   "j"   nil)))
      (is (= (re-groups m) (vector "Cloje"   "j"   nil)))

      (is (= (re-find m)   (vector "clone"   "n"   nil)))
      (is (= (re-groups m) (vector "clone"   "n"   nil)))

      (is (= (re-find m)   (vector "Clojure" "jur" "ur")))
      (is (= (re-groups m) (vector "Clojure" "jur" "ur")))

      (is (nil? (re-find m)))
      (is (test/error? (re-groups m)))

      (is (nil? (re-find m)))
      (is (test/error? (re-groups m))))))



(deftest test-re-matches
  (is (test/arity-error? (re-matches)))
  (is (test/arity-error? (re-matches "foo")))
  (is (test/arity-error? (re-matches "too" "many" "args")))

  (is (test/type-error? (re-matches "foo" "bar")))
  (is (test/type-error? (re-matches (re-pattern "foo") 'bar)))

  (let [pat   (re-pattern ".loj")
        pat2  (re-pattern ".loj.")
        gpat  (re-pattern "(.)loj")
        gpat2 (re-pattern "(.)loj(.)")]

    (is (= "Cloj" (re-matches pat "Cloj")))
    (is (nil? (re-matches pat "Cloje")))
    (is (nil? (re-matches pat "zCloj")))
    (is (nil? (re-matches pat "foo")))

    (is (= "Cloje" (re-matches pat2 "Cloje")))
    (is (nil? (re-matches pat2 "Cloj")))
    (is (nil? (re-matches pat2 "Clojur")))
    (is (nil? (re-matches pat2 "zCloj")))
    (is (nil? (re-matches pat2 "zCloje")))
    (is (nil? (re-matches pat2 "foo")))

    (is (= (vector "Cloj" "C") (re-matches gpat "Cloj")))
    (is (nil? (re-matches gpat "Cloje")))
    (is (nil? (re-matches gpat "zCloj")))
    (is (nil? (re-matches gpat "foo")))

    (is (= (vector "Cloje" "C" "e")
           (re-matches gpat2 "Cloje")))
    (is (nil? (re-matches gpat2 "Cloj")))
    (is (nil? (re-matches gpat2 "Clojur")))
    (is (nil? (re-matches gpat2 "zCloj")))
    (is (nil? (re-matches gpat2 "zCloje")))
    (is (nil? (re-matches gpat2 "foo")))))



(deftest test-re-seq
  (is (test/arity-error? (re-seq)))
  (is (test/arity-error? (re-seq "foo")))
  (is (test/arity-error? (re-seq "too" "many" "args")))

  (is (test/type-error? (re-seq "foo" "bar")))
  (is (test/type-error? (re-seq (re-pattern "foo") 'bar)))

  (let [pattern       (re-pattern "[Cc]lo[njur]+e")
        group-pattern (re-pattern "[Cc]lo(n|j(ur)?)e")
        input "Cloje: A clone of Clojure built atop Scheme/Lisp"]

    (is (= (re-seq pattern input)
           (list "Cloje" "clone" "Clojure")))

    (is (= (re-seq group-pattern input)
           (list (vector "Cloje"   "j"   nil)
                 (vector "clone"   "n"   nil)
                 (vector "Clojure" "jur" "ur"))))

    (is (nil? (re-seq pattern "foo")))))
