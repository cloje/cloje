
(deftest test-with-in-str
  (is (nil? (with-in-str "a")))
  (is (= 'a (with-in-str "a" (scheme/read))))
  (is (= 42 (with-in-str "a" 42)))
  (is (= 42 (with-in-str "a" (scheme/read) 42)))

  (is (test/arity-error? (with-in-str))))


(deftest test-with-out-str
  (is (= "" (with-out-str)))
  (is (= "" (with-out-str "foo")))
  (is (= "a" (with-out-str (print "a"))))
  (is (= "a" (with-out-str (print "a") "foo"))))



(define _newline-str (with-out-str (scheme/newline)))
(define (_append-newline s)
  (scheme/string-append s _newline-str))



(deftest test-newline
  (is (= _newline-str) (with-out-str (newline))))



(deftest test-print
  (is (= "" (with-out-str (print))))

  (is (= "1" (with-out-str (print 1))))
  (is (= "a" (with-out-str (print "a"))))
  (is (= "a" (with-out-str (print 'a))))

  (is (= "1 2 3" (with-out-str (print 1 2 3))))
  (is (= "a b c" (with-out-str (print "a" "b" "c"))))
  (is (= "a b c" (with-out-str (print 'a 'b 'c))))

  (is (= "(a 1) (b 2)" (with-out-str (print '(a 1) '(b 2)))))
  (is (= "(a 1) (b 2)" (with-out-str (print '("a" 1) '("b" 2))))))



(deftest test-println
  (is (= _newline-str (with-out-str (println))))

  (is (= (_append-newline "1")
         (with-out-str (println 1))))
  (is (= (_append-newline "a")
         (with-out-str (println "a"))))
  (is (= (_append-newline "a")
         (with-out-str (println 'a))))

  (is (= (_append-newline "1 2 3")
         (with-out-str (println 1 2 3))))
  (is (= (_append-newline "a b c")
         (with-out-str (println "a" "b" "c"))))
  (is (= (_append-newline "a b c")
         (with-out-str (println 'a 'b 'c))))

  (is (= (_append-newline "(a 1) (b 2)")
         (with-out-str (println '(a 1) '(b 2)))))
  (is (= (_append-newline "(a 1) (b 2)")
         (with-out-str (println '("a" 1) '("b" 2))))))



(deftest test-print-str
  (is (= "" (print-str)))

  (is (= "1" (print-str 1)))
  (is (= "a" (print-str "a")))
  (is (= "a" (print-str 'a)))

  (is (= "1 2 3" (print-str 1 2 3)))
  (is (= "a b c" (print-str "a" "b" "c")))
  (is (= "a b c" (print-str 'a 'b 'c)))

  (is (= "(a 1) (b 2)" (print-str '(a 1) '(b 2))))
  (is (= "(a 1) (b 2)" (print-str '("a" 1) '("b" 2)))))



(deftest test-println-str
  (is (= _newline-str (println-str)))

  (is (= (_append-newline "1")
         (println-str 1)))
  (is (= (_append-newline "a")
         (println-str "a")))
  (is (= (_append-newline "a")
         (println-str 'a)))

  (is (= (_append-newline "1 2 3")
         (println-str 1 2 3)))
  (is (= (_append-newline "a b c")
         (println-str "a" "b" "c")))
  (is (= (_append-newline "a b c")
         (println-str 'a 'b 'c)))

  (is (= (_append-newline "(a 1) (b 2)")
         (println-str '(a 1) '(b 2))))
  (is (= (_append-newline "(a 1) (b 2)")
         (println-str '("a" 1) '("b" 2)))))



(deftest test-pr
  (is (= "" (with-out-str (pr))))

  (is (= "1" (with-out-str (pr 1))))
  (is (= "\"a\"" (with-out-str (pr "a"))))
  (is (= "a" (with-out-str (pr 'a))))

  (is (= "1 2 3" (with-out-str (pr 1 2 3))))
  (is (= "\"a\" \"b\" \"c\"" (with-out-str (pr "a" "b" "c"))))
  (is (= "a b c" (with-out-str (pr 'a 'b 'c))))

  (is (= "(a 1) (b 2)" (with-out-str (pr '(a 1) '(b 2)))))
  (is (= "(\"a\" 1) (\"b\" 2)" (with-out-str (pr '("a" 1) '("b" 2))))))



(deftest test-prn
  (is (= _newline-str (with-out-str (prn))))

  (is (= (_append-newline "1")
         (with-out-str (prn 1))))
  (is (= (_append-newline "\"a\"")
         (with-out-str (prn "a"))))
  (is (= (_append-newline "a")
         (with-out-str (prn 'a))))

  (is (= (_append-newline "1 2 3")
         (with-out-str (prn 1 2 3))))
  (is (= (_append-newline "\"a\" \"b\" \"c\"")
         (with-out-str (prn "a" "b" "c"))))
  (is (= (_append-newline "a b c")
         (with-out-str (prn 'a 'b 'c))))

  (is (= (_append-newline "(a 1) (b 2)")
         (with-out-str (prn '(a 1) '(b 2)))))
  (is (= (_append-newline "(\"a\" 1) (\"b\" 2)")
         (with-out-str (prn '("a" 1) '("b" 2))))))



(deftest test-pr-str
  (is (= "" (pr-str)))

  (is (= "1" (pr-str 1)))
  (is (= "\"a\"" (pr-str "a")))
  (is (= "a" (pr-str 'a)))

  (is (= "1 2 3" (pr-str 1 2 3)))
  (is (= "\"a\" \"b\" \"c\"" (pr-str "a" "b" "c")))
  (is (= "a b c" (pr-str 'a 'b 'c)))

  (is (= "(a 1) (b 2)" (pr-str '(a 1) '(b 2))))
  (is (= "(\"a\" 1) (\"b\" 2)" (pr-str '("a" 1) '("b" 2)))))



(deftest test-prn-str
  (is (= _newline-str (prn-str)))

  (is (= (_append-newline "1")
         (prn-str 1)))
  (is (= (_append-newline "\"a\"")
         (prn-str "a")))
  (is (= (_append-newline "a")
         (prn-str 'a)))

  (is (= (_append-newline "1 2 3")
         (prn-str 1 2 3)))
  (is (= (_append-newline "\"a\" \"b\" \"c\"")
         (prn-str "a" "b" "c")))
  (is (= (_append-newline "a b c")
         (prn-str 'a 'b 'c)))

  (is (= (_append-newline "(a 1) (b 2)")
         (prn-str '(a 1) '(b 2))))
  (is (= (_append-newline "(\"a\" 1) (\"b\" 2)")
         (prn-str '("a" 1) '("b" 2)))))
