
(defn addify [x y] (+ x y))
(defn addify2 "Adds two numbers" [x y] (+ x y))
(defn add-all "Adds all the things" [& nums] (scheme/apply + nums))

(deftest test-defn
  (is (= 2 (addify 1 1)))
  (is (= 4 (addify2 2 2)))
  (is (= 6 (add-all 1 2 3))))


(deftest test-fn
  (let [ret1       (fn [] 1)
        add        (fn [x y] (+ x y))
        make-adder (fn [n] (fn [x] (add n x)))
        add1       (make-adder 1)]
    (is (true? (fn? ret1)))
    (is (true? (fn? add)))
    (is (true? (fn? make-adder)))
    (is (true? (fn? add1)))
    (is (= 2 (add1 (ret1)))))

  (let [has-rest (fn [a & rest] (list a rest))
        only-rest (fn [& rest] rest)]
    (is (= '(1 (2 3)) (has-rest 1 2 3)))
    (is (= '(1 2 3) (only-rest 1 2 3)))))


(deftest test-fn?
  (is (true? (fn? (fn [] 1))))
  (is (false? (fn? 1)))
  (is (false? (fn? 'a)))
  (is (false? (fn? '#:a))))
