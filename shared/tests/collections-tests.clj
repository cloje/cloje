
(deftest test-coll?
  (is (true? (coll? (list 1 2))))
  (is (true? (coll? (vector 1 2))))
  (is (true? (coll? (seq (vector 1 2)))))
  (is (true? (coll? (seq "clj"))))
  (is (true? (coll? (hash-map 'a 1 'b 2))))
  (is (true? (coll? (hash-set 'a 'b 'c))))

  (is (false? (coll? "clj")))
  (is (false? (coll? true)))
  (is (false? (coll? false)))
  (is (false? (coll? nil)))
  (is (false? (coll? 1))))


(deftest test-associative?
  (is (true? (associative? (vector 1 2))))
  (is (true? (associative? (hash-map 'a 1 'b 2))))
  (is (false? (associative? (list 1 2))))
  (is (false? (associative? (hash-set 'a 'b 'c)))))


(deftest test-counted?
  ;; (is (true? (counted? (list 1 2))))
  (is (true? (counted? (vector 1 2))))
  (is (true? (counted? (hash-map 'a 1 'b 2))))
  (is (true? (counted? (hash-set 'a 'b 'c))))

  (is (false? (counted? "clj"))))


(deftest test-sequential?
  (is (true? (sequential? (list 1 2))))
  (is (true? (sequential? (vector 1 2))))
  (is (true? (sequential? (seq (vector 1 2)))))
  (is (true? (sequential? (seq "clj"))))

  (is (false? (sequential? (hash-map 'a 1 'b 2))))
  (is (false? (sequential? (hash-set 'a 'b 'c))))
  (is (false? (sequential? "clj")))
  (is (false? (sequential? nil))))


(deftest test-sorted?
  (is (false? (sorted? (list 1 2))))
  (is (false? (sorted? (vector 1 2))))
  (is (false? (sorted? (hash-map 'a 1 'b 2))))
  (is (false? (sorted? (hash-set 'a 'b 'c)))))


(deftest test-reversible?
  (is (false? (reversible? (list 1 2))))
  (is (true? (reversible? (vector 1 2))))
  (is (false? (reversible? (hash-map 'a 1 'b 2))))
  (is (false? (reversible? (hash-set 'a 'b 'c)))))



(deftest test-assoc
  (is (test/arity-error? (assoc)))

  (testing "vectors"
    (let [v1 (vector 1 2 3)
          v2 (assoc v1  0 -1  2 4  0 9)] ; note setting index 0 twice
      (is (= 1 (scheme/vector-ref v1 0)))
      (is (= 2 (scheme/vector-ref v1 1)))
      (is (= 3 (scheme/vector-ref v1 2)))

      (is (= 9 (scheme/vector-ref v2 0)))
      (is (= 2 (scheme/vector-ref v2 1)))
      (is (= 4 (scheme/vector-ref v2 2))))

    (is (test/bounds-error? (assoc (vector 0) 1 2))) ; out of bounds

    (is (test/arity-error? (assoc (vector 0) 0)))
    (is (test/arity-error? (assoc (vector 0)))))

  (testing "hash-maps"
    (let [h1 (hash-map 'a 1 'b 2 'c 3)
          h2 (assoc h1  'a -1  'c 4  'z 5  'a 9)] ; note set 'a twice
      (is (= 1 (get h1 'a)))
      (is (= 2 (get h1 'b)))
      (is (= 3 (get h1 'c)))

      (is (= 9 (get h2 'a)))
      (is (= 2 (get h2 'b)))
      (is (= 4 (get h2 'c)))
      (is (= 5 (get h2 'z))))

    (is (test/arity-error? (assoc (hash-map 'a 0) 'a)))
    (is (test/arity-error? (assoc (hash-map 'a 0)))))

  (testing "nil"
    (let [h (assoc nil  'a -1  'c 4  'z 5  'a 9)] ; note set 'a twice
      (is (true? (map? h)))
      (is (= 9 (get h 'a)))
      (is (= 4 (get h 'c)))
      (is (= 5 (get h 'z))))))



(deftest test-count
  (is (test/arity-error? (count)))

  (is (test/type-error? (count 1)))
  (is (test/type-error? (count 'a)))
  (is (test/type-error? (count true)))
  (is (test/type-error? (count false)))

  (is (= 0 (count nil)))

  (testing "consed seqs"
    (is (= 4 (count (cons 1 (cons 2 (seq (vector 3 4)))))))
    (is (= 4 (count (cons 1 (cons 2 (seq "34")))))))

  (testing "lists"
    (is (= 0 (count (list))))
    (is (= 1 (count (list 1))))
    (is (= 5 (count (list 1 2 3 4 5)))))

  (testing "vectors"
    (is (= 0 (count (vector))))
    (is (= 1 (count (vector 1))))
    (is (= 5 (count (vector 1 2 3 4 5)))))

  (testing "vector seqs"
    (is (= 0 (count (seq (vector)))))
    (is (= 1 (count (seq (vector 1)))))
    (is (= 5 (count (seq (vector 1 2 3 4 5))))))

  (testing "strings"
    (is (= 0 (count "")))
    (is (= 1 (count "c")))
    (is (= 5 (count "cloje")))
    (is (= 1 (count "λ"))))

  (testing "string seqs"
    (is (= 0 (count (seq ""))))
    (is (= 1 (count (seq "c"))))
    (is (= 5 (count (seq "cloje")))))

  (testing "hash-maps"
    (is (= 0 (count (hash-map))))
    (is (= 1 (count (hash-map 'a 1))))
    (is (= 3 (count (hash-map 'a 1 'b 2 'c 3)))))

  (testing "hash-map seqs"
    (is (= 0 (count (seq (hash-map)))))
    (is (= 1 (count (seq (hash-map 'a 1)))))
    (is (= 3 (count (seq (hash-map 'a 1 'b 2 'c 3))))))

  (testing "hash-sets"
    (is (= 0 (count (hash-set))))
    (is (= 1 (count (hash-set 'a))))
    (is (= 3 (count (hash-set 'a 'b 'c))))
    (is (= 3 (count (hash-set 'a 'b 'c 'c 'a 'b))))))



(deftest test-distinct?
  (is (test/arity-error? (distinct?)))

  (is (true? (distinct? 1)))
  (is (true? (distinct? 1 2 3 4 5 6 7 8 9 10)))
  (is (true? (distinct? (list 1 2) (list 2 1))))
  (is (true? (distinct? "clo" "je" "me")))
  (is (true? (distinct? 1 1.0)))

  (is (false? (distinct? 1 2 1)))
  (is (false? (distinct? 1 1 1)))
  (is (false? (distinct? (list 1 2) (list 2 1) (list 1 2))))
  (is (false? (distinct? (list 1 2) (vector 1 2))))
  (is (false? (distinct? "clo" "je" "je"))))



(deftest test-empty
  (is (test/arity-error? (empty)))

  (is (nil? (empty nil)))
  (is (nil? (empty 'a)))
  (is (nil? (empty "clj")))
  (is (nil? (empty 1)))

  (is (= '() (empty (seq "clj"))))
  (is (= '() (empty (seq (vector 1 2 3)))))

  (testing "lists"
    (is (= '() (empty '())))
    (is (= '() (empty '(1 2 3)))))

  (testing "vectors"
    (is (= (vector) (empty (vector))))
    (is (= (vector) (empty (vector 1 2 3)))))

  (testing "hash-maps"
    (is (= (hash-map) (empty (hash-map))))
    (is (= (hash-map) (empty (hash-map 'a 1 'b 2)))))

  (testing "hash-sets"
    (is (= (hash-set) (empty (hash-set))))
    (is (= (hash-set) (empty (hash-set 'a 'b 'c))))))



(deftest test-empty?
  (is (test/arity-error? (empty?)))

  (is (test/type-error? (empty? true)))
  (is (test/type-error? (empty? false)))
  (is (test/type-error? (empty? 'a)))
  (is (test/type-error? (empty? 1)))

  (is (true? (empty? nil)))

  (is (false? (empty? (cons 1 (vector)))))

  (testing "lists"
    (is (true?  (empty? '())))
    (is (false? (empty? '(1)))))

  (testing "vectors"
    (is (true?  (empty? (vector))))
    (is (false? (empty? (vector 1)))))

  (testing "vector seqs"
    (is (true?  (empty? (seq (vector)))))
    (is (false? (empty? (seq (vector 1))))))

  (testing "strings"
    (is (true?  (empty? "")))
    (is (false? (empty? "c"))))

  (testing "string seqs"
    (is (true?  (empty? (seq ""))))
    (is (false? (empty? (seq "c")))))

  (testing "hash-maps"
    (is (true?  (empty? (hash-map))))
    (is (false? (empty? (hash-map 'a 1)))))

  (testing "hash-sets"
    (is (true?  (empty? (hash-set))))
    (is (false? (empty? (hash-set 'a))))))



(deftest test-every?
  (is (test/arity-error? (every?)))

  (is (test/type-error? (every? true? true)))
  (is (test/type-error? (every? true? false)))
  (is (test/type-error? (every? true? 'a)))
  (is (test/type-error? (every? true? 1)))

  (is (true? (every? (fn [x] (/ 1 0)) nil)))

  (testing "consed seqs"
    (let [even-42 (fn [x] (when (even? x) 42))]
      (is (true? (every? even-42
                         (cons 2 (cons 4 (seq (vector 6 8)))))))
      (is (false? (every? even-42
                          (cons 2 (cons 99 (seq (vector 6 8)))))))
      (is (false? (every? even-42
                          (cons 2 (cons 4 (seq (vector 6 99))))))))

    (let [c-42 (fn [char] (when (= #\c char) 42))]
      (is (true? (every? c-42 (cons #\c (cons #\c (seq "c"))))))

      (is (false? (every? c-42 (cons #\c (cons #\c (seq "ca"))))))
      (is (false? (every? c-42 (cons #\c (cons #\a (seq "cc"))))))))

  (testing "lists"
    (let [even-42 (fn [x] (when (even? x) 42))]
      (is (true? (every? even-42 '())))
      (is (true? (every? even-42 '(2))))
      (is (true? (every? even-42 '(2 4 6))))

      (is (false? (every? even-42 '(1))))
      (is (false? (every? even-42 '(1 3 5))))
      (is (false? (every? even-42 '(2 4 6 7))))))

  (testing "vectors"
    (let [even-42 (fn [x] (when (even? x) 42))]
      (is (true? (every? even-42 (vector))))
      (is (true? (every? even-42 (vector 2))))
      (is (true? (every? even-42 (vector 2 4 6))))

      (is (false? (every? even-42 (vector 1))))
      (is (false? (every? even-42 (vector 1 3 5))))
      (is (false? (every? even-42 (vector 2 4 6 7))))))

  (testing "vector seqs"
    (let [even-42 (fn [x] (when (even? x) 42))]
      (is (true? (every? even-42 (seq (vector 2)))))
      (is (true? (every? even-42 (seq (vector 2 4 6)))))

      (is (false? (every? even-42 (seq (vector 1)))))
      (is (false? (every? even-42 (seq (vector 1 3 5)))))
      (is (false? (every? even-42 (seq (vector 2 4 6 7)))))))

  (testing "strings"
    (let [c-42 (fn [char] (when (= #\c char) 42))]
      (is (true? (every? c-42 "")))
      (is (true? (every? c-42 "c")))
      (is (true? (every? c-42 "ccc")))

      (is (false? (every? c-42 "a")))
      (is (false? (every? c-42 "aba")))
      (is (false? (every? c-42 "cabac")))))

  (testing "string seqs"
    (let [c-42 (fn [char] (when (= #\c char) 42))]
      (is (true? (every? c-42 (seq "c"))))
      (is (true? (every? c-42 (seq "ccc"))))

      (is (false? (every? c-42 (seq "a"))))
      (is (false? (every? c-42 (seq "aba"))))
      (is (false? (every? c-42 (seq "cabac"))))))

  (testing "hash-maps"
    (let [sym-even-42
          (fn [kv] (when (and (scheme/symbol? (first kv))
                              (even? (first (next kv))))
                     42))]
      (is (true? (every? sym-even-42 (hash-map))))
      (is (true? (every? sym-even-42 (hash-map 'a 2))))
      (is (true? (every? sym-even-42 (hash-map 'a 2 'b 4 'c 6))))

      (is (false? (every? sym-even-42 (hash-map 'a 1))))
      (is (false? (every? sym-even-42 (hash-map 'a 1 'b 3 'c 5))))
      (is (false? (every? sym-even-42 (hash-map 'a 2 'b 4 'c 7))))
      (is (false? (every? sym-even-42 (hash-map 'a 2 'b 4 "c" 6)))))))



(deftest test-get
  (is (test/arity-error? (get)))

  (is (nil? (get nil 1)))
  (is (= 42 (get nil 1 42)))
  (is (nil? (get nil 'a)))
  (is (= 42 (get nil 'a 42)))

  (is (nil? (get '(1 2 3) 0)))
  (is (= 42 (get '(1 2 3) 0 42)))

  (is (nil? (get 1 0)))
  (is (= 42 (get 1 0 42)))

  (testing "vectors"
    (let [v (vector 1 2 3)]
      (is (= 1 (get v 0)))
      (is (= 2 (get v 1)))
      (is (= 3 (get v 2)))

      (is (nil? (get v -1)))
      (is (nil? (get v 3)))
      (is (nil? (get v 0.5)))
      (is (nil? (get v 'a)))

      (is (= 42 (get v -1  42)))
      (is (= 42 (get v 3   42)))
      (is (= 42 (get v 0.5 42)))
      (is (= 42 (get v 'a  42)))

      (is (test/arity-error? (get v)))
      (is (test/arity-error? (get v "too" "many" "args")))))

  (testing "hash-maps"
    (let [h (hash-map 'a 1 'b 2 'c 3)]
      (is (= 1 (get h 'a)))
      (is (= 2 (get h 'b)))
      (is (= 3 (get h 'c)))

      (is (nil? (get h 'z)))
      (is (= 42 (get h 'z 42)))

      (is (test/arity-error? (get h)))
      (is (test/arity-error? (get h "too" "many" "args")))))

  (testing "strings"
    (let [s "clj"]
      (is (= #\c (get s 0)))
      (is (= #\l (get s 1)))
      (is (= #\j (get s 2)))

      (is (nil? (get s -1)))
      (is (nil? (get s 3)))
      (is (nil? (get s 0.5)))
      (is (nil? (get s 'a)))

      (is (= 42 (get s -1  42)))
      (is (= 42 (get s 3   42)))
      (is (= 42 (get s 0.5 42)))
      (is (= 42 (get s 'a  42)))

      (is (test/arity-error? (get s)))
      (is (test/arity-error? (get s "too" "many" "args")))))

  (testing "sets"
    (let [hs (hash-set 1 'a 'b 'c 1 2 'c 3)]
      (is (= 'a (get hs 'a)))
      (is (= 'b (get hs 'b)))
      (is (= 'c (get hs 'c)))
      (is (= 1  (get hs 1)))
      (is (= 2  (get hs 2)))
      (is (= 3  (get hs 3)))

      (is (nil? (get hs 'z)))
      (is (= 42 (get hs 'z 42))))

    (let [a   (hash-set 1 2 3)
          b   (hash-set 1 2 3)
          hs  (hash-set a)
          got (get hs b)]
      (is (not (identical? a b)))
      (is (identical? got a)
          "should return the found object")
      (is (not (identical? got b))
          "should not return the query object"))

    (is (test/arity-error? (get (hash-set))))
    (is (test/arity-error? (get (hash-set) "too" "many" "args")))))



(deftest test-not-any?
  (is (test/arity-error? (not-any?)))

  (is (test/type-error? (not-any? true? true)))
  (is (test/type-error? (not-any? true? false)))
  (is (test/type-error? (not-any? true? 'a)))
  (is (test/type-error? (not-any? true? 1)))

  (is (true? (not-any? (fn [x] (/ 1 0)) nil)))

  (let [even-42 (fn [x] (when (even? x) 42))]
    (is (true? (not-any? even-42 '(1 3 5))))
    (is (false? (not-any? even-42 '(1 3 6))))
    (is (false? (not-any? even-42 '(2 4 6))))

    (is (true? (not-any? even-42 (vector 1 3 5))))
    (is (false? (not-any? even-42 (vector 1 3 6))))
    (is (false? (not-any? even-42 (vector 2 4 6))))

    (is (true? (not-any? even-42 (seq (vector 1 3 5)))))
    (is (false? (not-any? even-42 (seq (vector 1 3 6)))))
    (is (false? (not-any? even-42 (seq (vector 2 4 6)))))

    (is (true? (not-any? even-42 (cons 1 (seq (vector 3 5))))))
    (is (false? (not-any? even-42 (cons 1 (seq (vector 3 6))))))
    (is (false? (not-any? even-42 (cons 2 (seq (vector 3 5)))))))

  (let [c-42 (fn [char] (when (= #\c char) 42))]
    (is (true? (not-any? c-42 "lj")))
    (is (false? (not-any? c-42 "clj")))
    (is (false? (not-any? c-42 "ccc")))

    (is (true? (not-any? c-42 (seq "lj"))))
    (is (false? (not-any? c-42 (seq "clj"))))
    (is (false? (not-any? c-42 (seq "ccc"))))

    (is (true? (not-any? c-42 (cons #\l (seq "j")))))
    (is (false? (not-any? c-42 (cons #\c (seq "lj")))))
    (is (false? (not-any? c-42 (cons #\j (seq "lc"))))))

  (let [sym-even-42
        (fn [kv] (when (and (scheme/symbol? (first kv))
                            (even? (first (next kv))))
                   42))]
    (is (true? (not-any? sym-even-42 (hash-map "a" 1 "b" 3))))
    (is (true? (not-any? sym-even-42 (hash-map "a" 1 "b" 2))))
    (is (false? (not-any? sym-even-42 (hash-map "a" 1 'b 2))))))



(deftest test-not-empty
  (is (test/arity-error? (not-empty)))

  (is (test/type-error? (not-empty true)))
  (is (test/type-error? (not-empty false)))
  (is (test/type-error? (not-empty 'a)))
  (is (test/type-error? (not-empty 1)))

  (is (nil? (not-empty nil)))

  (testing "lists"
    (is (nil? (not-empty '())))
    (is (= '(1) (not-empty '(1)))))

  (testing "vectors"
    (is (nil? (not-empty (vector))))
    (is (= (vector 1) (not-empty (vector 1)))))

  (testing "strings"
    (is (nil? (not-empty "")))
    (is (= "c" (not-empty "c"))))

  (testing "hash-maps"
    (is (nil? (not-empty (hash-map))))
    (is (= (hash-map 'a 1) (not-empty (hash-map 'a 1)))))

  (testing "hash-sets"
    (is (nil? (not-empty (hash-set))))
    (is (= (hash-set 'a 'b) (not-empty (hash-set 'a 'b))))))



(deftest test-not-every?
  (is (test/arity-error? (not-every?)))

  (is (test/type-error? (not-every? true? true)))
  (is (test/type-error? (not-every? true? false)))
  (is (test/type-error? (not-every? true? 'a)))
  (is (test/type-error? (not-every? true? 1)))

  (is (false? (not-every? (fn [x] (/ 1 0)) nil)))

  (let [even-42 (fn [x] (when (even? x) 42))]
    (is (false? (not-every? even-42 '(2 4 6))))
    (is (true? (not-every? even-42 '(2 4 6 7))))

    (is (false? (not-every? even-42 (vector 2 4 6))))
    (is (true? (not-every? even-42 (vector 2 4 6 7))))

    (is (false? (not-every? even-42 (seq (vector 2 4 6)))))
    (is (true? (not-every? even-42 (seq (vector 2 4 6 7)))))

    (is (false? (not-every? even-42 (cons 2 (seq (vector 4 6))))))
    (is (true? (not-every? even-42 (cons 2 (seq (vector 4 99)))))))

  (let [c-42 (fn [char] (when (= #\c char) 42))]
    (is (false? (not-every? c-42 "ccc")))
    (is (true? (not-every? c-42 "cabac")))

    (is (false? (not-every? c-42 (seq "ccc"))))
    (is (true? (not-every? c-42 (seq "cabac"))))

    (is (false? (not-every? c-42 (cons #\c (cons #\c (seq "c"))))))
    (is (true? (not-every? c-42 (cons #\c (cons #\c (seq "ca")))))))

  (let [sym-even-42
        (fn [kv] (when (and (scheme/symbol? (first kv))
                            (even? (first (next kv))))
                   42))]
    (is (false? (not-every? sym-even-42 (hash-map 'a 2 'b 4 'c 6))))
    (is (true? (not-every? sym-even-42 (hash-map 'a 1 'b 3 'c 5))))))



(deftest test-some
  (is (test/arity-error? (some)))

  (is (test/type-error? (some true? true)))
  (is (test/type-error? (some true? false)))
  (is (test/type-error? (some true? 'a)))
  (is (test/type-error? (some true? 1)))

  (is (nil? (some (fn [x] (/ 1 0)) nil)))

  (let [fizzbuzz (fn [x] (cond
                          (= 0 (scheme/modulo x 5))  'buzz
                          (= 0 (scheme/modulo x 3))  'fizz))
        vowel (fn [c] (when (or (= c #\a) (= c #\e) (= c #\i)
                                (= c #\o) (= c #\u))
                        c))
        key-if-even (fn [kv] (when (even? (first (next kv)))
                               (first kv)))]
    (testing "consed seqs"
      (is (= 'fizz (some fizzbuzz (cons 2 (seq (vector 6 15))))))
      (is (= 'buzz (some fizzbuzz (cons 5 (seq (vector 6 15))))))
      (is (nil? (some fizzbuzz (cons 2 (seq (vector 4 8)))))))

    (testing "lists"
      (is (= 'fizz (some fizzbuzz '(2 6 15))))
      (is (= 'buzz (some fizzbuzz '(5 6 15))))
      (is (nil? (some fizzbuzz '(2 4 8))))
      (is (nil? (some fizzbuzz '()))))

    (testing "vectors"
      (is (= 'fizz (some fizzbuzz (vector 2 6 15))))
      (is (= 'buzz (some fizzbuzz (vector 5 6 15))))
      (is (nil? (some fizzbuzz (vector 2 4 8))))
      (is (nil? (some fizzbuzz (vector)))))

    (testing "vector seqs"
      (is (= 'fizz (some fizzbuzz (seq (vector 2 6 15)))))
      (is (= 'buzz (some fizzbuzz (seq (vector 5 6 15)))))
      (is (nil? (some fizzbuzz (seq (vector 2 4 8)))))
      (is (nil? (some fizzbuzz (seq (vector)))))

      (is (= 'fizz (some fizzbuzz (cons 2 (seq (vector 6 15))))))
      (is (= 'buzz (some fizzbuzz (cons 5 (seq (vector 6 15))))))
      (is (nil? (some fizzbuzz (cons 2 (seq (vector 4 8)))))))

    (testing "strings"
      (is (nil? (some vowel "")))
      (is (nil? (some vowel "clj")))
      (is (= #\o (some vowel "cloje")))
      (is (= #\e (some vowel "clje"))))

    (testing "string seqs"
      (is (nil? (some vowel (seq "clj"))))
      (is (= #\o (some vowel (seq "cloje"))))
      (is (= #\e (some vowel (seq "clje"))))

      (is (nil? (some vowel (cons #\c (seq "lj")))))
      (is (= #\o (some vowel (cons #\c (seq "loje")))))
      (is (= #\e (some vowel (cons #\e (seq "clj"))))))

    (testing "hash-maps"
      (is (= 'a (some key-if-even (hash-map 'a 2))))
      (is (= 'b (some key-if-even (hash-map 'a 1 'b 2 'c 5))))
      (is (nil? (some key-if-even (hash-map))))
      (is (nil? (some key-if-even (hash-map 'a 1 'b 3 'c 5)))))))



(deftest test-contains?
  (is (test/arity-error? (contains?)))
  (is (test/arity-error? (contains? (vector 10))))
  (is (test/arity-error? (contains? (vector 10) 1 2)))

  (is (test/type-error? (contains? (list 10) 0)))
  (is (test/type-error? (contains? (seq (vector 10)) 0)))
  (is (test/type-error? (contains? (seq "abc") 0)))
  (is (test/type-error? (contains? (seq (hash-map 'a 10)) 'a)))
  (is (test/type-error? (contains? (seq (hash-map 'a 10)) 0)))

  (is (test/type-error? (contains? true 0)))
  (is (test/type-error? (contains? false 0)))
  (is (test/type-error? (contains? nil 0)))
  (is (test/type-error? (contains? 'a 0)))
  (is (test/type-error? (contains? '#\a 0)))
  (is (test/type-error? (contains? '#:a 0)))

  (testing "vectors"
    (is (false? (contains? (vector 10 11 12) -1)))
    (is (true?  (contains? (vector 10 11 12)  0)))
    (is (true?  (contains? (vector 10 11 12)  1)))
    (is (true?  (contains? (vector 10 11 12)  2)))
    (is (false? (contains? (vector 10 11 12)  3)))

    (is (false? (contains? (vector 10 11 12) 10)))
    (is (false? (contains? (vector 10 11 12) 11)))
    (is (false? (contains? (vector 10 11 12) 12)))

    (is (false? (contains? (vector 10 11 12) true)))
    (is (false? (contains? (vector 10 11 12) false)))
    (is (false? (contains? (vector 10 11 12) nil)))
    (is (false? (contains? (vector 10 11 12) 'a)))
    (is (false? (contains? (vector 10 11 12) "a")))
    (is (false? (contains? (vector 10 11 12) #\a))))

  (testing "strings"
    (is (false? (contains? "abc" -1)))
    (is (true?  (contains? "abc"  0)))
    (is (true?  (contains? "abc"  1)))
    (is (true?  (contains? "abc"  2)))
    (is (false? (contains? "abc"  3)))

    (is (false? (contains? "abc" true)))
    (is (false? (contains? "abc" false)))
    (is (false? (contains? "abc" nil)))
    (is (false? (contains? "abc" #\a)))
    (is (false? (contains? "abc" "a")))
    (is (false? (contains? "abc" 'a))))

  (testing "hash-maps"
    (is (true?  (contains? (hash-map 'a 10 '#:b "11") 'a)))
    (is (true?  (contains? (hash-map 'a 10 '#:b "11") '#:b)))

    (is (false? (contains? (hash-map 'a 10 '#:b "11") 0)))
    (is (false? (contains? (hash-map 'a 10 '#:b "11") 10)))
    (is (false? (contains? (hash-map 'a 10 '#:b "11") "11")))
    (is (false? (contains? (hash-map 'a 10 '#:b "11") true)))
    (is (false? (contains? (hash-map 'a 10 '#:b "11") false)))
    (is (false? (contains? (hash-map 'a 10 '#:b "11") nil))))

  (testing "hash-sets"
    (let [hs (hash-set 'a "b" (char 99) (keyword "d")
                       true false nil
                       (list 1 2 3)
                       (vector 4 5 6)
                       (hash-map 'a 1 'b 2)
                       (hash-set 'c 'l 'j))]
      (is (true? (contains? hs 'a)))
      (is (true? (contains? hs "b")))
      (is (true? (contains? hs (char 99))))
      (is (true? (contains? hs (keyword "d"))))
      (is (true? (contains? hs true)))
      (is (true? (contains? hs false)))
      (is (true? (contains? hs nil)))
      (is (true? (contains? hs (list 1 2 3))))
      (is (true? (contains? hs (vector 4 5 6))))
      (is (true? (contains? hs (hash-map 'a 1 'b 2))))
      (is (true? (contains? hs (hash-set 'c 'l 'j))))
      (is (true? (contains? hs (hash-set 'j 'l 'c 'l 'j))))

      (is (false? (contains? hs 'z)))
      (is (false? (contains? hs "y")))
      (is (false? (contains? hs (char 100))))
      (is (false? (contains? hs (keyword "e"))))
      (is (false? (contains? hs (list 3 2 1))))
      (is (false? (contains? hs (vector 6 5 4))))
      (is (false? (contains? hs (hash-map 'a 1 'b 999))))
      (is (false? (contains? hs (hash-set 'c 'l 'o 'j 'e)))))))



(deftest test-.contains
  (is (test/arity-error? (.contains)))
  (is (test/arity-error? (.contains "foo")))
  (is (test/arity-error? (.contains "too" "many" "args")))

  (is (test/type-error? (.contains nil "f")))
  (is (test/type-error? (.contains (hash-map 'a 1) (vector 'a 1))))

  (testing "lists"
    (is (true?  (.contains '(a 1 a) 'a)))
    (is (true?  (.contains '(a 1 a) 1)))

    (is (true?  (.contains (list true false nil) true)))
    (is (true?  (.contains (list true false nil) false)))
    (is (true?  (.contains (list true false nil) nil)))

    (is (false? (.contains '(a 1 a) 'b)))
    (is (false? (.contains '(a 1 a) "a")))
    (is (false? (.contains '(a 1 a) '#:a)))
    (is (false? (.contains '(a 1 a) #\a)))
    (is (false? (.contains '(a 1 a) nil)))
    (is (false? (.contains '(a 1 a) 1.0)))
    (is (false? (.contains '(a 1 a) '(a 1 a))))
    (is (false? (.contains '(a 1 a) '(a))))
    (is (false? (.contains '() 'a))))

  (testing "vectors"
    (is (true?  (.contains (vector 'a 1 'a) 'a)))
    (is (true?  (.contains (vector 'a 1 'a) 1)))

    (is (true?  (.contains (vector true false nil) true)))
    (is (true?  (.contains (vector true false nil) false)))
    (is (true?  (.contains (vector true false nil) nil)))

    (is (false? (.contains (vector 'a 1 'a) 'b)))
    (is (false? (.contains (vector 'a 1 'a) "a")))
    (is (false? (.contains (vector 'a 1 'a) '#:a)))
    (is (false? (.contains (vector 'a 1 'a) #\a)))
    (is (false? (.contains (vector 'a 1 'a) nil)))
    (is (false? (.contains (vector 'a 1 'a) 1.0)))
    (is (false? (.contains (vector 'a 1 'a) (vector 'a 1 'a))))
    (is (false? (.contains (vector 'a 1 'a) (vector 'a))))
    (is (false? (.contains (vector 'a 1 'a) '(a 1 a))))
    (is (false? (.contains (vector 'a 1 'a) '(a))))
    (is (false? (.contains (vector) 'a))))

  (testing "strings"
    (is (true?  (.contains "foo" "foo")))
    (is (true?  (.contains "foo" "")))
    (is (true?  (.contains "foo" "f")))
    (is (true?  (.contains "foo" "o")))
    (is (true?  (.contains "foo" "oo")))

    (is (false? (.contains "foo" "bar")))
    (is (false? (.contains "" "bar")))

    (is (test/type-error? (.contains "foo" (seq "f"))))
    (is (test/type-error? (.contains "foo" #\f)))
    (is (test/type-error? (.contains "foo" 'f)))
    (is (test/type-error? (.contains "foo" nil))))

  (testing "seqs"
    (is (true?  (.contains (seq (vector 'a 1 'a)) 'a)))
    (is (true?  (.contains (seq (vector 'a 1 'a)) 1)))
    (is (false? (.contains (seq (vector 'a 1 'a)) 'b)))

    (is (true?  (.contains (cons 'b (vector 'a 1 'a)) 'a)))
    (is (true?  (.contains (cons 'b (vector 'a 1 'a)) 1)))
    (is (true?  (.contains (cons 'b (vector 'a 1 'a)) 'b)))
    (is (false? (.contains (cons 'b (vector 'a 1 'a)) 'c)))

    (is (false? (.contains (seq "foo") "f")))
    (is (true?  (.contains (seq "foo") #\f)))
    (is (true?  (.contains (seq "foo") #\o)))

    (is (false? (.contains (cons #\f "foo") "f")))
    (is (true?  (.contains (cons #\f "foo") #\f)))
    (is (true?  (.contains (cons #\f "foo") #\o)))

    (is (true?  (.contains (seq (hash-map 'a 1)) (vector 'a 1))))
    (is (false? (.contains (seq (hash-map 'a 1)) (vector 'a 2)))))

  (testing "hash-sets"
    (let [hs (hash-set 'a "b" (char 99) (keyword "d")
                       true false nil
                       (list 1 2 3)
                       (vector 4 5 6)
                       (hash-map 'a 1 'b 2)
                       (hash-set 'c 'l 'j))]
      (is (true? (.contains hs 'a)))
      (is (true? (.contains hs "b")))
      (is (true? (.contains hs (char 99))))
      (is (true? (.contains hs (keyword "d"))))
      (is (true? (.contains hs true)))
      (is (true? (.contains hs false)))
      (is (true? (.contains hs nil)))
      (is (true? (.contains hs (list 1 2 3))))
      (is (true? (.contains hs (vector 4 5 6))))
      (is (true? (.contains hs (hash-map 'a 1 'b 2))))
      (is (true? (.contains hs (hash-set 'c 'l 'j))))
      (is (true? (.contains hs (hash-set 'j 'l 'c 'l 'j))))

      (is (false? (.contains hs 'z)))
      (is (false? (.contains hs "y")))
      (is (false? (.contains hs (char 100))))
      (is (false? (.contains hs (keyword "e"))))
      (is (false? (.contains hs (list 3 2 1))))
      (is (false? (.contains hs (vector 6 5 4))))
      (is (false? (.contains hs (hash-map 'a 1 'b 999))))
      (is (false? (.contains hs (hash-set 'c 'l 'o 'j 'e)))))))
