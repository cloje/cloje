
(deftest test-*cloje-version*
  (let [major       (get *cloje-version* (keyword "major"))
        minor       (get *cloje-version* (keyword "minor"))
        incremental (get *cloje-version* (keyword "incremental"))
        qualifier   (get *cloje-version* (keyword "qualifier"))]
    (is (and (integer? major) (not (neg? major))))
    (is (and (integer? minor) (not (neg? minor))))
    (is (and (integer? incremental) (not (neg? incremental))))
    (is (or (nil? qualifier) (string? qualifier)))))



(deftest test-cloje-version
  (is (string? (cloje-version)))
  (let [re (re-pattern
            (str "([0-9]+)"                 ; major
                 ".([0-9]+)"                ; minor
                 ".([0-9]+)"                ; incremental
                 "(?:-([-.0-9A-Za-z]+))?")) ; qualifier
        groups (re-matches re (cloje-version))]

    (is groups
        "should return a string matching the expected pattern")

    (let [major       (get *cloje-version* (keyword "major"))
          minor       (get *cloje-version* (keyword "minor"))
          incremental (get *cloje-version* (keyword "incremental"))
          qualifier   (get *cloje-version* (keyword "qualifier"))]
      (is (= (get groups 1) (str major)))
      (is (= (get groups 2) (str minor)))
      (is (= (get groups 3) (str incremental)))
      (is (= (get groups 4) qualifier)))))
