
(def set-test-examples
  (hash-map 'colls
            (let [base "abc"]
              (list base
                    (into (list) base)
                    (into (list) (str base base))
                    (into (vector) base)
                    (into (vector) (str base base))
                    (into (hash-set) base)))
            ;; Subsets of colls
            'subs
            (let [base "ab"]
              (list base
                    (into (list) base)
                    (into (list) (str base base))
                    (into (vector) base)
                    (into (vector) (str base base))
                    (into (hash-set) base)))
            ;; Different from colls (mutually non-subsets)
            'diffs
            (let [base "cde"]
              (list base
                    (into (list) base)
                    (into (list) (str base base))
                    (into (vector) base)
                    (into (vector) (str base base))
                    (into (hash-set) base)))
            ;; Empty collections
            'empts
            (list nil
                  (list)
                  (vector)
                  (hash-set)
                  (hash-map)
                  "")))

;;; Hash-map based examples
(def set-test-hm-examples
  (hash-map 'colls
            (let [base (hash-map 'a 1 'b 2 'c 3)]
              (list base
                    (into (list) base)
                    (into (vector) base)
                    (into (hash-set) base)))
            'subs
            (let [base (hash-map 'a 1 'b 2)]
              (list base
                    (into (list) base)
                    (into (vector) base)
                    (into (hash-set) base)))
            'diffs
            (let [base (hash-map 'b 4 'c 3 'd 5)]
              (list base
                    (into (list) base)
                    (into (vector) base)
                    (into (hash-set) base)))))



(deftest test-hash-set
  (is (set? (hash-set)))
  (is (set? (hash-set 1)))
  (is (set? (hash-set 1 2 3)))
  (is (set? (hash-set 1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1)))
  (is (set? (hash-set 'a (keyword "b") (char 99) "cloje"
                      true false nil
                      (list 1 2 3)
                      (vector 1 2 3)
                      (hash-map 'a 1 'b 2)))))



(deftest test-set?
  (is (true? (set? (hash-set))))
  (is (true? (set? (hash-set 1 2 3 4 5))))

  (is (false? (set? 'a)))
  (is (false? (set? (keyword "b"))))
  (is (false? (set? (char 99))))
  (is (false? (set? "cloje")))
  (is (false? (set? true)))
  (is (false? (set? false)))
  (is (false? (set? nil)))
  (is (false? (set? (list 1 2 3))))
  (is (false? (set? (hash-map 'a 1 'b 2))))
  (is (false? (set? (vector 1 2 3 4 5)))))



(deftest test-set-equality
  (let [hs1  (hash-set)
        hs2a (hash-set 1 2)
        hs2b (hash-set 2 1)
        hs3a (hash-set 1 2 3)
        hs3b (hash-set 3 2 1)
        hs3c (hash-set 1 3 1 2 2 3 2 1)
        hs4a (hash-set "1" "2" "3")
        hs4b (hash-set "3" "1" "2" "3" "2" "1")]
    (is (true? (= hs1 hs1)))
    (is (true? (= hs1 (hash-set))))

    (is (true? (= hs2a hs2a)))
    (is (true? (= hs2a hs2b)))
    (is (true? (= hs2a (hash-set 1 2))))

    (is (true? (= hs3a hs3b)))
    (is (true? (= hs3a hs3c)))
    (is (true? (= hs3b hs3c)))
    (is (true? (= hs3a hs3b hs3c)))
    (is (true? (= hs3a (hash-set 1 2 3))))

    (is (true? (= hs4a hs4b)))
    (is (true? (= hs4a (hash-set "1" "2" "3"))))

    (is (false? (= hs1 hs2a)))
    (is (false? (= hs1 hs3a)))
    (is (false? (= hs1 hs4a)))
    (is (false? (= hs2a hs4a)))
    (is (false? (= hs2a hs3a)))
    (is (false? (= hs3a hs4a)))

    (is (true? (identical? hs1 hs1)))
    (is (false? (identical? hs1 (hash-set))))

    (is (true? (identical? hs2a hs2a)))
    (is (false? (identical? hs2a hs2b)))
    (is (false? (identical? hs2a (hash-set 1 2)))))

  (is (false? (= (hash-set) true)))
  (is (false? (= (hash-set) false)))
  (is (false? (= (hash-set) nil)))

  (is (false? (= (hash-set) (list))))
  (is (false? (= (hash-set) (vector))))
  (is (false? (= (hash-set) (hash-map))))

  (is (false? (= (hash-set 1 2 3) (list 1 2 3))))
  (is (false? (= (hash-set 1 2 3) (vector 1 2 3))))
  (is (false? (= (hash-set 1 2 3) (hash-map 1 true 2 true 3 true))))
  (is (false? (= (hash-set (vector 1 true) (vector 2 true) (vector 3 true))
                 (hash-map 1 true 2 true 3 true)))))



(deftest test-set
  (is (= (hash-set) (set nil)))

  (is (= (hash-set) (set (hash-set))))
  (is (= (hash-set 1 2 3) (set (hash-set 1 2 3))))

  (is (= (hash-set) (set (list))))
  (is (= (hash-set 1 2 3) (set (list 1 2 3))))

  (is (= (hash-set) (set (vector))))
  (is (= (hash-set 1 2 3) (set (vector 1 2 3))))

  (is (= (hash-set) (set (hash-map))))
  (is (= (hash-set (vector 'a 1) (vector 'b 2))
         (set (hash-map 'a 1 'b 2))))

  (is (test/arity-error? (set)))
  (is (test/arity-error? (set '(too) '(many))))

  (is (test/type-error? (set true)))
  (is (test/type-error? (set false)))
  (is (test/type-error? (set 1)))
  (is (test/type-error? (set 'a))))



(deftest test-disj
  (is (= (hash-set) (disj (hash-set) 'c)))
  (is (= (hash-set) (disj (hash-set 'c)
                          'a 'b 'c)))
  (is (= (hash-set) (disj (hash-set 'c 'l 'j)
                          'c 'l 'o 'j 'e)))
  (is (= (hash-set 'o 'e) (disj (hash-set 'c 'l 'o 'j 'e)
                                'c 'l 'j)))
  (is (= (hash-set 'c 'l 'j) (disj (hash-set 'c 'l 'j)
                                   'x 'y 'z)))
  (is (= (hash-set 'c 'l 'j) (disj (hash-set 'c 'l 'j))))

  (is (nil? (disj nil 'a)))

  (is (test/type-error? (disj (list) 'a)))
  (is (test/type-error? (disj (vector) 'a)))
  (is (test/type-error? (disj (hash-map) 'a))))



(deftest test-set/union
  (is (= (hash-set) (set/union)))
  (is (= (hash-set 1 2) (set/union (hash-set 1 2))))
  (is (= (hash-set 1 2 3 4) (set/union (hash-set 1 2)
                                       (hash-set 2 3)
                                       (hash-set 3 4))))

  (is (= (hash-set 1 2) (set/union (hash-set 1 2) nil)))

  (is (test/type-error? (hash-set) (set/union 1)))
  (is (test/type-error? (hash-set) (set/union (hash-set) 1)))

  (testing "with non-set collections"
    ;; NOTE: These behaviors are intentionally different from Clojure.
    ;; See docs/discrepancies.md
    (is (= (hash-set) (set/union nil)))
    (is (= (hash-set) (set/union (list))))
    (is (= (hash-set) (set/union (vector))))
    (is (= (hash-set) (set/union (hash-map))))
    (is (= (hash-set) (set/union "")))

    (is (= (hash-set 1 2) (set/union (list 1 2))))
    (is (= (hash-set 1 2) (set/union (vector 1 2))))
    (is (= (hash-set (vector 'a 1) (vector 'b 2))
           (set/union (hash-map 'a 1 'b 2))))
    (is (= (hash-set (char 97) (char 98))
           (set/union "ab")))

    (is (= (hash-set 1 2 3 4
                     (vector 'a 1) (vector 'b 2) (vector 'c 3)
                     (char 97) (char 98))
           (set/union (list 1 2 3)
                      (vector 2 3 4)
                      (hash-map 'a 1 'b 2)
                      (hash-map 'b 2 'c 3)
                      "ab")))))



(deftest test-set/difference
  (is (= (hash-set 1 2 3)
         (set/difference (hash-set 1 2 3))))

  (is (= (hash-set 1 2 3)
         (set/difference (hash-set 1 2 3) (hash-set))))
  (is (= (hash-set)
         (set/difference (hash-set) (hash-set 1 2 3))))

  (is (= (hash-set 1 3 5)
         (set/difference
          (hash-set 1 2 3 4 5 6 7 8 9)
          (hash-set 2 4 6)
          (hash-set 7 8 9))))

  (is (test/arity-error? (set/difference)))
  (is (test/type-error? (set/difference (hash-set 1) 1)))
  (is (test/type-error? (set/difference 1 (hash-set 1))))

  (testing "with non-set collections"
    ;; NOTE: These behaviors are intentionally different from Clojure.
    ;; See docs/discrepancies.md
    (is (= (hash-set) (set/difference nil)))
    (is (= (hash-set) (set/difference (list))))
    (is (= (hash-set) (set/difference (vector))))
    (is (= (hash-set) (set/difference (hash-map))))
    (is (= (hash-set) (set/difference "")))

    (let [parts (list (list 1 2 3)
                      (vector 2 3 4)
                      (hash-map 'a 1 'b 2)
                      (hash-map 'b 2 'c 3)
                      "ab")
          extra (hash-set 'c 'l 'o 'j 'e)]
      (is (= extra
             (apply set/difference
                    (apply set/union extra parts)
                    parts)))
      (is (= extra
             (apply set/difference
                    (into '() (apply set/union extra parts))
                    parts)))
      (is (= extra
             (apply set/difference
                    (into (vector) (apply set/union extra parts))
                    parts))))

    (is (= (hash-set (vector 'a 1))
           (set/difference (hash-map 'a 1 'b 2 'c 3)
                           (hash-map 'b 2)
                           (list (vector 'c 3)))))

    (is (= (set "je")
           (set/difference "so cloje" "is" "kinda cool")))

    ;; Examples from docs/discrepancies.md
    (is (= (hash-set 2 4)
           (set/difference (list 1 2 3 4 5)
                           (list 1 3) (vector 3 5))))
    (is (= (hash-set 2 4)
           (set/difference (set (list 1 2 3 4 5))
                           (list 1 3) (vector 3 5))))))



(deftest test-set/intersection
  (is (test/arity-error? (set/intersection)))
  (is (test/type-error? (set/intersection (hash-set 1) 1)))
  (is (test/type-error? (set/intersection 1 (hash-set 1))))

  (is (= (hash-set 1 2 3)
         (set/intersection (hash-set 1 2 3))))

  (is (= (hash-set 1 3 5)
         (set/intersection
          (hash-set 1 2 3 4 5)
          (hash-set 1 3 5 7 9)
          (hash-set 0 1 1 2 3 5 8))))

  (testing "with non-set collections"
    ;; NOTE: These behaviors are intentionally different from Clojure.
    ;; See docs/discrepancies.md
    (is (= (hash-set) (set/intersection nil)))
    (is (= (hash-set) (set/intersection (list))))
    (is (= (hash-set) (set/intersection (vector))))
    (is (= (hash-set) (set/intersection (hash-map))))
    (is (= (hash-set) (set/intersection "")))

    (is (= (hash-set 1 2) (set/intersection (list 1 2))))
    (is (= (hash-set 1 2) (set/intersection (vector 1 2))))
    (is (= (hash-set (vector 'a 1) (vector 'b 2))
           (set/intersection (hash-map 'a 1 'b 2))))
    (is (= (hash-set (char 97) (char 98))
           (set/intersection "ab")))

    (is (= (hash-set 1 3 5)
           (set/intersection
            (list 1 2 3 4 5)
            (list 1 3 5 7 9)
            (vector 0 1 1 2 3 5 8))))

    (is (= (hash-set 1 3 5)
           (set/intersection
            (list 1 2 3 4 5)
            (hash-set 1 3 5 7 9)
            (vector 0 1 1 2 3 5 8))))

    (is (= (hash-set)
           (set/intersection
            (list 1 2 3 4 5)
            (list))))

    (is (= (hash-set)
           (set/intersection
            (list 1 2 3 4 5)
            nil)))))


(deftest test-set/select
  (is (test/arity-error? (set/select)))
  (is (test/arity-error? (set/select even?)))
  (is (test/arity-error? (set/select symbol? '(too many) 'args)))

  (is (test/type-error? (set/select (hash-set 1) even?)))

  (is (= (hash-set 1 3)
         (set/select odd? (hash-set 1 2 3))))
  (is (= (hash-set 1 2 3)
         (set/select (fn [x] true) (hash-set 1 2 3))))
  (is (= (hash-set 1 2 3)
         (set/select (fn [x] x) (hash-set 1 2 3))))
  (is (= (hash-set)
         (set/select (fn [x] false) (hash-set 1 2 3))))
  (is (= (hash-set)
         (set/select (fn [x] nil) (hash-set 1 2 3))))

  (testing "with a non-set collection"
    ;; NOTE: These behaviors are intentionally different from Clojure.
    ;; See docs/discrepancies.md

    (is (= (hash-set 1 3)
           (set/select odd? (list 1 2 3))))
    (is (= (hash-set 1 2 3)
           (set/select (fn [x] true) (list 1 2 3))))
    (is (= (hash-set 1 2 3)
           (set/select (fn [x] x) (list 1 2 3))))
    (is (= (hash-set)
           (set/select (fn [x] false) (list 1 2 3))))
    (is (= (hash-set)
           (set/select (fn [x] nil) (list 1 2 3))))

    (is (= (hash-set 1 3)
           (set/select odd? (vector 1 2 3))))
    (is (= (hash-set 1 2 3)
           (set/select (fn [x] true) (vector 1 2 3))))
    (is (= (hash-set 1 2 3)
           (set/select (fn [x] x) (vector 1 2 3))))
    (is (= (hash-set)
           (set/select (fn [x] false) (vector 1 2 3))))
    (is (= (hash-set)
           (set/select (fn [x] nil) (vector 1 2 3))))

    (is (= (hash-set (vector 'a 1))
           (set/select (fn [kv] (odd? (fnext kv)))
                       (hash-map 'a 1 'b 2))))
    (is (= (hash-set (vector 'a 1) (vector 'b 2))
           (set/select (fn [x] true) (hash-map 'a 1 'b 2))))
    (is (= (hash-set (vector 'a 1) (vector 'b 2))
           (set/select (fn [x] x) (hash-map 'a 1 'b 2))))
    (is (= (hash-set)
           (set/select (fn [x] false) (hash-map 'a 1 'b 2))))
    (is (= (hash-set)
           (set/select (fn [x] nil) (hash-map 'a 1 'b 2))))

    (is (= (hash-set (char 98))
           (set/select (fn [x] (= x (char 98))) "abc")))
    (is (= (hash-set (char 97) (char 98) (char 99))
           (set/select (fn [x] true) "abc")))
    (is (= (hash-set (char 97) (char 98) (char 99))
           (set/select (fn [x] x) "abc")))
    (is (= (hash-set)
           (set/select (fn [x] false) "abc")))
    (is (= (hash-set)
           (set/select (fn [x] nil) "abc")))

    ;; Examples from docs/discrepancies.md
    (is (= (hash-set 2 4)
           (set/select even? (list 2 4))))
    (is (= (hash-set 2 4)
           (set/select even? (list 1 2 3 4))))
    (is (= (hash-set 2 4)
           (set/select even? (set (list 1 2 3 4)))))))



(deftest test-set/subset?
  (is (test/arity-error? (set/subset?)))
  (is (test/arity-error? (set/subset? (hash-set 1))))
  (is (test/arity-error?
       (set/subset? (hash-set) (hash-set) (hash-set))))

  (is (test/type-error? (set/subset? (hash-set 1) 1)))
  (is (test/type-error? (set/subset? 1 (hash-set 1))))

  (let [colls (get set-test-examples 'colls)
        subs  (get set-test-examples 'subs)
        diffs (get set-test-examples 'diffs)
        empts (get set-test-examples 'empts)]
    (map (fn [coll]
           ;; coll vs coll (mutual subsets)
           (map (fn [coll2]
                  (is (true? (set/subset? coll coll2))
                      (str (list 'set/subset? coll coll2)))
                  (is (true? (set/subset? coll2 coll))
                      (str (list 'set/subset? coll2 coll))))
                colls)
           ;; coll vs sub-coll (one-way subsets)
           (map (fn [sub]
                  (is (true? (set/subset? sub coll))
                      (str (list 'set/subset? sub coll)))
                  (is (false? (set/subset? coll sub))
                      (str (list 'set/subset? coll sub))))
                subs)
           ;; coll vs different-coll (mutual non-subsets)
           (map (fn [diff]
                  (is (false? (set/subset? diff coll))
                      (str (list 'set/subset? diff coll)))
                  (is (false? (set/subset? coll diff))
                      (str (list 'set/subset? coll diff))))
                diffs)
           ;; coll vs empty-coll (one-way subsets)
           (map (fn [empt]
                  (is (true? (set/subset? empt coll))
                      (str (list 'set/subset? empt coll)))
                  (is (false? (set/subset? coll empt))
                      (str (list 'set/subset? coll empt))))
                empts))
         colls)
    (map (fn [empt]
           ;; empty-coll vs empty-coll (mutual subsets)
           (map (fn [empt2]
                  (is (true? (set/subset? empt empt2))
                      (str (list 'set/subset? empt empt2)))
                  (is (true? (set/subset? empt2 empt))
                      (str (list 'set/subset? empt2 empt))))
                empts))
         empts))

  (let [colls (get set-test-hm-examples 'colls)
        subs  (get set-test-hm-examples 'subs)
        diffs (get set-test-hm-examples 'diffs)
        empts (get set-test-examples 'empts)]
    (map (fn [coll]
           ;; hm-coll vs hm-coll (mutual subsets)
           (map (fn [coll2]
                  (is (true? (set/subset? coll coll2))
                      (str (list 'set/subset? coll coll2)))
                  (is (true? (set/subset? coll2 coll))
                      (str (list 'set/subset? coll2 coll))))
                colls)
           ;; hm-coll vs hm-sub-coll (one-way subsets)
           (map (fn [sub]
                  (is (true? (set/subset? sub coll))
                      (str (list 'set/subset? sub coll)))
                  (is (false? (set/subset? coll sub))
                      (str (list 'set/subset? coll sub))))
                subs)
           ;; hm-coll vs hm-different-coll (mutual non-subsets)
           (map (fn [diff]
                  (is (false? (set/subset? diff coll))
                      (str (list 'set/subset? diff coll)))
                  (is (false? (set/subset? coll diff))
                      (str (list 'set/subset? coll diff))))
                diffs)
           ;; hm-coll vs empty-coll (one-way subsets)
           (map (fn [empt]
                  (is (true? (set/subset? empt coll))
                      (str (list 'set/subset? empt coll)))
                  (is (false? (set/subset? coll empt))
                      (str (list 'set/subset? coll empt))))
                empts))
         colls)))



(deftest test-set/superset?
  (is (test/arity-error? (set/superset?)))
  (is (test/arity-error? (set/superset? (hash-set 1))))
  (is (test/arity-error?
       (set/superset? (hash-set) (hash-set) (hash-set))))

  (is (test/type-error? (set/superset? (hash-set 1) 1)))
  (is (test/type-error? (set/superset? 1 (hash-set 1))))

  (let [colls (get set-test-examples 'colls)
        subs  (get set-test-examples 'subs)
        diffs (get set-test-examples 'diffs)
        empts (get set-test-examples 'empts)]
    (map (fn [coll]
           ;; coll vs coll (mutual supersets)
           (map (fn [coll2]
                  (is (true? (set/superset? coll coll2))
                      (str (list 'set/superset? coll coll2)))
                  (is (true? (set/superset? coll2 coll))
                      (str (list 'set/superset? coll2 coll))))
                colls)
           ;; coll vs sub-coll (one-way supersets)
           (map (fn [sub]
                  (is (false? (set/superset? sub coll))
                      (str (list 'set/superset? sub coll)))
                  (is (true? (set/superset? coll sub))
                      (str (list 'set/superset? coll sub))))
                subs)
           ;; coll vs different-coll (mutual non-supersets)
           (map (fn [diff]
                  (is (false? (set/superset? diff coll))
                      (str (list 'set/superset? diff coll)))
                  (is (false? (set/superset? coll diff))
                      (str (list 'set/superset? coll diff))))
                diffs)
           ;; coll vs empty-coll (one-way supersets)
           (map (fn [empt]
                  (is (false? (set/superset? empt coll))
                      (str (list 'set/superset? empt coll)))
                  (is (true? (set/superset? coll empt))
                      (str (list 'set/superset? coll empt))))
                empts))
         colls)
    (map (fn [empt]
           ;; empty-coll vs empty-coll (mutual supersets)
           (map (fn [empt2]
                  (is (true? (set/superset? empt empt2))
                      (str (list 'set/superset? empt empt2)))
                  (is (true? (set/superset? empt2 empt))
                      (str (list 'set/superset? empt2 empt))))
                empts))
         empts))

  (let [colls (get set-test-hm-examples 'colls)
        subs  (get set-test-hm-examples 'subs)
        diffs (get set-test-hm-examples 'diffs)
        empts (get set-test-examples 'empts)]
    (map (fn [coll]
           ;; hm-coll vs hm-coll (mutual supersets)
           (map (fn [coll2]
                  (is (true? (set/superset? coll coll2))
                      (str (list 'set/superset? coll coll2)))
                  (is (true? (set/superset? coll2 coll))
                      (str (list 'set/superset? coll2 coll))))
                colls)
           ;; hm-coll vs hm-sub-coll (one-way supersets)
           (map (fn [sub]
                  (is (false? (set/superset? sub coll))
                      (str (list 'set/superset? sub coll)))
                  (is (true? (set/superset? coll sub))
                      (str (list 'set/superset? coll sub))))
                subs)
           ;; hm-coll vs hm-different-coll (mutual non-supersets)
           (map (fn [diff]
                  (is (false? (set/superset? diff coll))
                      (str (list 'set/superset? diff coll)))
                  (is (false? (set/superset? coll diff))
                      (str (list 'set/superset? coll diff))))
                diffs)
           ;; hm-coll vs empty-coll (one-way supersets)
           (map (fn [empt]
                  (is (false? (set/superset? empt coll))
                      (str (list 'set/superset? empt coll)))
                  (is (true? (set/superset? coll empt))
                      (str (list 'set/superset? coll empt))))
                empts))
         colls)))
