
(deftest test-seq
  (is (nil? (seq nil)))
  (is (nil? (seq '())))
  (is (nil? (seq (vector))))
  (is (nil? (seq "")))
  (is (nil? (seq (hash-map))))
  (is (nil? (seq (hash-set))))

  (is (= 1 (first (seq (list 1 2 3)))))
  (is (= 1 (first (seq (scheme/cons 1 (seq (vector 2 3)))))))
  (is (= 1 (first (seq (vector 1 2 3)))))
  (is (= #\c (first (seq "clj"))))

  (let [hms (seq (hash-map 'a 1 'b 2))]
    (if (= 'a (first (first hms)))
        (do (is (= (vector 'a 1) (first hms)))
            (is (= (vector 'b 2) (first (rest hms)))))
        (do (is (= (vector 'b 2) (first hms)))
            (is (= (vector 'a 1) (first (rest hms)))))))

  (let [hss (seq (hash-set 'a 'b))]
    (if (= 'a (first hss))
        (do (is (= 'a (first hss)))
            (is (= 'b (first (rest hss)))))
        (do (is (= 'b (first hss)))
            (is (= 'a (first (rest hss)))))))

  (is (test/arity-error? (seq)))
  (is (test/type-error? (seq true)))
  (is (test/type-error? (seq false)))
  (is (test/type-error? (seq 1)))
  (is (test/type-error? (seq 'a)))
  (is (test/type-error? (seq '#:a))))



(deftest test-seq?
  (is (true? (seq? '())))
  (is (true? (seq? (list 1 2 3))))
  (is (true? (seq? (seq (list 1 2 3)))))
  (is (true? (seq? (scheme/cons 1 (seq (vector 2 3))))))
  (is (true? (seq? (seq (scheme/cons 1 (seq (vector 2 3)))))))
  (is (true? (seq? (seq (vector 1 2 3)))))
  (is (true? (seq? (seq "clj"))))
  (is (true? (seq? (seq (hash-map 'a 1 'b 2)))))
  (is (true? (seq? (seq (hash-set 'a 'b 'c)))))

  (is (false? (seq? nil)))
  (is (false? (seq? (vector 1 2 3))))
  (is (false? (seq? "clj")))
  (is (false? (seq? (hash-map 'a 1 'b 2))))
  (is (false? (seq? (hash-set 'a 'b 'c))))

  (is (false? (seq? true)))
  (is (false? (seq? false)))
  (is (false? (seq? 'a)))
  (is (false? (seq? '#:a)))
  (is (false? (seq? 1))))



(deftest test-seqable?
  (is (true? (seqable? nil)))

  (is (true? (seqable? (list ))))
  (is (true? (seqable? (list 1 2 3))))
  (is (true? (seqable? (seq (list 1 2 3)))))
  (is (true? (seqable? (cons 0 (list 1 2 3)))))

  (is (true? (seqable? (vector))))
  (is (true? (seqable? (vector 1 2 3))))
  (is (true? (seqable? (seq (vector 1 2 3)))))
  (is (true? (seqable? (cons 0 (vector 1 2 3)))))

  (is (true? (seqable? "")))
  (is (true? (seqable? "cloje")))
  (is (true? (seqable? (seq "cloje"))))
  (is (true? (seqable? (cons #\a "cloje"))))

  (is (true? (seqable? (hash-map))))
  (is (true? (seqable? (hash-map 'a 1 'b 2))))

  (is (true? (seqable? (hash-set))))
  (is (true? (seqable? (hash-set 'a 'b 'c)))))



(deftest test-apply
  (is (test/arity-error? (apply -)))
  (is (test/type-error? (apply - 1)))
  (is (test/type-error? (apply - 1 2 3 4)))

  (is (test/type-error? (apply 1 -)))
  (is (test/type-error? (apply '(1 2 3 4) -)))

  (is (= -8 (apply - '(1 2 3 4))))
  (is (= -8 (apply - (vector 1 2 3 4))))

  (is (= -8 (apply - 1 2 '(3 4))))
  (is (= -8 (apply - 1 2 (vector 3 4)))))



(deftest test-conj
  (is (test/arity-error? (conj)))

  (is (test/type-error? (conj true 'boom)))
  (is (test/type-error? (conj false 'boom)))
  (is (test/type-error? (conj "boo" #\m)))

  (testing "nil"
    (is (= (list 1) (conj nil 1)))
    (is (= (list 3 2 1) (conj nil 1 2 3)))

    (is (test/arity-error? (conj nil))))

  (testing "lists"
    (let [l1 (list 1 2 3)
          l2 (conj l1 42)]
      (is (= l1 (list 1 2 3)))
      (is (= l2 (list 42 1 2 3))))

    (let [l1 (list 1 2 3)
          l2 (conj l1 42 43 44)]
      (is (= l1 (list 1 2 3)))
      (is (= l2 (list 44 43 42 1 2 3))))

    (is (test/arity-error? (conj '()))))

  (testing "vectors"
    (let [v1 (vector 1 2 3)
          v2 (conj v1 42)]
      (is (= (vector 1 2 3) v1))
      (is (= (vector 1 2 3 42) v2)))

    (let [v1 (vector 1 2 3)
          v2 (conj v1 42 43 44)]
      (is (= (vector 1 2 3) v1))
      (is (= (vector 1 2 3 42 43 44) v2)))

    (is (test/arity-error? (conj (vector)))))

  (testing "hash-maps"
    (let [h1 (hash-map 'a 1)
          h2 (conj h1 (vector 'b 2))]
      (is (= (hash-map 'a 1) h1))
      (is (= (hash-map 'a 1 'b 2) h2)))

    (let [h1 (hash-map 'a 1)
          h2 (conj h1 (vector 'b 2) (vector 'a 9) (vector 'c 3))]
      (is (= (hash-map 'a 1) h1))
      (is (= (hash-map 'a 9 'b 2 'c 3) h2)))

    (is (test/arity-error? (conj (hash-map))))
    (is (test/type-error? (conj (hash-map) 'foo)))
    (is (test/type-error? (conj (hash-map) 'foo 'bar)))
    (is (test/type-error? (conj (hash-map) '(foo bar)))))

  (testing "hash-sets"
    (let [hs1 (hash-set 'a 'b)
          hs2 (conj hs1 'c)]
      (is (= (hash-set 'a 'b) hs1))
      (is (= (hash-set 'a 'b 'c) hs2)))

    (let [hs1 (hash-set 'c 'l 'j)
          hs2 (conj hs1 'o 'e 'o)]
      (is (= (hash-set 'c 'l 'j) hs1))
      (is (= (hash-set 'c 'l 'o 'j 'e) hs2)))

    (let [hs1 (hash-set 'c 'l 'j)
          hs2 (conj hs1 (hash-set 'o 'e 'o))]
      (is (= (hash-set 'c 'l 'j) hs1))
      (is (= (hash-set 'c 'l 'j (hash-set 'o 'e)) hs2)))

    (is (test/arity-error? (conj (hash-set))))))



(deftest test-cons
  (is (test/arity-error? (cons)))

  (is (test/type-error? (cons 'boom 1)))
  (is (test/type-error? (cons 'boom 'a)))
  (is (test/type-error? (cons 'boom '#:a)))
  (is (test/type-error? (cons 'boom false)))

  (is (= (list 1) (cons 1 nil)))
  (is (= (list 1) (cons 1 '())))

  (testing "pairs"
    (let [a (scheme/cons 1 (vector 2))
          b (cons 42 a)]
      (= 1 (first a))
      (= 2 (first (rest a)))

      (= 42 (first b))
      (= 1 (first (rest b)))
      (= 2 (first (rest (rest b))))))

  (testing "lists"
    (let [a (list 1 2 3)
          b (cons 42 a)]
      (is (= (list 1 2 3) a))
      (is (= (list 42 1 2 3) b))))

  (testing "vectors"
    (let [a (vector 1 2)
          b (cons 42 a)]
      (is (= (vector 1 2) a))

      (is (= 42 (first b)))
      (is (= 1 (first (rest b))))
      (is (= 2 (first (rest (rest b)))))))

  (testing "vector seqs"
    (let [a (seq (vector 1 2))
          b (cons 42 a)]
      (is (= 1 (first a)))
      (is (= 2 (first (rest a))))

      (is (= 42 (first b)))
      (is (= 1 (first (rest b))))
      (is (= 2 (first (rest (rest b)))))))

  (testing "strings"
    (let [a "lj"
          b (cons #\c a)]
      (is (= "lj" a))

      (is (= #\c (first b)))
      (is (= #\l (first (rest b))))
      (is (= #\j (first (rest (rest b))))))

    (let [a "lj"
          b (cons 'foo a)]
      (is (= "lj" a))

      (is (= 'foo (first b)))
      (is (= #\l (first (rest b))))
      (is (= #\j (first (rest (rest b)))))))

  (testing "string seqs"
    (let [a (seq "lj")
          b (cons #\c a)]
      (is (= #\l (first a)))
      (is (= #\j (first (rest a))))

      (is (= #\c (first b)))
      (is (= #\l (first (rest b))))
      (is (= #\j (first (rest (rest b))))))

    (let [a (seq "lj")
          b (cons 'foo a)]
      (is (= #\l (first a)))
      (is (= #\j (first (rest a))))

      (is (= 'foo (first b)))
      (is (= #\l (first (rest b))))
      (is (= #\j (first (rest (rest b))))))))



(deftest test-ffirst
  (is (nil? (ffirst nil)))
  (is (= 1 (ffirst (list (list 1 2) 3 4))))
  (is (= 1 (ffirst (vector (list 1 2) 3 4))))
  (is (= 1 (ffirst (list (vector 1 2) 3 4))))
  (is (= 'a (ffirst (hash-map 'a 1))))
  (is (nil? (ffirst '())))
  (is (nil? (ffirst (vector)))))



(deftest test-first
  (is (test/arity-error? (first)))

  (is (nil? (first nil)))
  (is (nil? (first '())))

  (testing "pairs"
    (is (= 1 (first (scheme/cons 1 (seq (vector 2 3)))))))

  (testing "lists"
    (is (nil? (first '())))
    (is (= 1 (first '(1))))
    (is (= 1 (first '(1 2 3)))))

  (testing "vectors"
    (is (nil? (first (vector))))
    (is (= 1 (first (vector 1))))
    (is (= 1 (first (vector 1 2 3)))))

  (testing "vector seqs"
    (is (nil? (first (seq (vector)))))
    (is (= 1 (first (seq (vector 1)))))
    (is (= 1 (first (seq (vector 1 2 3))))))

  (testing "strings"
    (is (nil? (first "")))
    (is (= #\c (first "c")))
    (is (= #\c (first "clj"))))

  (testing "string seqs"
    (is (nil? (first (seq ""))))
    (is (= #\c (first (seq "c"))))
    (is (= #\c (first (seq "clj")))))

  (testing "hash-maps"
    (is (nil? (first (hash-map))))
    (let [f (first (hash-map 'a 1 'b 2))]
      (is (or (= (vector 'a 1) f)
              (= (vector 'b 2) f)))))

  (testing "hash-map seqs"
    (is (nil? (first (seq (hash-map)))))
    (let [f (first (seq (hash-map 'a 1 'b 2)))]
      (is (or (= (vector 'a 1) f)
              (= (vector 'b 2) f)))))

  (testing "hash-sets"
    (is (nil? (first (hash-set))))
    (let [x (first (hash-set 'a 'b))]
      (is (or (= x 'a) (= x 'b)))))

  (testing "hash-set seqs"
    (is (nil? (first (seq (hash-set)))))
    (let [x (first (seq (hash-set 'a 'b)))]
      (is (or (= x 'a) (= x 'b))))))



(deftest test-fnext
  (is (nil? (fnext nil)))
  (is (= 3 (fnext (list (list 1 2) 3 4))))
  (is (= 3 (fnext (vector (list 1 2) 3 4))))
  (is (= 3 (fnext (list (vector 1 2) 3 4))))
  (is (= (vector 'a 1) (fnext (cons 0 (hash-map 'a 1)))))
  (is (nil? (fnext '())))
  (is (nil? (fnext (vector)))))



(deftest test-into
  (testing "nil"
    (is (= nil (into nil nil)))
    (is (= nil (into nil '())))
    (is (= nil (into nil (seq '()))))
    (is (= nil (into nil (vector))))
    (is (= nil (into nil (seq (vector)))))
    (is (= nil (into nil "")))
    (is (= nil (into nil (seq ""))))

    (is (= '(1 2) (into nil '(2 1))))
    (is (= '(1 2) (into nil (seq '(2 1)))))
    (is (= '(1 2) (into nil (vector 2 1))))
    (is (= '(1 2) (into nil (seq (vector 2 1)))))
    (is (= '(#\c #\l) (into nil "lc")))
    (is (= '(#\c #\l) (into nil (seq "lc")))))

  (testing "lists"
    (is (= '() (into '() nil)))
    (is (= '() (into '() '())))
    (is (= '() (into '() (seq '()))))
    (is (= '() (into '() (vector))))
    (is (= '() (into '() (seq (vector)))))
    (is (= '() (into '() "")))
    (is (= '() (into '() (seq ""))))

    (is (= '(1 2) (into '(1 2) nil)))
    (is (= '(1 2) (into '(1 2) '())))
    (is (= '(1 2) (into '(1 2) (seq '()))))
    (is (= '(1 2) (into '(1 2) (vector))))
    (is (= '(1 2) (into '(1 2) (seq (vector)))))
    (is (= '(1 2) (into '(1 2) "")))
    (is (= '(1 2) (into '(1 2) (seq ""))))

    (is (= '(1 2) (into '() '(2 1))))
    (is (= '(1 2) (into '() (seq '(2 1)))))
    (is (= '(1 2) (into '() (vector 2 1))))
    (is (= '(1 2) (into '() (seq (vector 2 1)))))
    (is (= '(#\c #\l) (into '() "lc")))
    (is (= '(#\c #\l) (into '() (seq "lc"))))

    (is (= '(1 2 3 4) (into '(3 4) '(2 1))))
    (is (= '(1 2 3 4) (into '(3 4) (seq '(2 1)))))
    (is (= '(1 2 3 4) (into '(3 4) (vector 2 1))))
    (is (= '(1 2 3 4) (into '(3 4) (seq (vector 2 1)))))
    (is (= '(#\c #\l #\j #\m) (into '(#\j #\m) "lc")))
    (is (= '(#\c #\l #\j #\m) (into '(#\j #\m) (seq "lc")))))

  (testing "vectors"
    (is (= (vector) (into (vector) nil)))
    (is (= (vector) (into (vector) '())))
    (is (= (vector) (into (vector) (seq '()))))
    (is (= (vector) (into (vector) (vector))))
    (is (= (vector) (into (vector) (seq (vector)))))
    (is (= (vector) (into (vector) "")))
    (is (= (vector) (into (vector) (seq ""))))

    (is (= (vector 1 2) (into (vector 1 2) nil)))
    (is (= (vector 1 2) (into (vector 1 2) '())))
    (is (= (vector 1 2) (into (vector 1 2) (seq '()))))
    (is (= (vector 1 2) (into (vector 1 2) (vector))))
    (is (= (vector 1 2) (into (vector 1 2) (seq (vector)))))
    (is (= (vector 1 2) (into (vector 1 2) "")))
    (is (= (vector 1 2) (into (vector 1 2) (seq ""))))

    (is (= (vector 1 2) (into (vector) '(1 2))))
    (is (= (vector 1 2) (into (vector) (seq '(1 2)))))
    (is (= (vector 1 2) (into (vector) (vector 1 2))))
    (is (= (vector 1 2) (into (vector) (seq (vector 1 2)))))
    (is (= (vector #\c #\l) (into (vector) "cl")))
    (is (= (vector #\c #\l) (into (vector) (seq "cl"))))

    (is (= (vector 1 2 3 4) (into (vector 1 2) '(3 4))))
    (is (= (vector 1 2 3 4) (into (vector 1 2) (seq '(3 4)))))
    (is (= (vector 1 2 3 4) (into (vector 1 2) (vector 3 4))))
    (is (= (vector 1 2 3 4) (into (vector 1 2) (seq (vector 3 4)))))
    (is (= (vector #\c #\l #\j #\e) (into (vector #\c #\l) "je")))
    (is (= (vector #\c #\l #\j #\e) (into (vector #\c #\l) (seq "je")))))

  (testing "hash-maps"
    (is (= (hash-map 'a 1 'b 2)
           (into (hash-map) (list (vector 'a 1)
                                  (vector 'b 2)))))
    (is (= (hash-map 'a 1 'b 2)
           (into (hash-map) (list (vector 'a 9)
                                  (vector 'b 2)
                                  (vector 'a 1)))))
    (is (= (hash-map 'a 1 'b 2)
           (into (hash-map) (vector (vector 'a 1)
                                    (vector 'b 2)))))
    (is (= (hash-map 'a 1 'b 2)
           (into (hash-map) (vector (vector 'a 9)
                                    (vector 'b 2)
                                    (vector 'a 1)))))

    (is (= (hash-map) (into (hash-map) nil)))
    (is (= (hash-map) (into (hash-map) '())))
    (is (= (hash-map) (into (hash-map) (vector))))
    (is (= (hash-map) (into (hash-map) "")))

    ;; list of lists (instead of list of vectors)
    (is (test/type-error?
         (into (hash-map) (list (list 'a 1) (list 'b 2))))))

  (testing "hash sets"
    (is (= (hash-set) (into (hash-set) nil)))

    (is (= (hash-set 'a 'b 'c)
           (into (hash-set) (hash-set 'c 'a 'b 'a 'b))))
    (is (= (hash-set 'c 'l 'o 'j 'e)
           (into (hash-set 'c 'l 'j) (hash-set 'o 'e 'o))))

    (is (= (hash-set) (into (hash-set) (list))))
    (is (= (hash-set 'a 'b 'c)
           (into (hash-set) (list 'c 'a 'b 'a 'b))))
    (is (= (hash-set 'c 'l 'o 'j 'e)
           (into (hash-set 'c 'l 'j) (list 'o 'e 'o))))

    (is (= (hash-set) (into (hash-set) (vector))))
    (is (= (hash-set 'a 'b 'c)
           (into (hash-set) (vector 'c 'a 'b 'a 'b))))
    (is (= (hash-set 'c 'l 'o 'j 'e)
           (into (hash-set 'c 'l 'j) (vector 'o 'e 'o))))

    (is (= (hash-set) (into (hash-set) (hash-set))))
    (is (= (hash-set (vector 'a 1) (vector 'b 2) (vector 'c 3))
           (into (hash-set) (hash-map 'a 1 'b 2 'c 3))))
    (is (= (hash-set (vector 'a 1) (vector 'b 2) (vector 'c 3))
           (into (hash-set (vector 'b 2)) (hash-map 'a 1 'c 3))))))



(deftest test-last
  (is (test/arity-error? (last)))

  (is (nil? (last nil)))

  (testing "lists"
    (is (nil? (last '())))
    (is (= 1 (last '(1))))
    (is (= 3 (last '(1 2 3)))))

  (testing "vectors"
    (is (nil? (last (vector))))
    (is (= 1 (last (vector 1))))
    (is (= 3 (last (vector 1 2 3)))))

  (testing "vector seqs"
    (is (nil? (last (seq (vector)))))
    (is (= 1 (last (seq (vector 1)))))
    (is (= 3 (last (seq (vector 1 2 3))))))

  (testing "strings"
    (is (nil? (last "")))
    (is (= #\c (last "c")))
    (is (= #\j (last "clj"))))

  (testing "string seqs"
    (is (nil? (last (seq ""))))
    (is (= #\c (last (seq "c"))))
    (is (= #\j (last (seq "clj")))))

  (testing "hash-maps"
    (is (nil? (last (hash-map))))
    (let [hm (hash-map 'a 1 'b 2)]
      (if (= 'a (first (first hm)))
        (is (= (vector 'b 2) (last hm)))
        (is (= (vector 'a 1) (last hm))))))

  (testing "hash-map seqs"
    (is (nil? (last (seq (hash-map)))))
    (let [hms (seq (hash-map 'a 1 'b 2))]
      (if (= 'a (first (first hms)))
        (is (= (vector 'b 2) (last hms)))
        (is (= (vector 'a 1) (last hms))))))

  (testing "hash-sets"
    (is (nil? (last (hash-set))))
    (let [hs (hash-set 'a 'b)]
      (if (= 'a (first hs))
        (is (= 'b (last hs)))
        (is (= 'a (last hs)))))))



(deftest test-map
  (is (test/arity-error? (map)))
  (is (test/arity-error? (map +)))

  (testing "non-collections"
    (is (test/type-error? (map + 1)))
    (is (test/type-error? (map + 1 2)))
    (is (test/type-error? (map + '(1) '(2) 3))))

  (is (= '() (map + nil)))

  (is (= '(-1 -2 -3 -4) (map - '(1 2 3 4))))

  (testing "non-equal sizes"
    (is (= '(3 5 7) (map + '(1 2 3) '(2 3 4))))
    (is (= '(3 5)   (map + '(1 2)   '(2 3 4))))
    (is (= '(3 5)   (map + '(1 2 3) '(2 3))))
    (is (= '()      (map + nil '(1 2 3) '(2 3 4))))
    (is (= '()      (map + '(1 2 3) '(2 3 4) nil))))

  (testing "vectors"
    (is (= '(3 5 7) (map + (vector 1 2 3) '(2 3 4))))
    (is (= '(3 5 7) (map + '(1 2 3) (vector 2 3 4))))
    (is (= '() (map + '(1 2 3) (vector))))
    (is (= '() (map + (vector) '(1 2 3)))))

  (is (= '(16 20 24)
         (map + '(1 2 3) '(3 4 5) '(5 6 7) '(7 8 9))))

  (testing "hash-maps"
    (let [kvs (map (fn [kv] kv) (hash-map 'a 1 'b 2))]
      (if (= 'a (first (first kvs)))
        (is (= (list (vector 'a 1) (vector 'b 2)) kvs))
        (is (= (list (vector 'b 2) (vector 'a 1)) kvs)))))

  (testing "hash-sets"
    (is (= '() (map (fn [k] k) (hash-set))))
    (let [mapped (map (fn [k] (- k)) (hash-set 1 2))]
      (is (or (= '(-1 -2) mapped)
              (= '(-2 -1) mapped))))))



(deftest test-next
  (is (test/arity-error? (next)))

  (is (nil? (next nil)))
  (is (nil? (next '())))

  (testing "pairs"
    (let [s (seq (vector 2 3))]
      (is (identical? s (next (scheme/cons 1 s))))
      (is (identical? s (next (next (srfi-1/cons* 0 1 s)))))))

  (testing "lists"
    (is (nil? (next '())))
    (is (nil? (next '(1))))
    (is (= '(2 3) (next '(1 2 3)))))

  (testing "vectors"
    (is (nil? (next (vector))))
    (is (nil? (next (vector 1))))
    (is (= 2 (first (next (vector 1 2 3)))))
    (is (= 3 (first (next (next (vector 1 2 3)))))))

  (testing "vector seqs"
    (is (nil? (next (seq (vector)))))
    (is (nil? (next (seq (vector 1)))))
    (is (= 2 (first (next (seq (vector 1 2 3))))))
    (is (= 3 (first (next (next (seq (vector 1 2 3))))))))

  (testing "strings"
    (is (nil? (next "")))
    (is (nil? (next "c")))
    (is (= #\l (first (next "clj"))))
    (is (= #\j (first (next (next "clj"))))))

  (testing "string seqs"
    (is (nil? (next (seq ""))))
    (is (nil? (next (seq "c"))))
    (is (= #\l (first (next (seq "clj")))))
    (is (= #\j (first (next (next (seq "clj")))))))

  (testing "hash-maps"
    (is (nil? (next (hash-map))))
    (is (nil? (next (hash-map 'a 1))))
    (let [hm (hash-map 'a 1 'b 2)]
      (is (or (= (list (vector 'b 2)) (next hm))
              (= (list (vector 'a 1)) (next hm))))))

  (testing "hash-map seqs"
    (is (nil? (next (seq (hash-map)))))
    (is (nil? (next (seq (hash-map 'a 1)))))
    (let [hms (seq (hash-map 'a 1 'b 2))]
      (is (or (= (list (vector 'b 2)) (next hms))
              (= (list (vector 'a 1)) (next hms))))))

  (testing "hash-sets"
    (is (nil? (next (hash-set))))
    (is (nil? (next (hash-set 'a))))

    (let [hs (hash-set 'a 'b 'c)
          f  (first hs)
          r  (next hs)]
      (case f
        'a  (is (or (= r '(b c)) (= r '(c b))))
        'b  (is (or (= r '(a c)) (= r '(c a))))
        'c  (is (or (= r '(a b)) (= r '(b a))))))))



(deftest test-nfirst
  (is (nil? (nfirst nil)))
  (is (= '(2) (nfirst (list (list 1 2) 3 4))))
  (is (= '(2) (nfirst (vector (list 1 2) 3 4))))
  (is (= '(2) (nfirst (list (vector 1 2) 3 4))))
  (is (= '(1) (nfirst (hash-map 'a 1))))
  (is (nil? (nfirst '())))
  (is (nil? (nfirst (vector)))))



(deftest test-nnext
  (is (nil? (nnext nil)))
  (is (= '(4) (nnext (list (list 1 2) 3 4))))
  (is (= '(4) (nnext (vector (list 1 2) 3 4))))
  (is (= '(4) (nnext (list (vector 1 2) 3 4))))
  (is (nil? (nnext (hash-map 'a 1 'b 2))))
  (let [x (nnext (hash-map 'a 1 'b 2 'c 3))]
    (is (or (= (list (vector 'a 1)) x)
            (= (list (vector 'b 2)) x)
            (= (list (vector 'c 3)) x))))
  (is (nil? (nnext '())))
  (is (nil? (nnext (vector)))))



(deftest test-nth
  (is (test/arity-error? (nth)))

  (is (test/type-error? (nth (hash-map 'a 1) 0)))
  (is (test/type-error? (nth 'a 0)))
  (is (test/type-error? (nth 1 0)))

  (testing "nil"
    (is (nil? (nth nil 0)))
    (is (nil? (nth nil 1)))

    (is (= 42 (nth nil 0 42)))
    (is (= 42 (nth nil 1 42)))

    (is (nil? (nth nil 0.5)))
    (is (= 42 (nth nil 0.5 42)))

    (is (test/type-error? (nth nil true)))
    (is (test/type-error? (nth nil true 42)))
    (is (test/type-error? (nth nil "not a number")))
    (is (test/type-error? (nth nil "not a number" 42))))

  (testing "lists"
    (let [l (list 1 2 3)]
      (is (= 1 (nth l 0)))
      (is (= 2 (nth l 1)))
      (is (= 3 (nth l 2)))

      (is (= 1 (nth l 0.01)))
      (is (= 1 (nth l 0.50)))
      (is (= 1 (nth l 0.99)))
      (is (= 2 (nth l 1.01)))
      (is (= 2 (nth l 1.50)))
      (is (= 2 (nth l 1.99)))

      (is (test/bounds-error? (nth l -1)))
      (is (test/bounds-error? (nth l 3)))

      (is (= 42 (nth l -1 42)))
      (is (= 42 (nth l  3 42)))

      (is (test/arity-error? (nth l)))
      (is (test/arity-error? (nth l "too" "many" "args")))
      (is (test/type-error? (nth l true)))
      (is (test/type-error? (nth l true 42)))
      (is (test/type-error? (nth l "not a number")))
      (is (test/type-error? (nth l "not a number" 42)))))

  (testing "vectors"
    (let [v (vector 1 2 3)]
      (is (= 1 (nth v 0)))
      (is (= 2 (nth v 1)))
      (is (= 3 (nth v 2)))

      (is (= 1 (nth v 0.01)))
      (is (= 1 (nth v 0.50)))
      (is (= 1 (nth v 0.99)))
      (is (= 2 (nth v 1.01)))
      (is (= 2 (nth v 1.50)))
      (is (= 2 (nth v 1.99)))

      (is (test/bounds-error? (nth v -1)))
      (is (test/bounds-error? (nth v 3)))

      (is (= 42 (nth v -1 42)))
      (is (= 42 (nth v  3 42)))

      (is (test/arity-error? (nth v)))
      (is (test/arity-error? (nth v "too" "many" "args")))
      (is (test/type-error? (nth v true)))
      (is (test/type-error? (nth v true 42)))
      (is (test/type-error? (nth v "not a number")))
      (is (test/type-error? (nth v "not a number" 42)))))

  (testing "vector seqs"
    (let [v (seq (vector 1 2 3))]
      (is (= 1 (nth v 0)))
      (is (= 2 (nth v 1)))
      (is (= 3 (nth v 2)))

      (is (= 1 (nth v 0.01)))
      (is (= 1 (nth v 0.50)))
      (is (= 1 (nth v 0.99)))
      (is (= 2 (nth v 1.01)))
      (is (= 2 (nth v 1.50)))
      (is (= 2 (nth v 1.99)))

      (is (test/bounds-error? (nth v -1)))
      (is (test/bounds-error? (nth v 3)))

      (is (= 42 (nth v -1 42)))
      (is (= 42 (nth v  3 42)))

      (is (test/arity-error? (nth v)))
      (is (test/arity-error? (nth v "too" "many" "args")))
      (is (test/type-error? (nth v true)))
      (is (test/type-error? (nth v true 42)))
      (is (test/type-error? (nth v "not a number")))
      (is (test/type-error? (nth v "not a number" 42)))))

  (testing "strings"
    (let [s "clj"]
      (is (= #\c (nth s 0)))
      (is (= #\l (nth s 1)))
      (is (= #\j (nth s 2)))

      (is (= #\c (nth s 0.01)))
      (is (= #\c (nth s 0.50)))
      (is (= #\c (nth s 0.99)))
      (is (= #\l (nth s 1.01)))
      (is (= #\l (nth s 1.50)))
      (is (= #\l (nth s 1.99)))

      (is (test/bounds-error? (nth s -1)))
      (is (test/bounds-error? (nth s 3)))

      (is (= 42 (nth s -1 42)))
      (is (= 42 (nth s  3 42)))

      (is (test/arity-error? (nth s)))
      (is (test/arity-error? (nth s "too" "many" "args")))
      (is (test/type-error? (nth s true)))
      (is (test/type-error? (nth s true 42)))
      (is (test/type-error? (nth s "not a number")))
      (is (test/type-error? (nth s "not a number" 42)))))

  (testing "string seqs"
    (let [s (seq "clj")]
      (is (= #\c (nth s 0)))
      (is (= #\l (nth s 1)))
      (is (= #\j (nth s 2)))

      (is (= #\c (nth s 0.01)))
      (is (= #\c (nth s 0.50)))
      (is (= #\c (nth s 0.99)))
      (is (= #\l (nth s 1.01)))
      (is (= #\l (nth s 1.50)))
      (is (= #\l (nth s 1.99)))

      (is (test/bounds-error? (nth s -1)))
      (is (test/bounds-error? (nth s 3)))

      (is (= 42 (nth s -1 42)))
      (is (= 42 (nth s  3 42)))

      (is (test/arity-error? (nth s)))
      (is (test/arity-error? (nth s "too" "many" "args")))
      (is (test/type-error? (nth s true)))
      (is (test/type-error? (nth s true 42)))
      (is (test/type-error? (nth s "not a number")))
      (is (test/type-error? (nth s "not a number" 42)))))

  (testing "hash-map seqs"
    (let [hms (seq (hash-map 'a 1 'b 2))]
      (if (= 'a (first (first hms)))
        (do (is (= (vector 'a 1) (nth hms 0)))
            (is (= (vector 'b 2) (nth hms 1))))
        (do (is (= (vector 'b 2) (nth hms 0)))
            (is (= (vector 'a 1) (nth hms 1)))))

      (is (test/bounds-error? (nth hms -1)))
      (is (test/bounds-error? (nth hms 3)))

      (is (= 42 (nth hms -1 42)))
      (is (= 42 (nth hms  3 42)))

      (is (test/arity-error? (nth hms)))
      (is (test/arity-error? (nth hms "too" "many" "args")))
      (is (test/type-error? (nth hms true)))
      (is (test/type-error? (nth hms true 42)))
      (is (test/type-error? (nth hms "not a number")))
      (is (test/type-error? (nth hms "not a number" 42))))))



(deftest test-peek
  (is (test/arity-error? (peek)))

  (is (test/type-error? (peek "clj")))
  (is (test/type-error? (peek (hash-map 'a 1))))

  (is (nil? (peek nil)))

  (testing "lists"
    (is (nil? (peek '())))
    (is (= 1 (peek '(1))))
    (is (= 1 (peek '(1 2 3)))))

  (testing "vectors"
    (is (nil? (peek (vector))))
    (is (= 1 (peek (vector 1))))
    (is (= 3 (peek (vector 1 2 3))))))



(deftest test-pop
  (is (test/arity-error? (pop)))

  (is (test/type-error? (pop "clj")))
  (is (test/type-error? (pop (hash-map 'a 1))))

  (is (nil? (pop nil)))

  (testing "lists"
    (is (test/error? (pop '())))
    (is (= '() (pop '(1))))
    (is (= '(2 3) (pop '(1 2 3)))))

  (testing "vectors"
    (is (test/error? (pop (vector))))
    (is (= (vector) (pop (vector 1))))
    (is (= (vector 1 2) (pop (vector 1 2 3))))))



(deftest test-reduce
  (testing "with NO initial val"
    (testing "and an empty coll"
      (let [f (fn [& args] (if (empty? args) 42))]
        (is (= 42 (reduce f nil)))
        (is (= 42 (reduce f (list))))
        (is (= 42 (reduce f (vector))))
        (is (= 42 (reduce f (hash-set))))
        (is (= 42 (reduce f (hash-map))))
        (is (= 42 (reduce f "")))))

    (testing "and a single element coll"
      (let [f (fn [& args] (/ 1 0))]
        (is (= 'a            (reduce f (list 'a))))
        (is (= 'a            (reduce f (vector 'a))))
        (is (= 'a            (reduce f (hash-set 'a))))
        (is (= (vector 'a 1) (reduce f (hash-map 'a 1))))
        (is (= (first "a")   (reduce f "a")))))

    (testing "and a two element coll"
      (let [f (fn [a b] (list (- b) a))]
        (is (= '(-2 1) (reduce f (list 1 2))))
        (is (= '(-2 1) (reduce f (vector 1 2))))
        (let [result (reduce f (hash-set 1 2))]
          (is (or (= '(-2 1) result)
                  (= '(-1 2) result)))))

      (let [result (reduce conj (hash-map 'a 1 'b 2))]
        (is (or (= (vector 'a 1 (vector 'b 2)) result)
                (= (vector 'b 2 (vector 'a 1)) result))))

      (let [f (fn [a b] (str "z" b a))]
        (is (= "zba" (reduce f "ab")))))

    (testing "and a many element coll"
      (let [f (fn [a b] (list (- b) a))]
        (is (= '(-4 (-3 (-2 1))) (reduce f (list 1 2 3 4))))
        (is (= '(-4 (-3 (-2 1))) (reduce f (vector 1 2 3 4))))

        (let [result (reduce f (hash-set 1 2 3))]
          (is (or (= '(-1 (-2 3)) result)
                  (= '(-1 (-3 2)) result)
                  (= '(-2 (-1 3)) result)
                  (= '(-2 (-3 1)) result)
                  (= '(-3 (-1 2)) result)
                  (= '(-3 (-2 1)) result)))))

      (let [result (reduce conj (hash-map 'a 1 'b 2 'c 3))]
        (is (or (= (vector 'a 1 (vector 'b 2) (vector 'c 3)) result)
                (= (vector 'a 1 (vector 'c 3) (vector 'b 2)) result)
                (= (vector 'b 2 (vector 'a 1) (vector 'c 3)) result)
                (= (vector 'b 2 (vector 'c 3) (vector 'a 1)) result)
                (= (vector 'c 3 (vector 'a 1) (vector 'b 2)) result)
                (= (vector 'c 3 (vector 'b 2) (vector 'a 1)) result))))

      (let [f (fn [a b] (str "z" b a))]
        (is (= "zdzczba" (reduce f "abcd"))))))

  (testing "with initial val"
    (testing "and an empty coll"
      (let [f (fn [& args] (/ 1 0))]
        (is (= 42 (reduce f 42 nil)))
        (is (= 42 (reduce f 42 (list))))
        (is (= 42 (reduce f 42 (vector))))
        (is (= 42 (reduce f 42 (hash-set))))
        (is (= 42 (reduce f 42 (hash-map))))
        (is (= 42 (reduce f 42 "")))))

    (testing "and a single element coll"
      (let [f (fn [a b] (list (- b) a))]
        (is (= '(-1 42) (reduce f 42 (list 1))))
        (is (= '(-1 42) (reduce f 42 (vector 1))))
        (is (= '(-1 42) (reduce f 42 (hash-set 1)))))

      (is (= (vector 42 (vector 'a 1))
             (reduce conj (vector 42) (hash-map 'a 1))))

      (let [f (fn [a b] (str "z" b a))]
        (is (= "zax" (reduce f "x" "a")))))

    (testing "and a multiple element coll"
      (let [f (fn [a b] (list (- b) a))]
        (is (= '(-3 (-2 (-1 42))) (reduce f 42 (list 1 2 3))))
        (is (= '(-3 (-2 (-1 42))) (reduce f 42 (vector 1 2 3))))
        (let [result (reduce f 42 (hash-set 1 2 3))]
          (is (or (= '(-3 (-2 (-1 42))) result)
                  (= '(-3 (-1 (-2 42))) result)
                  (= '(-2 (-3 (-1 42))) result)
                  (= '(-2 (-1 (-3 42))) result)
                  (= '(-1 (-3 (-2 42))) result)
                  (= '(-1 (-2 (-3 42))) result)))))

      (let [v vector
            result (reduce conj (v 42) (hash-map 'a 1 'b 2 'c 3))]
        (is (or (= (v 42 (v 'a 1) (v 'b 2) (v 'c 3)) result)
                (= (v 42 (v 'a 1) (v 'c 3) (v 'b 2)) result)
                (= (v 42 (v 'b 2) (v 'a 1) (v 'c 3)) result)
                (= (v 42 (v 'b 2) (v 'c 3) (v 'a 1)) result)
                (= (v 42 (v 'c 3) (v 'a 1) (v 'b 2)) result)
                (= (v 42 (v 'c 3) (v 'b 2) (v 'a 1)) result))))

      (let [f (fn [a b] (str "z" b a))]
        (is (= "zczbzax" (reduce f "x" "abc"))))))

  (testing "error cases"
    (is (test/arity-error? (reduce)))
    (is (test/arity-error? (reduce list)))
    (is (test/arity-error? (reduce list 1 '(2) '(3))))

    (is (test/type-error? (reduce list 1)))
    (is (test/type-error? (reduce list 1 2)))
    (is (test/type-error? (reduce list 'a)))
    (is (test/type-error? (reduce list 1 'a)))
    (is (test/type-error? (reduce list (char 97))))
    (is (test/type-error? (reduce list 1 (char 97))))
    (is (test/type-error? (reduce list (keyword "a"))))
    (is (test/type-error? (reduce list 1 (keyword "a"))))))



(deftest test-rest
  (is (test/arity-error? (rest)))

  (is (= '() (rest nil)))
  (is (= '() (rest '())))

  (testing "pairs"
    (let [s (seq (vector 2 3))]
      (is (identical? s (rest (scheme/cons 1 s))))
      (is (identical? s (rest (rest (srfi-1/cons* 0 1 s)))))))

  (testing "lists"
    (is (= '() (rest '())))
    (is (= '() (rest '(1))))
    (is (= '(2 3) (rest '(1 2 3)))))

  (testing "vectors"
    (is (= '() (rest (vector))))
    (is (= '() (rest (vector 1))))
    (is (= 2 (first (rest (vector 1 2 3)))))
    (is (= 3 (first (rest (rest (vector 1 2 3)))))))

  (testing "vector seqs"
    (is (= '() (rest (seq (vector)))))
    (is (= '() (rest (seq (vector 1)))))
    (is (= 2 (first (rest (seq (vector 1 2 3))))))
    (is (= 3 (first (rest (rest (seq (vector 1 2 3))))))))

  (testing "strings"
    (is (= '() (rest "")))
    (is (= '() (rest "c")))
    (is (= #\l (first (rest "clj"))))
    (is (= #\j (first (rest (rest "clj"))))))

  (testing "string seqs"
    (is (= '() (rest (seq ""))))
    (is (= '() (rest (seq "c"))))
    (is (= #\l (first (rest (seq "clj")))))
    (is (= #\j (first (rest (rest (seq "clj")))))))

  (testing "hash-maps"
    (is (= '() (rest (hash-map))))
    (is (= '() (rest (hash-map 'a 1))))
    (let [hm (hash-map 'a 1 'b 2)]
      (is (or (= (list (vector 'b 2)) (rest hm))
              (= (list (vector 'a 1)) (rest hm))))))

  (testing "hash-map seqs"
    (is (= '() (rest (seq (hash-map)))))
    (is (= '() (rest (seq (hash-map 'a 1)))))
    (let [hms (seq (hash-map 'a 1 'b 2))]
      (is (or (= (list (vector 'b 2)) (rest hms))
              (= (list (vector 'a 1)) (rest hms))))))

  (testing "hash-sets"
    (is (= '() (rest (hash-set))))
    (is (= '() (rest (hash-set 'a))))

    (let [hs (hash-set 'a 'b 'c)
          f  (first hs)
          r  (rest hs)]
      (case f
        'a  (is (or (= r '(b c)) (= r '(c b))))
        'b  (is (or (= r '(a c)) (= r '(c a))))
        'c  (is (or (= r '(a b)) (= r '(b a))))))))



(deftest test-index-of
  (is (identical? index-of .indexOf))

  (is (test/arity-error? (index-of)))
  (is (test/arity-error? (index-of "foo")))
  (is (test/arity-error? (index-of "too" "many" "args")))

  (is (test/type-error? (index-of nil "f")))
  (is (test/type-error? (index-of (hash-map 'a 1) (vector 'a 1))))

  (testing "lists"
    (is (= 0 (index-of '(a 1 a) 'a)))
    (is (= 1 (index-of '(a 1 a) 1)))

    (is (= 0 (index-of (list true false nil) true)))
    (is (= 1 (index-of (list true false nil) false)))
    (is (= 2 (index-of (list true false nil) nil)))

    (is (= -1 (index-of '(a 1 a) 'b)))
    (is (= -1 (index-of '(a 1 a) "a")))
    (is (= -1 (index-of '(a 1 a) '#:a)))
    (is (= -1 (index-of '(a 1 a) #\a)))
    (is (= -1 (index-of '(a 1 a) nil)))
    (is (= -1 (index-of '(a 1 a) 1.0)))
    (is (= -1 (index-of '(a 1 a) '(a 1 a))))
    (is (= -1 (index-of '(a 1 a) '(a))))
    (is (= -1 (index-of '() 'a))))

  (testing "vectors"
    (is (= 0 (index-of (vector 'a 1 'a) 'a)))
    (is (= 1 (index-of (vector 'a 1 'a) 1)))

    (is (= 0 (index-of (vector true false nil) true)))
    (is (= 1 (index-of (vector true false nil) false)))
    (is (= 2 (index-of (vector true false nil) nil)))

    (is (= -1 (index-of (vector 'a 1 'a) 'b)))
    (is (= -1 (index-of (vector 'a 1 'a) "a")))
    (is (= -1 (index-of (vector 'a 1 'a) '#:a)))
    (is (= -1 (index-of (vector 'a 1 'a) #\a)))
    (is (= -1 (index-of (vector 'a 1 'a) nil)))
    (is (= -1 (index-of (vector 'a 1 'a) 1.0)))
    (is (= -1 (index-of (vector 'a 1 'a) (vector 'a 1 'a))))
    (is (= -1 (index-of (vector 'a 1 'a) (vector 'a))))
    (is (= -1 (index-of (vector 'a 1 'a) '(a 1 a))))
    (is (= -1 (index-of (vector 'a 1 'a) '(a))))
    (is (= -1 (index-of (vector) 'a))))

  (testing "strings"
    (is (= 0 (index-of "foo" "foo")))
    (is (= 0 (index-of "foo" "")))
    (is (= 0 (index-of "foo" "f")))
    (is (= 1 (index-of "foo" "o")))
    (is (= 1 (index-of "foo" "oo")))

    (is (= -1 (index-of "foo" "bar")))
    (is (= -1 (index-of "" "bar")))

    (is (test/type-error? (index-of "foo" (seq "f"))))
    (is (test/type-error? (index-of "foo" #\f)))
    (is (test/type-error? (index-of "foo" 'f)))
    (is (test/type-error? (index-of "foo" nil))))

  (testing "seqs"
    (is (= 0 (index-of (seq (vector 'a 1 'a)) 'a)))
    (is (= 1 (index-of (seq (vector 'a 1 'a)) 1)))
    (is (= -1 (index-of (seq (vector 'a 1 'a)) 'b)))

    (is (= 1 (index-of (cons 'b (vector 'a 1 'a)) 'a)))
    (is (= 2 (index-of (cons 'b (vector 'a 1 'a)) 1)))
    (is (= 0 (index-of (cons 'b (vector 'a 1 'a)) 'b)))
    (is (= -1 (index-of (cons 'b (vector 'a 1 'a)) 'c)))

    (is (= -1 (index-of (seq "foo") "f")))
    (is (= 0 (index-of (seq "foo") #\f)))
    (is (= 1 (index-of (seq "foo") #\o)))

    (is (= -1 (index-of (cons #\f "foo") "f")))
    (is (= 0 (index-of (cons #\f "foo") #\f)))
    (is (= 2 (index-of (cons #\f "foo") #\o)))

    (is (= 0 (index-of (seq (hash-map 'a 1)) (vector 'a 1))))
    (is (= -1 (index-of (seq (hash-map 'a 1)) (vector 'a 2))))))



(deftest test-last-index-of
  (is (identical? last-index-of .lastIndexOf))

  (is (test/arity-error? (last-index-of)))
  (is (test/arity-error? (last-index-of "foo")))
  (is (test/arity-error? (last-index-of "too" "many" "args")))

  (is (test/type-error? (last-index-of nil "f")))
  (is (test/type-error? (last-index-of (hash-map 'a 1) (vector 'a 1))))

  (testing "lists"
    (is (= 2 (last-index-of '(a 1 a) 'a)))
    (is (= 1 (last-index-of '(a 1 a) 1)))

    (is (= 3 (last-index-of (list true false nil true false nil) true)))
    (is (= 4 (last-index-of (list true false nil true false nil) false)))
    (is (= 5 (last-index-of (list true false nil true false nil) nil)))

    (is (= -1 (last-index-of '(a 1 a) 'b)))
    (is (= -1 (last-index-of '(a 1 a) "a")))
    (is (= -1 (last-index-of '(a 1 a) '#:a)))
    (is (= -1 (last-index-of '(a 1 a) #\a)))
    (is (= -1 (last-index-of '(a 1 a) nil)))
    (is (= -1 (last-index-of '(a 1 a) 1.0)))
    (is (= -1 (last-index-of '(a 1 a) '(a 1 a))))
    (is (= -1 (last-index-of '(a 1 a) '(a))))
    (is (= -1 (last-index-of '() 'a))))

  (testing "vectors"
    (is (= 2 (last-index-of (vector 'a 1 'a) 'a)))
    (is (= 1 (last-index-of (vector 'a 1 'a) 1)))

    (is (= 3 (last-index-of (vector true false nil true false nil) true)))
    (is (= 4 (last-index-of (vector true false nil true false nil) false)))
    (is (= 5 (last-index-of (vector true false nil true false nil) nil)))

    (is (= -1 (last-index-of (vector 'a 1 'a) 'b)))
    (is (= -1 (last-index-of (vector 'a 1 'a) "a")))
    (is (= -1 (last-index-of (vector 'a 1 'a) '#:a)))
    (is (= -1 (last-index-of (vector 'a 1 'a) #\a)))
    (is (= -1 (last-index-of (vector 'a 1 'a) nil)))
    (is (= -1 (last-index-of (vector 'a 1 'a) 1.0)))
    (is (= -1 (last-index-of (vector 'a 1 'a) (vector 'a 1 'a))))
    (is (= -1 (last-index-of (vector 'a 1 'a) (vector 'a))))
    (is (= -1 (last-index-of (vector 'a 1 'a) '(a 1 a))))
    (is (= -1 (last-index-of (vector 'a 1 'a) '(a))))
    (is (= -1 (last-index-of (vector) 'a))))

  (testing "strings"
    (is (= 0 (last-index-of "foo" "foo")))
    (is (= 3 (last-index-of "foofoo" "foo")))
    (is (= 3 (last-index-of "foo" "")))
    (is (= 0 (last-index-of "foo" "f")))
    (is (= 2 (last-index-of "foo" "o")))
    (is (= 1 (last-index-of "foo" "oo")))

    (is (= -1 (last-index-of "foo" "bar")))
    (is (= -1 (last-index-of "" "bar")))

    (is (test/type-error? (last-index-of "foo" (seq "f"))))
    (is (test/type-error? (last-index-of "foo" #\f)))
    (is (test/type-error? (last-index-of "foo" 'f)))
    (is (test/type-error? (last-index-of "foo" nil))))

  (testing "seqs"
    (is (= 2 (last-index-of (seq (vector 'a 1 'a)) 'a)))
    (is (= 1 (last-index-of (seq (vector 'a 1 'a)) 1)))
    (is (= -1 (last-index-of (seq (vector 'a 1 'a)) 'b)))

    (is (= 3 (last-index-of (cons 'b (vector 'a 1 'b)) 'b)))
    (is (= 0 (last-index-of (cons 'b (vector 'a 1 'a)) 'b)))
    (is (= 2 (last-index-of (cons 'b (vector 'a 1 'a)) 1)))
    (is (= -1 (last-index-of (cons 'b (vector 'a 1 'a)) 'c)))

    (is (= -1 (last-index-of (seq "foo") "f")))
    (is (= 0 (last-index-of (seq "foo") #\f)))
    (is (= 2 (last-index-of (seq "foo") #\o)))

    (is (= -1 (last-index-of (cons #\f "foo") "f")))
    (is (= 1 (last-index-of (cons #\f "foo") #\f)))
    (is (= 3 (last-index-of (cons #\f "foo") #\o)))

    (is (= 0 (last-index-of (seq (hash-map 'a 1)) (vector 'a 1))))
    (is (= -1 (last-index-of (seq (hash-map 'a 1)) (vector 'a 2))))))
