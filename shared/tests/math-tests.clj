
(deftest test-+
  (is (= 2 (+ 1 1))))

(deftest test--
  (is (= -1 (- 1 2))))

(deftest test-*
  (is (* -4 (+ 2 -2))))

(deftest test-/
  (is (= 2 (/ 4 2))))


(deftest test-inc
  (is (= -1 (inc -2)))
  (is (=  0 (inc -1)))
  (is (=  1 (inc  0)))
  (is (=  2 (inc  1)))

  (is (= -1.0 (inc -2.0)))
  (is (= -0.5 (inc -1.5)))
  (is (=  0.0 (inc -1.0)))
  (is (=  0.5 (inc -0.5)))
  (is (=  1.0 (inc  0.0)))
  (is (=  1.5 (inc  0.5)))
  (is (=  2.0 (inc  1.0))))


(deftest test-dec
  (is (= -2 (dec -1)))
  (is (= -1 (dec  0)))
  (is (=  0 (dec  1)))
  (is (=  1 (dec  2)))

  (is (= -2.0 (dec -1.0)))
  (is (= -1.5 (dec -0.5)))
  (is (= -1.0 (dec  0.0)))
  (is (= -0.5 (dec  0.5)))
  (is (=  0.0 (dec  1.0)))
  (is (=  0.5 (dec  1.5)))
  (is (=  1.0 (dec  2.0))))


(deftest test-quot
  (is (=  1 (quot  1   1)))
  (is (=  5 (quot 10   2)))
  (is (=  0 (quot 99 100)))

  (is (=  2 (quot  11  4)))
  (is (= -2 (quot  11 -4)))
  (is (= -2 (quot -11  4)))
  (is (=  2 (quot -11 -4)))

  (is (test/approx=  7.0 (quot  10  1.4)))
  (is (test/approx= -7.0 (quot -10  1.4)))
  (is (test/approx= -7.0 (quot  10 -1.4)))
  (is (test/approx=  7.0 (quot -10 -1.4)))

  (is (test/error? (quot 1 0)))

  (is (test/arity-error? (quot)))
  (is (test/arity-error? (quot 1)))
  (is (test/arity-error? (quot 1 2 3))))


(deftest test-rem
  (is (=  0 (rem  1   1)))
  (is (=  0 (rem 10   2)))
  (is (= 99 (rem 99 100)))

  (is (=  3 (rem  11  4)))
  (is (=  3 (rem  11 -4)))
  (is (= -3 (rem -11  4)))
  (is (= -3 (rem -11 -4)))

  (is (test/approx=  0.2 (rem  10  1.4)))
  (is (test/approx=  0.2 (rem  10 -1.4)))
  (is (test/approx= -0.2 (rem -10  1.4)))
  (is (test/approx= -0.2 (rem -10 -1.4)))

  (is (test/error? (rem 1 0)))

  (is (test/arity-error? (rem)))
  (is (test/arity-error? (rem 1)))
  (is (test/arity-error? (rem 1 2 3))))


(deftest test-mod
  (is (=  0 (mod  1   1)))
  (is (=  0 (mod 10   2)))
  (is (= 99 (mod 99 100)))

  (is (=  3 (mod  11  4)))
  (is (= -1 (mod  11 -4)))
  (is (=  1 (mod -11  4)))
  (is (= -3 (mod -11 -4)))

  (is (test/approx=  0.2 (mod  10  1.4)))
  (is (test/approx= -1.2 (mod  10 -1.4)))
  (is (test/approx=  1.2 (mod -10  1.4)))
  (is (test/approx= -0.2 (mod -10 -1.4)))

  (is (test/error? (mod 1 0)))

  (is (test/arity-error? (mod)))
  (is (test/arity-error? (mod 1)))
  (is (test/arity-error? (mod 1 2 3))))


(deftest test-<
  (is (true? (< 1 2)))
  (is (false? (< 2 1)))
  (is (true? (< 1 2 3 4)))
  (is (false? (< 1 2 4 3))))


(deftest test-<=
  (is (true? (<= 1 2)))
  (is (true? (<= 2 2)))
  (is (false? (<= 2 1)))
  (is (true? (<= 1 2 3 4)))
  (is (true? (<= 1 2 2 3 4)))
  (is (false? (<= 1 2 4 2 3))))


(deftest test->
  (is (true? (> 2 1)))
  (is (false? (> 1 2)))
  (is (true? (> 4 3 2 1)))
  (is (false? (> 4 3 1 2))))


(deftest test->=
  (is (true? (>= 2 1)))
  (is (true? (>= 2 2)))
  (is (false? (>= 1 2)))
  (is (true? (>= 4 3 2 1)))
  (is (true? (>= 4 3 2 2 1)))
  (is (false? (>= 3 2 4 2 1))))


(deftest test-max
  (is (= 1 (max 1)))
  (is (= 2 (max 1 2)))
  (is (= 3.1 (max 1 2 3.1 1.5 0 3 -3)))

  (is (= 1.0 (max 1   1.0)))
  (is (= 1   (max 1.0 1)))

  (is (test/arity-error? (max)))

  (is (test/type-error? (max 1 2 "3")))
  (is (test/type-error? (max 1 2 'a)))
  (is (test/type-error? (max 1 2 true)))
  (is (test/type-error? (max 1 2 false)))
  (is (test/type-error? (max 1 2 nil)))

  (is (= "1" (max "1")))
  (is (= 'a (max 'a)))
  (is (= true (max true)))
  (is (= false (max false)))
  (is (= nil (max nil))))


(deftest test-min
  (is (= 1 (min 1)))
  (is (= 1 (min 1 2)))
  (is (= -3 (min 1 2 3.1 1.5 0 3 -3)))

  (is (= 1.0 (min 1   1.0)))
  (is (= 1   (min 1.0 1)))

  (is (test/arity-error? (min)))

  (is (test/type-error? (min 1 2 "3")))
  (is (test/type-error? (min 1 2 'a)))
  (is (test/type-error? (min 1 2 true)))
  (is (test/type-error? (min 1 2 false)))
  (is (test/type-error? (min 1 2 nil)))

  (is (= "1" (min "1")))
  (is (= 'a (min 'a)))
  (is (= true (min true)))
  (is (= false (min false)))
  (is (= nil (min nil))))


(deftest test-pos?
  (is (false? (pos? -2)))
  (is (false? (pos? -1)))
  (is (false? (pos? 0)))
  (is (true?  (pos? 1)))
  (is (true?  (pos? 2)))

  (is (false? (pos? -2.0)))
  (is (false? (pos? -1.0)))
  (is (false? (pos? -0.5)))
  (is (false? (pos? -0.0)))
  (is (false? (pos? 0.0)))
  (is (true?  (pos? 0.5)))
  (is (true?  (pos? 1.0)))
  (is (true?  (pos? 2.0))))


(deftest test-neg?
  (is (true?  (neg? -2)))
  (is (true?  (neg? -1)))
  (is (false? (neg? 0)))
  (is (false? (neg? 1)))
  (is (false? (neg? 2)))

  (is (true?  (neg? -2.0)))
  (is (true?  (neg? -1.0)))
  (is (true?  (neg? -0.5)))
  (is (false? (neg? -0.0)))
  (is (false? (neg? 0.0)))
  (is (false? (neg? 0.5)))
  (is (false? (neg? 1.0)))
  (is (false? (neg? 2.0))))


(deftest test-zero?
  (is (false? (zero? -2)))
  (is (false? (zero? -1)))
  (is (true?  (zero? 0)))
  (is (false? (zero? 1)))
  (is (false? (zero? 2)))

  (is (false? (zero? -2.0)))
  (is (false? (zero? -1.0)))
  (is (false? (zero? -0.5)))
  (is (true?  (zero? -0.0)))
  (is (true?  (zero? 0.0)))
  (is (false? (zero? 0.5)))
  (is (false? (zero? 1.0)))
  (is (false? (zero? 2.0))))


(deftest test-even?
  (is (true? (even? -100)))
  (is (true? (even? -2)))
  (is (true? (even? 0)))
  (is (true? (even? 2)))
  (is (true? (even? 100)))

  (is (false? (even? -101)))
  (is (false? (even? -1)))
  (is (false? (even? 1)))
  (is (false? (even? 101)))

  (is (test/type-error? (even? 2.0)))
  (is (test/type-error? (even? 0.5)))

  (is (test/type-error? (even? 'a)))
  (is (test/type-error? (even? true)))
  (is (test/type-error? (even? false)))
  (is (test/type-error? (even? nil))))


(deftest test-odd?
  (is (true? (odd? -101)))
  (is (true? (odd? -1)))
  (is (true? (odd? 1)))
  (is (true? (odd? 101)))

  (is (false? (odd? -100)))
  (is (false? (odd? -2)))
  (is (false? (odd? 0)))
  (is (false? (odd? 2)))
  (is (false? (odd? 100)))

  (is (test/type-error? (odd? 1.0)))
  (is (test/type-error? (odd? 0.5)))

  (is (test/type-error? (odd? 'a)))
  (is (test/type-error? (odd? true)))
  (is (test/type-error? (odd? false)))
  (is (test/type-error? (odd? nil))))


(deftest test-number?
  (is (true? (number? -10.0)))
  (is (true? (number? -2.5)))
  (is (true? (number? -1)))
  (is (true? (number? -0.0)))
  (is (true? (number? 0)))
  (is (true? (number? 0.0)))
  (is (true? (number? 1)))
  (is (true? (number? 2.5)))
  (is (true? (number? 10.0)))

  (is (false? (number? 'a)))
  (is (false? (number? "1")))
  (is (false? (number? '(1))))
  (is (false? (number? (vector 1))))
  (is (false? (number? (hash-map 1 2)))))


(deftest test-integer?
  (is (true? (integer? -100)))
  (is (true? (integer? -1)))
  (is (true? (integer? 0)))
  (is (true? (integer? 1)))
  (is (true? (integer? 100)))

  (is (false? (integer? -10.0)))
  (is (false? (integer? -2.5)))
  (is (false? (integer? -0.0)))
  (is (false? (integer? 0.0)))
  (is (false? (integer? 2.5)))
  (is (false? (integer? 10.0)))

  (is (false? (integer? 'a)))
  (is (false? (integer? "1")))
  (is (false? (integer? '(1))))
  (is (false? (integer? (vector 1))))
  (is (false? (integer? (hash-map 1 2)))))


(deftest test-float?
  (is (true? (float? -10.0)))
  (is (true? (float? -2.5)))
  (is (true? (float? -0.0)))
  (is (true? (float? 0.0)))
  (is (true? (float? 2.5)))
  (is (true? (float? 10.0)))

  (is (false? (float? -100)))
  (is (false? (float? -1)))
  (is (false? (float? 0)))
  (is (false? (float? 1)))
  (is (false? (float? 100)))

  (is (false? (float? 'a)))
  (is (false? (float? "1.0")))
  (is (false? (float? '(1.0))))
  (is (false? (float? (vector 1.0))))
  (is (false? (float? (hash-map 1.0 2.0)))))



(defn _make-in-range? [n type?]
  (cond
    (zero? n) (fn [x] (and (type? x) (zero? x)))
    (neg? n)  (fn [x] (and (type? x) (<= x 0) (< n x)))
    (pos? n)  (fn [x] (and (type? x) (>= x 0) (> n x)))))

(defn _range-test-message [n]
  (cond
    (zero? n) (str "expected rand results to be zero")
    (neg? n)  (str "expected rand results in range (" n ", 0]")
    (pos? n)  (str "expected rand results in range [0, " n ")")))

(defn _do-rand-test [samples gen n type?]
  (let [xs            (map (fn [_] (gen))
                           (srfi-1/make-list samples nil))
        in-range?     (_make-in-range? n type?)
        out-of-range  (srfi-1/remove in-range? xs)]
    ;; TODO: check distribution e.g. http://fourmilab.ch/random/
    (is (empty? out-of-range)
        (_range-test-message n))))

(deftest test-rand
  (let [samples 100]
    (_do-rand-test samples (fn [] (rand))           1 float?)
    (_do-rand-test samples (fn [] (rand     1))     1 float?)
    (_do-rand-test samples (fn [] (rand     0))     0 float?)
    (_do-rand-test samples (fn [] (rand   1.5))   1.5 float?)
    (_do-rand-test samples (fn [] (rand  -1.5))  -1.5 float?)
    (_do-rand-test samples (fn [] (rand  1000))  1000 float?)
    (_do-rand-test samples (fn [] (rand -1000)) -1000 float?))

  (is (test/arity-error? (rand 1 2)))

  (is (test/type-error? (rand "a")))
  (is (test/type-error? (rand 'a)))
  (is (test/type-error? (rand #\a)))
  (is (test/type-error? (rand true)))
  (is (test/type-error? (rand false)))
  (is (test/type-error? (rand nil))))

(deftest test-rand-int
  (let [samples 100]
    (_do-rand-test samples (fn [] (rand-int     0))     0 integer?)
    (_do-rand-test samples (fn [] (rand-int     1))     0 integer?)
    (_do-rand-test samples (fn [] (rand-int    -1))     0 integer?)
    (_do-rand-test samples (fn [] (rand-int   1.1))     2 integer?)
    (_do-rand-test samples (fn [] (rand-int  -1.1))    -2 integer?)
    (_do-rand-test samples (fn [] (rand-int  1000))  1000 integer?)
    (_do-rand-test samples (fn [] (rand-int -1000)) -1000 integer?))

  (is (test/arity-error? (rand-int)))
  (is (test/arity-error? (rand-int 1 2)))

  (is (test/type-error? (rand-int "a")))
  (is (test/type-error? (rand-int 'a)))
  (is (test/type-error? (rand-int #\a)))
  (is (test/type-error? (rand-int true)))
  (is (test/type-error? (rand-int false)))
  (is (test/type-error? (rand-int nil))))
