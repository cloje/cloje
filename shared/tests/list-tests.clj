
(deftest test-list
  (is (= '()  (list)))
  (is (= '(a b c) (list 'a 'b 'c)))
  (is (= '(a (b c))
         (list 'a (list 'b 'c)))))



(deftest test-list?
  (is (true? (list? (list))))
  (is (true? (list? '())))
  (is (true? (list? '(1 2 3))))

  (is (false? (list? 'a)))
  (is (false? (list? 1)))
  (is (false? (list? true)))
  (is (false? (list? false)))
  (is (false? (list? nil))))



(deftest test-list*
  (is (test/arity-error? (list*)))
  (is (test/type-error? (list* 'a)))
  (is (test/type-error? (list* 'a 'b)))

  (is (= (list 'a 'b) (list* 'a 'b nil)))
  (is (= (list 'a 'b) (list* 'a 'b '())))

  (is (= (list 'a 'b 'c 'd)
         (list* 'a 'b (list 'c 'd))))

  (is (= (list 'a 'b 'c 'd)
         (list* 'a 'b (vector 'c 'd))))

  (is (= (list 'a 'b #\c #\d)
         (list* 'a 'b "cd")))

  (let [hm (hash-map 'c 3 'd 4)]
    (if (= 'c (first (first hm)))
      (is (= (list 'a 'b (vector 'c 3) (vector 'd 4))
             (list* 'a 'b hm)))
      (is (= (list 'a 'b (vector 'd 4) (vector 'c 3))
             (list* 'a 'b hm))))))
