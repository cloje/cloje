
(defn test/approx= [x y & epsilon]
  (let [e (if (scheme/null? epsilon) 0.0000001 (scheme/car epsilon))]
    (> e (scheme/abs (- x y)))))


(defn test/starts-with? [s1 s2]
  (= 0 (srfi-13/string-contains s1 s2)))
