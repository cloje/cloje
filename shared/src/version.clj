;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


;;; NOTE: This is the canonical definition of the Cloje version
;;; number. This expression is parsed by scripts, so be careful if you
;;; change the format of it. It must be the first expression in this
;;; file. If there is no qualifier string (fourth component), omit it
;;; entirely rather than using nil.
(def _cloje-version (list 0 3 0 "dev"))


(def *cloje-version*
  (hash-map (keyword "major")        (nth _cloje-version 0)
            (keyword "minor")        (nth _cloje-version 1)
            (keyword "incremental")  (nth _cloje-version 2)
            (keyword "qualifier")    (nth _cloje-version 3 nil)))


(defn cloje-version []
  (str (nth _cloje-version 0) "."
       (nth _cloje-version 1) "."
       (nth _cloje-version 2)
       (if-let [q (nth _cloje-version 3 nil)]
         (str "-" q)
         nil)))
