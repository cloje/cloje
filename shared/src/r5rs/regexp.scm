;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn _check-re-args [fn-name pattern input]
  (cond
    (not (re-pattern? pattern))
    (_raise-type-error fn-name "re-pattern?" pattern)

    (not (string? input))
    (_raise-type-error fn-name "string?" input)

    'else true))


(defn re-matcher [pattern input]
  (when (_check-re-args 're-matcher pattern input)
    (_make-re-matcher pattern input 0 nil)))


(def re-find (srfi-16/case-lambda
  ((pattern input)
   (re-find (re-matcher pattern input)))
  ((matcher)
   (if-let [start (_re-matcher-start matcher)]
     (let [pattern (_re-matcher-pattern matcher)
           input (_re-matcher-input matcher)
           match (_re-find pattern input start)]
       (_update-re-matcher! matcher match)
       (if match
         (re-groups matcher)
         nil))
     nil))))


(defn _update-re-matcher! [matcher last-match]
  (if last-match
    (do
      (set-_re-matcher-last-match! matcher last-match)
      (set-_re-matcher-start!
       matcher (_re-matcher-next-start matcher last-match)))
    (do
      (set-_re-matcher-last-match! matcher nil)
      (set-_re-matcher-start! matcher nil))))


(defn re-groups [matcher]
  (cond
    (not (re-matcher? matcher))
    (_raise-type-error 're-groups "re-matcher?" matcher)

    (or (nil? (_re-matcher-last-match matcher))
        (nil? (_re-matcher-start matcher)))
    (_raise-generic-error 're-groups "No match found")

    'else
    (let [groups (_re-groups (_re-matcher-last-match matcher))]
      (if (= 1 (scheme/vector-length groups))
        (first groups)
        groups))))


(defn re-matches [pattern input]
  (when (_check-re-args 're-seq pattern input)
    (let [matcher (re-matcher pattern input)
          match (_re-matches pattern input)]
      (if match
        (do (_update-re-matcher! matcher match)
            (re-groups matcher))
        nil))))


(defn re-seq [pattern input]
  (when (_check-re-args 're-seq pattern input)
    (_re-seq (re-matcher pattern input) nil)))

(defn _re-seq [m accum]
  (let [next (re-find m)]
    (if next
      (_re-seq m (cons next accum))
      (if (empty? accum)
        nil
        (scheme/reverse accum)))))
