;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn _clj-vseq-first [vseq]
  (let [v (_clj-vseq-v vseq)
        i (_clj-vseq-i vseq)]
    (if (< i (scheme/vector-length v))
        (scheme/vector-ref v i)
        nil)))

(defn _clj-vseq-rest [vseq & end]
  (let [v (_clj-vseq-v vseq)
        i (_clj-vseq-i vseq)]
    (if (< i (dec (scheme/vector-length v)))
        (_clj-vseq v (inc i))
        (if (scheme/null? end)
            '()
            (scheme/car end)))))


(defn _clj-sseq-first [sseq]
  (let [s (_clj-sseq-s sseq)
        i (_clj-sseq-i sseq)]
    (if (< i (scheme/string-length s))
        (scheme/string-ref s i)
        nil)))

(defn _clj-sseq-rest [sseq & end]
  (let [s (_clj-sseq-s sseq)
        i (_clj-sseq-i sseq)]
    (if (< i (dec (scheme/string-length s)))
        (_clj-sseq s (inc i))
        (if (scheme/null? end)
            '()
            (scheme/car end)))))

