;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


;;; This file defines the 'hash' function. If you're looking for hash
;;; maps, see map.scm. For hash sets, see set.scm.


(defn hash [x]
  (cond
   (string? x)     (_string-hash x)
   (_hash-set? x)  (_hash-set-hash x)
   'else           (_equal-hash x)))
