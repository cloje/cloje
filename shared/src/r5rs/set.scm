;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn _hash-set-print [hs port]
  (scheme/display "#{" port)
  (let [keys (_hash-set-keys hs)]
    (scheme/display (pr-str (first keys)) port)
    (scheme/for-each
     (fn [key]
       (scheme/display " " port)
       (scheme/display (pr-str key) port))
     (rest keys)))
  (scheme/display "}" port))


(defn _hash-set= [hs1 hs2]
  (and (scheme/= (_hash-set-count hs1)
                 (_hash-set-count hs2))
       (srfi-1/every
        (fn [key] (_hash-set-contains? hs2 key))
        (_hash-set-keys hs1))))


(defn _hash-set-hash [hs]
  (+ (hash hash-set) (_equal-hash (_hash-set-table hs))))


(defn _hash-set-count [hs]
  (_hash-map-count (_hash-set-table hs)))

(defn _hash-set-keys [hs]
  (_hash-map-keys (_hash-set-table hs)))

(defn _hash-set-contains? [hs key]
  (_hash-map-has-key? (_hash-set-table hs) key))

(defn _hash-set-get [hs key not-found]
  (_hash-map-get (_hash-set-table hs) key not-found))

(defn _hash-set-into [hs keys]
  (_make-hash-set
   (_hash-map-assoc
    (_hash-set-table hs)
    (srfi-1/append-map
     (fn [key] (list key key))
     (srfi-1/delete-duplicates keys =)))))


(defn hash-set [& keys]
  (_make-hash-set
   (apply hash-map
          (srfi-1/append-map
           (fn [key] (list key key))
           (srfi-1/delete-duplicates keys =)))))


(defn set? [x]
  (_hash-set? x))


(defn set [coll]
  (_assert-type 'set seqable? coll)
  (apply hash-set (into '() coll)))


(defn disj [set & keys]
  (if (nil? set)
    nil
    (do (_assert-type 'disj set? set)
        (apply hash-set (srfi-1/lset-difference
                         = (_hash-set-keys set) keys)))))



(defn set/union
  "Returns a hash-set that contains all distinct elements from the
  given collections. Accepts arguments of any seqable collection
  type (list, vector, map, set, string), sequence type, or nil. Throws
  an error if any argument is a type other than a seqable collection,
  a sequence, or nil.

  If there are repeated elements (i.e. multiple elements that are the
  same according to the \"hash\" and \"=\" functions) in the given
  collections, exactly one of the repeated elements will appear in the
  returned set. But, there is no guarantee of which of the repeated
  elements will be used. This is deliberately left ambiguous to allow
  for optimization, and for compatibility with Clojure."

  [& colls]
  (scheme/for-each
   (fn [c] (_assert-type 'set/union seqable? c))
   colls)
  (reduce into (hash-set) colls))


(defn set/difference
  "Returns a hash-set that contains the elements of the first
  collection without the elements of the other collections. Accepts
  arguments of any seqable collection type (list, vector, map, set,
  string), sequence type, or nil. Throws an error if any argument is a
  type other than a seqable collection, a sequence, or nil."

  [coll & more-colls]
  (_assert-type 'set/difference seqable? coll)
  (set (apply srfi-1/lset-difference
              =
              (into '() coll)
              (map (fn [c]
                     (_assert-type 'set/difference seqable? c)
                     (into '() c))
                   more-colls))))


(defn set/intersection
  "Returns a hash-set that contains each element in the first
  collection where that element also exists in every other given
  collection. Accepts arguments of any seqable collection type (list,
  vector, map, set, string), sequence type, or nil. Throws an error if
  any argument is a type other than a seqable collection, a sequence,
  or nil."

  [coll & more-colls]
  (_assert-type 'set/intersection seqable? coll)
  (set (apply srfi-1/lset-intersection
              =
              (into '() coll)
              (map (fn [c]
                     (_assert-type 'set/intersection seqable? c)
                     (into '() c))
                   more-colls))))


(defn set/select
  "Returns a hash-set that contains every element in coll for which
  pred returns logical truth (i.e. any value except false or nil).

  coll can be any seqable collection type (list, vector, map, set,
  string), sequence type, or nil. Throws an error if coll is a type
  other than a seqable collection, a sequence, or nil."

  [pred coll]
  (_assert-type 'set/select seqable? coll)
  (set (srfi-1/filter
        (fn [x] (host-boolean (pred x)))
        (into '() coll))))


(defn set/subset?
  "Returns true if every element of coll1 also exists in coll2.
  Otherwise returns false.

  Accepts arguments of any seqable collection type (list, vector, map,
  set, string), sequence type, or nil. Throws an error if any argument
  is a type other than a seqable collection, a sequence, or nil."

  [coll1 coll2]
  (_assert-type 'set/subset? seqable? coll1)
  (_assert-type 'set/subset? seqable? coll2)
  (let [s2 (if (set? coll2)
             coll2
             (set coll2))]
    (every? (fn [x] (contains? s2 x))
            coll1)))


(defn set/superset?
  "Returns true if every element of coll2 also exists in coll1.
  Otherwise returns false.

  Accepts arguments of any seqable collection type (list, vector, map,
  set, string), sequence type, or nil. Throws an error if any argument
  is a type other than a seqable collection, a sequence, or nil."

  [coll1 coll2]
  (_assert-type 'set/superset? seqable? coll1)
  (_assert-type 'set/superset? seqable? coll2)
  (set/subset? coll2 coll1))
