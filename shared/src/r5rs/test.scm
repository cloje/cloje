;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def test/*testing-contexts* (srfi-39/make-parameter nil))
(def test/*testing-vars* (srfi-39/make-parameter nil))


(define-syntax test/deftest
  (syntax-rules ()
    ((test/deftest name clause ...)
     (do (defn name []
           (srfi-39/parameterize ((test/*testing-vars*
                                   (cons 'name (test/*testing-vars*))))
             (when (*_cloje-test-tests-run*)
               (*_cloje-test-tests-run*
                (inc (*_cloje-test-tests-run*))))
             clause ...))
         (*_cloje-tests* (cons name (*_cloje-tests*)))))))


(define-syntax test/testing
  (syntax-rules ()
    ((test/testing summary test ...)
     (srfi-39/parameterize ((test/*testing-contexts*
                             (cons summary (test/*testing-contexts*))))
       test ...))))


(define-syntax test/is
  (syntax-rules ()
    ((test/is (op arg ...) message)
     (_catch-errors ((error) (_cloje-test-error
                              '(op arg ...)
                              error
                              message))
       (if-let [result (op arg ...)]
         (_cloje-test-pass result)
         (_cloje-test-fail '(op arg ...)
                             (list 'not (list 'op arg ...))
                             message))))
    ((test/is expr message)
     (_catch-errors ((error) (_cloje-test-error
                              'expr error message))
       (if-let [result expr]
         (_cloje-test-pass result)
         (_cloje-test-fail 'expr expr message))))
    ((test/is expr)
     (test/is expr nil))))


(defn test/run-all-tests []
  (srfi-39/parameterize ((*_cloje-test-tests-run* 0)
                         (*_cloje-test-passes* 0)
                         (*_cloje-test-fails* 0)
                         (*_cloje-test-errors* 0))
    (scheme/for-each (fn [test] (test)) (*_cloje-tests*))

    (let [tests-run  (*_cloje-test-tests-run*)
          passes     (*_cloje-test-passes*)
          failures   (*_cloje-test-fails*)
          errors     (*_cloje-test-errors*)
          assertions (+ passes failures errors)

          summary    (hash-map '#:type '#:summary
                               '#:test tests-run
                               '#:pass passes
                               '#:fail failures
                               '#:error errors)]
      (_cloje-print-test-summary summary)
      summary)))


(defn test/success? [summary]
  (zero? (+ (get summary '#:fail)
            (get summary '#:error))))




(def *_cloje-tests* (srfi-39/make-parameter nil))

(def *_cloje-test-tests-run* (srfi-39/make-parameter nil))
(def *_cloje-test-passes* (srfi-39/make-parameter nil))
(def *_cloje-test-fails* (srfi-39/make-parameter nil))
(def *_cloje-test-errors* (srfi-39/make-parameter nil))


(defn _cloje-test-pass [result]
  (when (*_cloje-test-passes*)
    (*_cloje-test-passes* (inc (*_cloje-test-passes*))))
  result)


(defn _cloje-test-fail [expected actual message]
  (when (*_cloje-test-fails*)
    (*_cloje-test-fails* (inc (*_cloje-test-fails*))))
  (_cloje-print-test-result "FAIL" expected actual message)
  false)


(defn _cloje-test-error [expected error message]
  (when (*_cloje-test-errors*)
    (*_cloje-test-errors* (inc (*_cloje-test-errors*))))
  (_cloje-print-test-result "ERROR" expected error message)
  nil)


(defn _cloje-print-test-result [action expected actual message]
  (newline)
  (if (test/*testing-vars*)
      (println action "in" (scheme/reverse (test/*testing-vars*)))
      (println action))
  (when (test/*testing-contexts*)
    (scheme/apply println (scheme/reverse (test/*testing-contexts*))))
  (when message
    (println message))
  (print "expected: ")
  (prn expected)
  (print "  actual: ")
  (prn actual))


(defn _cloje-print-test-summary [summary]
  (let [tests-run  (get summary '#:test)
        passes     (get summary '#:pass)
        failures   (get summary '#:fail)
        errors     (get summary '#:error)
        assertions (+ passes failures errors)]
    (println
     (_format
      "~nRan ~a tests containing ~a assertions.~n~a failures, ~a errors."
      tests-run assertions failures errors))))
