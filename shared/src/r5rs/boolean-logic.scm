;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def true #t)
(def false #f)

(defn true? [x] (identical? true x))
(defn false? [x] (identical? false x))


(defn host-boolean [x]
  (scheme/if (scheme/or (false? x) (nil? x) (scheme/not x))
             #f
             #t))

(def boolean host-boolean)

(defn not [x]
  (scheme/not (host-boolean x)))


(define-syntax and
  (syntax-rules ()
    ((and)
     true)
    ((and expr)
     expr)
    ((and expr exprs ...)
     (let [expr-value expr]
       (if-not expr-value expr-value (and exprs ...))))))


(define-syntax or
  (syntax-rules ()
    ((or)
     nil)
    ((or expr)
     expr)
    ((or expr exprs ...)
     (let [expr-value expr]
       (if expr-value expr-value (or exprs ...))))))
