;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(define-syntax defn
  (syntax-rules ()
    ((defn name [param ...] body ...)
     (def name (fn [param ...] body ...)))
    ((defn name docstring [param ...] body ...)
     (def name docstring (fn [param ...] body ...)))))


(define-syntax fn
  (syntax-rules (&)
    ((fn)
     (_raise-arity-error 'fn (_arity-at-least 2) '()))

    ;; Single-arity fn with only rest param
    ((fn [& rest] expr ...)
     (scheme/lambda rest (_fn-body expr ...)))

    ;; Single-arity fn with normal params and rest param
    ((fn [param ... & rest] expr ...)
     (scheme/lambda (param ... . rest) (_fn-body expr ...)))

    ;; Single-arity fn
    ((fn [param ...] expr ...)
     (scheme/lambda (param ...) (_fn-body expr ...)))

    ;; Named single-arity fn (name is discarded)
    ((fn name [param ...] expr ...)
     (fn [param ...] expr ...))

    ((fn otherwise)
     (_raise-arity-error 'fn (_arity-at-least 2) '(otherwise)))))

(define-syntax _fn-body
  (syntax-rules ()
    ((_fn-body)
     nil)
    ((_fn-body expr ...)
     (scheme/begin expr ...))))


(def fn? scheme/procedure?)
