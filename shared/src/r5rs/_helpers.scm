;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(define-syntax _even-form-guard
  (syntax-rules ()
    ((_even-form-guard (form1 form2 more ...) pass-expr fail-expr)
     (_even-form-guard (more ...) pass-expr fail-expr))
    ((_even-form-guard (form1) pass-expr fail-expr)
     fail-expr)
    ((_even-form-guard () pass-expr fail-expr)
     pass-expr)))


(scheme/define (_plist->alist plist accum)
  (scheme/if (scheme/null? plist)
     accum
     (_plist->alist
      (scheme/cddr plist)
      (scheme/append
       (scheme/list (scheme/cons
                     (scheme/car plist)
                     (scheme/cadr plist)))
       accum))))


;;; Checks that start and end are in-bounds for coll, and that start
;;; <= end. Returns true if everything is okay, otherwise raises a
;;; bounds error.
(define-syntax _assert-bounds-range
  (syntax-rules ()
    ((_assert-bounds-range fn-name type-desc coll min max start end)
     (cond
      (not (and (integer? start) (<= min start max)))
      (_raise-bounds-error fn-name type-desc "starting " start
                           coll min (dec max) false)

      (not (and (integer? end) (<= min start end max)))
      (_raise-bounds-error fn-name "string" "ending " end
                           coll start (dec max) min)

      'else true))))


(define-syntax _assert-type
  (syntax-rules ()
    ((_assert-type fn-name type-pred? val)
     (scheme/or
      (type-pred? val)
      (_raise-type-error fn-name
                         (scheme/symbol->string 'type-pred?)
                         val)))))
