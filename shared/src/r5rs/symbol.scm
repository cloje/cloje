;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def gensym (srfi-16/case-lambda
  (()
   (gensym "G__"))
  ((prefix)
   (host/gensym (str prefix)))))


(defn symbol [s]
  (cond
   (symbol? s)  s
   (string? s)  (scheme/string->symbol s)
   'else        (_raise-type-error 'symbol "string?" s)))


(defn symbol? [x]
  (and (scheme/symbol? x)
       (not (host/keyword? x))))


(defn keyword [s]
  (cond
   (keyword? s)  s
   (symbol? s)   (keyword (scheme/symbol->string s))
   (string? s)   (host/string->keyword s)
   'else         nil))


(def keyword? host/keyword?)
