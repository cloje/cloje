;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn _seq-unsupported [fn-name coll]
  (_raise-type-error fn-name "seqable?" coll))


(defn seq [coll]
  (cond
   (scheme/pair? coll)  coll
   (empty? coll)        nil
   (vector? coll)       (_clj-vseq coll 0)
   (string? coll)       (_clj-sseq coll 0)
   (_hash-map? coll)    (_hash-map-seq coll)
   (_hash-set? coll)    (_hash-set-keys coll)
   (seq? coll)          coll
   'else                (_seq-unsupported 'seq coll)))


(defn seq? [x]
  (or (list? x)
      (scheme/pair? x)
      (_clj-vseq? x)
      (_clj-sseq? x)))


(defn seqable? [x]
  (or (seq? x)
      (nil? x)
      (list? x)
      (scheme/pair? x)
      (vector? x)
      (string? x)
      (_hash-map? x)
      (_hash-set? x)))


(def apply (srfi-16/case-lambda
  ((f)
   (_raise-arity-error 'apply (_arity-at-least 2) (list f)))
  ((f coll)
   (scheme/apply f (scheme/reverse (into '() coll))))
  ((f . more)
   (apply f (scheme/apply list* more)))))


(defn conj [coll x & xs]
  (if (or (nil? coll)
          (sequential? coll)
          (_hash-map? coll)
          (_hash-set? coll))
      (into coll (scheme/cons x xs))
      (_seq-unsupported 'conj coll)))


(defn cons [x coll]
  (cond
   (nil? coll)       (list x)
   (seq? coll)       (scheme/cons x coll)
   (seqable? coll)   (cons x (seq coll))
   'else             (_seq-unsupported 'cons coll)))


(defn ffirst [coll]
  (first (first coll)))


(defn first [coll]
  (cond
   (scheme/pair? coll) (scheme/car coll)
   (empty? coll)       nil
   (list? coll)        (_list-nth   coll 0)
   (vector? coll)      (_vector-get coll 0)
   (string? coll)      (_string-get coll 0)
   (_clj-vseq? coll)   (_clj-vseq-first coll)
   (_clj-sseq? coll)   (_clj-sseq-first coll)
   (seqable? coll)     (first (seq coll))
   'else               (_seq-unsupported 'first coll)))


(defn fnext [coll]
  (first (next coll)))


(defn into [seq1 seq2]
  (cond
   (empty? seq2)      seq1
   (nil? seq1)        (into '() seq2)
   (list? seq1)       (into (cons (first seq2) seq1) (rest seq2))
   (vector? seq1)     (srfi-43/vector-append
                       seq1 (scheme/list->vector
                             (scheme/reverse (into '() seq2))))
   (_hash-map? seq1)  (_hash-map-into seq1 (into '() seq2))
   (_hash-set? seq1)  (_hash-set-into seq1 (into '() seq2))
   'else              (_seq-unsupported 'into seq1)))


(defn last [coll]
  (cond
   (empty? coll)     nil
   (list? coll)      (_list-nth   coll (dec (count coll)) nil)
   (vector? coll)    (_vector-get coll (dec (count coll)) nil)
   (string? coll)    (_string-get coll (dec (count coll)) nil)
   (seq? coll)       (nth coll (dec (count coll)) nil)
   (seqable? coll)   (last (seq coll))
   'else             (_seq-unsupported 'last coll)))


(defn map [f seq & more]
  (let [seqs (scheme/cons seq more)]
    (if (every? not-empty seqs)
        (cons (scheme/apply f (scheme/map first seqs))
              (scheme/apply map f (scheme/map rest seqs)))
        '())))


(defn next [coll]
  (cond
   (seqable? coll)  (seq (rest coll))
   'else            (_seq-unsupported 'next coll)))


(defn nfirst [coll]
  (next (first coll)))


(defn nnext [coll]
  (next (next coll)))


(def nth (srfi-16/case-lambda
  ((coll i)
   (cond
    (float? i)          (nth coll
                             (scheme/inexact->exact (scheme/floor i)))
    (not (integer? i))  (_raise-type-error 'nth "integer?" i)
    (nil? coll)         nil
    (list? coll)        (_list-nth   coll i)
    (vector? coll)      (_vector-get coll i)
    (string? coll)      (_string-get coll i)
    (_clj-vseq? coll)   (nth (_clj-vseq-v coll) i)
    (_clj-sseq? coll)   (nth (_clj-sseq-s coll) i)
    'else               (_seq-unsupported 'nth coll)))
  ((coll i not-found)
   (cond
    (float? i)          (nth coll
                             (scheme/inexact->exact (scheme/floor i))
                             not-found)
    (not (integer? i))  (_raise-type-error 'nth "integer?" i)
    (nil? coll)         not-found
    (list? coll)        (_list-nth   coll i not-found)
    (vector? coll)      (_vector-get coll i not-found)
    (string? coll)      (_string-get coll i not-found)
    (_clj-vseq? coll)   (nth (_clj-vseq-v coll) i not-found)
    (_clj-sseq? coll)   (nth (_clj-sseq-s coll) i not-found)
    'else               (_seq-unsupported 'nth coll)))
  (otherwise
   (_raise-arity-error 'nth (_normalize-arity '(2 3)) otherwise))))


(defn peek [coll]
  (cond
   (list? coll)      (first coll)
   (vector? coll)    (last coll)
   (nil? coll)       nil
   'else             (_seq-unsupported 'peek coll)))


(defn pop [coll]
  (cond
   (list? coll)      (scheme/cdr coll)
   (vector? coll)    (subvec coll 0 (dec (count coll)))
   (nil? coll)       nil
   'else             (_seq-unsupported 'pop coll)))


(def reduce (srfi-16/case-lambda
  ((f coll)
   (_assert-type 'reduce seqable? coll)
   (case (count coll)
     0 (f)
     1 (first coll)
     (let [a (first coll)
           b (first (rest coll))
           more (rest (rest coll))]
       (reduce f (f a b) more))))
  ((f val coll)
   (_assert-type 'reduce seqable? coll)
   (if (empty? coll)
     val
     (reduce f (f val (first coll)) (rest coll))))
  (otherwise
   (_raise-arity-error
    'reduce (_normalize-arity '(2 3)) otherwise))))


(defn rest [coll]
  (cond
   (nil? coll)         '()
   (scheme/null? coll) '()
   (scheme/pair? coll) (scheme/cdr coll)
   (_clj-vseq? coll)   (_clj-vseq-rest coll)
   (_clj-sseq? coll)   (_clj-sseq-rest coll)
   (seqable? coll)     (rest (seq coll))
   'else               (_seq-unsupported 'rest coll)))


(defn index-of [coll x]
  (cond
   (nil? coll)     (_seq-unsupported 'index-of coll)
   (string? coll)  (string/index-of coll x)
   (vector? coll)  (_vector-index-of coll x)
   (seq? coll)     (_seq-index-of coll x 0)
   'else           (_seq-unsupported 'index-of coll)))

(def .indexOf index-of)

(defn _seq-index-of [coll x i]
  (cond
   (empty? coll)       -1
   (= x (first coll))  i
   'else               (_seq-index-of (rest coll) x (inc i))))


(defn last-index-of [coll x]
  (cond
   (nil? coll)     (_seq-unsupported 'last-index-of coll)
   (string? coll)  (string/last-index-of coll x)
   (vector? coll)  (_vector-last-index-of coll x)
   (seq? coll)     (_seq-last-index-of coll x 0 -1)
   'else           (_seq-unsupported 'last-index-of coll)))

(def .lastIndexOf last-index-of)

(defn _seq-last-index-of [coll x i prev-found]
  (cond
   (empty? coll)       prev-found
   (= x (first coll))  (_seq-last-index-of
                        (rest coll) x (inc i) i)
   'else               (_seq-last-index-of
                        (rest coll) x (inc i) prev-found)))
