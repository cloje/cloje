;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn string/replace [s match replacement]
  (_string/replace-n s match replacement -1))

(defn string/replace-first [s match replacement]
  (_string/replace-n s match replacement 1))


(defn _string/replace-n [s match rep limit]
  (cond
   (re-pattern? match)
   (__string/replace-n_re s match rep limit 0)

   (and (char? match) (char? rep))
   (_string/replace-n s (str match) (str rep) limit)

   (not (string? match))
   (_raise-type-error 'string/replace "string?" match)

   (not (string? rep))
   (_raise-type-error 'string/replace "string?" rep)

   'else
   (__string/replace-n (str s) match rep limit 0)))


(defn __string/replace-n [s match rep limit start]
  (if (or (= 0 limit)
          (> start (scheme/string-length s)))
    s
    (let [i (srfi-13/string-contains s match start)]
      (if i
        (__string/replace-n
         (srfi-13/string-replace
          s rep i (+ i (scheme/string-length match)))
         match rep
         (dec limit)
         (+ i (scheme/string-length rep)))
        s))))


(defn __string/replace-n_re [input pattern rep limit start]
  (if (or (= 0 limit)
          (> start (scheme/string-length input)))
    input
    (if-let [match (_re-find pattern input start)]
      (let [match-range (first (_re-match-ranges match))
            match-start (scheme/car match-range)
            match-end   (scheme/cdr match-range)
            replacement (_prepare-replacement rep match input)]
        (__string/replace-n_re
         (srfi-13/string-replace
          input replacement match-start match-end)
         pattern rep
         (dec limit)
         (+ match-start (scheme/string-length replacement))))
      input)))


(defn _prepare-replacement [rep match input]
  (cond
    (fn? rep)
    (let [groups (_re-groups match)]
      (if (= 1 (scheme/vector-length groups))
        (rep (first groups))
        (rep groups)))

    (string? rep)
    (if (re-find (re-pattern "(\\\\|\\$[0-9])") rep)
      (_replace-$-strs rep match input)
      rep)

    'else
    (_raise-type-error 'string/replace "string?" rep)))


(defn _replace-$-strs [rep match input]
  (__replace-$-strs rep 0 match input (_re-groups match)))

(defn __replace-$-strs [rep rep-start match input groups]
  (if-let [match2 (_re-find (re-pattern "(\\\\.|\\\\$|\\$[0-9])") rep rep-start)]
    (let [found (first (_re-groups match2))
          match2-range (first (_re-match-ranges match2))
          match2-start (scheme/car match2-range)
          match2-end   (scheme/cdr match2-range)]
      (if (= #\\ (scheme/string-ref found 0))
        (if (= 2 (scheme/string-length found))
          ;; Drop the backslash and proceed after the next char
          (__replace-$-strs
           (srfi-13/string-replace rep found match2-start match2-end 1)
           (dec match2-end)
           match input groups)
          ;; Backslash at end of string; raise error
          (_raise-generic-error 'string/replace
                                "character to be escaped is missing"))
        ;; Fill in the $-str and proceed
        (let [group (_$-str->group found groups)]
          (__replace-$-strs
           (srfi-13/string-replace rep group match2-start match2-end)
           (+ rep-start (scheme/string-length group))
           match input groups))))
    rep))


(defn _$-str->group [$-str groups]
  (let [i (_$-str->int $-str)]
    (or (nth groups i nil)
        (_raise-generic-error
         'string/replace (str "No group " i)))))

(defn _$-str->int [$-str]
  (case $-str
    "$0" 0  "$1" 1  "$2" 2  "$3" 3  "$4" 4
    "$5" 5  "$6" 6  "$7" 7  "$8" 8  "$9" 9
    (_raise-generic-error
     'string/replace (str "Invalid $-str: " $-str))))


(defn string/re-quote-replacement [s]
  (if (nil? s)
    (_raise-type-error 'string/re-quote-replacement "not nil?" s)
    (string/replace (str s) (re-pattern "\\\\|\\$")
                    (fn [x] (str "\\" x)))))
