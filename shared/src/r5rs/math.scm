;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def + scheme/+)
(def - scheme/-)
(def * scheme/*)
(def / scheme//)

(defn inc [x] (+ x 1))
(defn dec [x] (- x 1))


(defn quot [x y]
  (if (and (integer? x) (integer? y))
      (scheme/quotient x y)
      (scheme/truncate (/ x y))))

(defn rem [x y]
  (if (and (integer? x) (integer? y))
      (scheme/remainder x y)
      (- x (* (quot x y) y))))

(defn mod [x y]
  (if (or (and (neg? x) (pos? y))
          (and (pos? x) (neg? y)))
      (+ (rem x y) y)
      (rem x y)))


(def < scheme/<)
(def <= scheme/<=)
(def > scheme/>)
(def >= scheme/>=)


;;; You may wonder, why not just use Scheme's built-in max and min
;;; functions? The answer: they return a float if ANY of the arguments
;;; is a float, even if the max/min value was actually an integer.

(defn max [x & xs]
  (srfi-1/fold (fn [n mem] (if (>= n mem) n mem))
               x xs))

(defn min [x & xs]
  (srfi-1/fold (fn [n mem] (if (<= n mem) n mem))
               x xs))


(defn pos?  [x] (> x 0))
(defn neg?  [x] (< x 0))
(defn zero? [x] (scheme/= x 0))

(defn even? [x]
  (if (integer? x)
      (scheme/even? x)
      (_raise-type-error 'even? "integer?" x)))

(defn odd? [x]
  (if (integer? x)
      (scheme/odd? x)
      (_raise-type-error 'odd? "integer?" x)))


(def number? scheme/number?)

;; (def rational? scheme/rational?)

(defn integer? [x]
  (and (scheme/integer? x)
       (scheme/exact? x)))

;; ratio?
;; decimal?

(defn float? [x]
  (and (scheme/real? x)
       (scheme/inexact? x)))


(def rand (srfi-16/case-lambda
  (()  (srfi-27/random-real))
  ((n) (scheme/exact->inexact (* n (rand))))
  (otherwise
   (_raise-arity-error 'rand (_normalize-arity '(0 1)) otherwise))))

(defn rand-int [n]
  (cond
    (zero? n)  0
    (float? n) (rand-int
                (if (neg? n)
                  (scheme/inexact->exact (scheme/floor n))
                  (scheme/inexact->exact (scheme/ceiling n))))
    (neg? n)   (- (srfi-27/random-integer (- n)))
    (pos? n)   (srfi-27/random-integer n)
    'else      (_raise-type-error 'rand-int "number?" n)))
