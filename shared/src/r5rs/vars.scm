;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(define-syntax def
  (syntax-rules ()
    ((def name docstring expr)
     ;; TODO: do something with docstring
     (def name expr))
    ((def name expr)
     (define name expr))))
