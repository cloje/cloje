;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def newline scheme/newline)


(defn print [& args]
  (if (scheme/null? args)
      nil
      (let [arg  (scheme/car args)
            more (scheme/cdr args)]
        (scheme/display arg)
        (if (scheme/null? more)
            nil
            (do (scheme/display " ")
                (scheme/apply print more))))))


(defn println [& args]
  (scheme/apply print args)
  (newline))


(defn print-str [& args]
  (with-out-str (scheme/apply print args)))


(defn println-str [& args]
  (with-out-str (scheme/apply println args)))


(defn pr [& args]
  (if (scheme/null? args)
      nil
      (let [arg  (scheme/car args)
            more (scheme/cdr args)]
        (scheme/write arg)
        (if (scheme/null? more)
            nil
            (do (scheme/display " ")
                (scheme/apply pr more))))))


(defn prn [& args]
  (scheme/apply pr args)
  (newline))


(defn pr-str [& args]
  (with-out-str (scheme/apply pr args)))


(defn prn-str [& args]
  (with-out-str (scheme/apply prn args)))
