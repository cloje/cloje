;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def list scheme/list)
(def list? scheme/list?)

(defn list* [& args]
  (let [num-args (scheme/length args)]
    (if (> num-args 0)
        (scheme/call-with-values
           (fn [] (srfi-1/split-at args (- num-args 1)))
         (fn [earlier-args last-arg]
           (let [target-seq (seq (scheme/car last-arg))]
             (if (nil? target-seq)
                 earlier-args
                 (scheme/append earlier-args target-seq)))))
        (_raise-arity-error 'list* (_arity-at-least 1) args))))


(defn _list-nth [lst index & not-found]
  (if (and (scheme/integer? index)
           (< -1 index (scheme/length lst)))
      (scheme/list-ref lst index)
      (if (scheme/null? not-found)
          (_raise-bounds-error
           'nth "list" "" index
           lst 0 (dec (scheme/length lst)) false)
          (scheme/car not-found))))
