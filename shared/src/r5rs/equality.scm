;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def identical? scheme/eqv?)


(def = (srfi-16/case-lambda
  ((x) true)
  ((x y)
   (cond
     (and (scheme/string? x) (scheme/string? y))
     (scheme/string=? x y)

     (and (scheme/vector? x) (scheme/vector? y))
     (srfi-43/vector= = x y)

     (and (_hash-map? x) (_hash-map? y))
     (_hash-map= x y)

     (and (_hash-set? x) (_hash-set? y))
     (_hash-set= x y)

     (and (list? x) (list? y))
     (and (scheme/= (scheme/length x) (scheme/length y))
          (srfi-1/every = x y))

     (and (sequential? x) (sequential? y))
     (and (= (first x) (first y))
          (= (rest x) (rest y)))

     'else (scheme/eqv? x y)))
  ((x y . more)
   (and (= x y) (scheme/apply = y more)))))


(defn not= [& args]
  (not (scheme/apply = args)))
