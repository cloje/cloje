;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn _string-get [str key & not-found]
  (if (and (scheme/integer? key)
           (< -1 key (scheme/string-length str)))
      (scheme/string-ref str key)
      (if (scheme/null? not-found)
          (_raise-bounds-error
           'nth "string" "" key
           str 0 (dec (scheme/string-length str)) false)
          (scheme/car not-found))))


(def char? scheme/char?)

(def string? scheme/string?)


;; NOTE: R5RS does not specify the charset used for integer->char. It
;; just so happens that CHICKEN (with utf8 egg) and Racket both use
;; UTF-8. So, this only works by coincidence. Let's hope that the
;; tests will catch it if that changes or if other hosts differ.
(defn char [x]
  (if (char? x)
    x
    (when (_assert-type 'char number? x)
      (let [x-int (scheme/inexact->exact (scheme/floor x))]
        (if (<= 0 x-int 65535)
          (scheme/integer->char x-int)
          (_raise-generic-error
           'char
           (_format
            "Value out of range: ~a~n  valid range: [0, 65535]"
            x-int)))))))


(def str (srfi-16/case-lambda
  (()
   "")
  ((s)
   (if (nil? s)
       ""
       (print-str s)))
  (more
   (apply scheme/string-append "" (map str more)))))


(def subs (srfi-16/case-lambda
  ((s start)
   (subs s start (scheme/string-length s)))
  ((s start end)
   (_assert-bounds-range
    'subs "string" s 0 (scheme/string-length s) start end)
   (srfi-13/string-copy s start end))
  (otherwise
   (_raise-arity-error 'subs (_normalize-arity '(2 3)) otherwise))))


(defn string/blank? [s]
  (cond
    (nil? s)     true
    (string? s)  (= "" (string/trim s))
    'else        (_raise-type-error 'string/blank?
                                    "string? or nil?" s)))


(defn string/capitalize [s]
  (if (nil? s)
    (_raise-type-error 'string/capitalize "not nil?" s)
    (let [s-str (str s)]
      (str (srfi-13/string-upcase (subs s-str 0 1))
           (srfi-13/string-downcase (subs s-str 1))))))

(defn string/lower-case [s]
  (if (nil? s)
    (_raise-type-error 'string/lower-case "not nil?" s)
    (srfi-13/string-downcase (str s))))

(defn string/upper-case [s]
  (if (nil? s)
    (_raise-type-error 'string/upper-case "not nil?" s)
    (srfi-13/string-upcase (str s))))


(def string/index-of (srfi-16/case-lambda
  ((s1 s2 from-index)
   (_assert-type 'string/index-of string? s1)
   (_assert-type 'string/index-of string? s2)
   (_assert-type 'string/index-of integer? from-index)
   (or (srfi-13/string-contains s1 s2 (max from-index 0))
       -1))
  ((s1 s2)
   (string/index-of s1 s2 0))
  (otherwise
   (_raise-arity-error
    'string/index-of (_normalize-arity '(2 3)) otherwise))))


(def string/last-index-of (srfi-16/case-lambda
  ((s1 s2 from-index)
   (_assert-type 'string/last-index-of string? s1)
   (_assert-type 'string/last-index-of string? s2)
   (_assert-type 'string/last-index-of integer? from-index)
   (let [from-index (min from-index (scheme/string-length s1))]
     (_string/last-index-of s1 s2 from-index)))
  ((s1 s2)
   (string/last-index-of s1 s2 (scheme/string-length s1)))
  (otherwise
   (_raise-arity-error
    'string/last-index-of (_normalize-arity '(2 3)) otherwise))))

(defn _string/last-index-of [s1 s2 from-index]
  (if (< from-index 0)
    -1
    (let [found (srfi-13/string-contains s1 s2 from-index)]
      (if (= found from-index)
        found
        (_string/last-index-of s1 s2 (dec from-index))))))


(def string/join (srfi-16/case-lambda
  ((coll)
   (string/join "" coll))
  ((separator coll)
   (srfi-13/string-join (map str coll) (str separator) 'infix))
  (otherwise
   (_raise-arity-error 'string/join (_normalize-arity '(1 2)) otherwise))))


(defn string/reverse [s]
  (_assert-type 'string/reverse string? s)
  (srfi-13/string-reverse s))


(def string/starts-with? (srfi-16/case-lambda
  ((s prefix offset)
   (_assert-type 'string/starts-with? string? s)
   (_assert-type 'string/starts-with? string? prefix)
   (_assert-type 'string/starts-with? number? offset)
   (let [offset (scheme/inexact->exact (scheme/floor offset))]
     (if (empty? prefix)
       (<= 0 offset (scheme/string-length s))
       (and (< -1 offset (scheme/string-length s))
            (= offset (string/index-of s prefix offset))))))
  ((s prefix)
   (string/starts-with? s prefix 0))
  (otherwise
   (_raise-arity-error
    'string/starts-with? (_normalize-arity '(2 3)) otherwise))))

(def .startsWith string/starts-with?)


(defn string/ends-with? [s suffix]
  (_assert-type 'string/starts-with? string? s)
  (_assert-type 'string/starts-with? string? suffix)
  (or (empty? suffix)
      (srfi-13/string-suffix? suffix s)))

(def .endsWith string/ends-with?)


(defn string/trim [s]
  (_assert-type 'string/trim string? s)
  (srfi-13/string-trim-both s))

(defn string/triml [s]
  (_assert-type 'string/triml string? s)
  (srfi-13/string-trim s))

(defn string/trimr [s]
  (_assert-type 'string/trimr string? s)
  (srfi-13/string-trim-right s))


(def _char-set:newline
  (srfi-14/char-set #\newline #\return))

(defn string/trim-newline [s]
  (_assert-type 'string/trim-newline string? s)
  (srfi-13/string-trim-right s _char-set:newline))
