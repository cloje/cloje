;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn coll? [coll]
  (or (list? coll)
      (map? coll)
      (set? coll)
      (seq? coll)
      (vector? coll)
      false))


;;; CAPABILITIES

(defn associative? [coll]
  (or (map? coll)
      (vector? coll)
      false))

(defn counted? [coll]
  (or ;; (list? coll) ; Lists are not constant time counted yet.
      (map? coll)
      (set? coll)
      (vector? coll)
      false))

(defn sequential? [coll]
  (or (list? coll)
      (seq? coll)
      (vector? coll)
      false))

(defn sorted? [coll]
  (or
   ; (sorted-map? coll)
   ; (sorted-set? coll)
   false))

(defn reversible? [coll]
  (or ; (sorted-map? coll)
      ; (sorted-set? coll)
      (vector? coll)
      false))


;;; OPERATIONS

(defn _coll-type-name [coll]
  (cond
   (list? coll)      "list"
   (vector? coll)    "vector"
   (_hash-map? coll) "hash-map"
   (string? coll)    "string"
   (set? coll)       "set"
   'else             (_format "(unknown collection type: ~s)" coll)))

(defn _coll-unsupported [fn-name coll]
  (_raise-type-error fn-name "coll?" coll))


(defn assoc [coll key val & more-key-vals]
  (let [key-vals (list* key val more-key-vals)]
   (cond
    (vector? coll)    (_vector-assoc coll key-vals)
    (_hash-map? coll) (_hash-map-assoc coll key-vals)
    (nil? coll)       (_hash-map-assoc (hash-map) key-vals)
    'else             (_coll-unsupported 'assoc coll))))


(defn count [coll]
  (cond
   (nil? coll)         0
   (list? coll)        (scheme/length coll)
   (scheme/pair? coll) (inc (count (scheme/cdr coll)))
   (vector? coll)      (scheme/vector-length coll)
   (_clj-vseq? coll)   (scheme/vector-length (_clj-vseq-v coll))
   (string? coll)      (scheme/string-length coll)
   (_clj-sseq? coll)   (scheme/string-length (_clj-sseq-s coll))
   (_hash-map? coll)   (_hash-map-count coll)
   (_hash-set? coll)   (_hash-set-count coll)
   'else               (_coll-unsupported 'count coll)))


(defn distinct? [x & more]
  (let [xs (cons x more)]
    (= (count xs)
       (count (scheme/apply srfi-1/lset-adjoin = '() xs)))))


(defn empty [coll]
  (cond
   (list? coll)      (list)
   (vector? coll)    (vector)
   (_hash-map? coll) (hash-map)
   (_hash-set? coll) (hash-set)
   (seq? coll)       (list)
   'else             nil))


(defn empty? [coll]
  (cond
   (scheme/pair? coll)  false
   (or (coll? coll)
       (string? coll))  (zero? (count coll))
   (nil? coll)          true
   'else                (_coll-unsupported 'empty? coll)))


(defn every? [pred coll]
  (cond
   (list? coll)     (let [bool-pred (fn [x] (boolean (pred x)))]
                      (srfi-1/every bool-pred coll))
   (seqable? coll)  (every? pred (into '() coll))
   'else            (_coll-unsupported 'every? coll)))


(def get (srfi-16/case-lambda
  ((coll key)
   (get coll key nil))
  ((coll key not-found)
   (cond
    (vector? coll)    (_vector-get coll key not-found)
    (_hash-map? coll) (_hash-map-get coll key not-found)
    (_hash-set? coll) (_hash-set-get coll key not-found)
    (string? coll)    (_string-get coll key not-found)
    'else             not-found))
  (otherwise
   (_raise-arity-error 'get (_normalize-arity '(2 3)) otherwise))))


(defn not-any? [pred coll]
  (every? (fn [x] (not (pred x))) coll))


(defn not-empty [coll]
  (if (empty? coll) nil coll))


(defn not-every? [pred coll]
  (not (every? pred coll)))


(defn some [pred coll]
  (cond
   (empty? coll)     nil
   (seqable? coll)   (if-let [value (pred (first coll))]
                       value
                       (some pred (rest coll)))
   'else             (_coll-unsupported 'some coll)))


(defn contains? [coll key]
  (cond
   (or (vector? coll) (string? coll))
   (and (integer? key) (< -1 key (count coll)))

   (_hash-map? coll)
   (_hash-map-has-key? coll key)

   (set? coll)
   (_hash-set-contains? coll key)

   'else
   (_raise-type-error 'contains?
                      "(or/c vector? string? hash-map? set?)"
                      coll)))


(defn .contains [coll x]
  (cond
   (or (string? coll)
       (sequential? coll))  (not= -1 (.indexOf coll x))
   (set? coll)              (contains? coll x)
   'else
   (_raise-type-error '.contains "string? or sequential? or set?" coll)))
