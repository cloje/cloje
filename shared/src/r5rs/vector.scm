;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def vector scheme/vector)
(def vector? scheme/vector?)
;; TODO (define (vec coll) ...)


(def subvec (srfi-16/case-lambda
  ((vec start)
   (subvec vec start (scheme/vector-length vec)))
  ((vec start end)
   (_assert-bounds-range
    'subvec "vector" vec 0 (scheme/vector-length vec) start end)
   (srfi-43/vector-copy vec start end))
  (otherwise
   (_raise-arity-error 'subvec (_normalize-arity '(2 3)) otherwise))))


(defn _vector-assoc [vec key-vals]
  (let [new-vec (_vector-copy vec 0)]
    (_vector-finalize (_vector-assoc-rec new-vec key-vals))))

(defn _vector-assoc-rec [new-vec key-vals]
  (if (scheme/null? key-vals)
      new-vec
      (let [key (scheme/car key-vals)
            val (scheme/cadr key-vals)
            more-key-vals (scheme/cddr key-vals)]
        (scheme/vector-set! new-vec key val)
        (_vector-assoc-rec new-vec more-key-vals))))


(defn _vector-get [vec key & not-found]
  (if (and (scheme/integer? key)
           (< -1 key (scheme/vector-length vec)))
      (scheme/vector-ref vec key)
      (if (scheme/null? not-found)
          (_raise-bounds-error
           'nth "vector" "" key
           vec 0 (dec (scheme/vector-length vec)) false)
          (scheme/car not-found))))


(defn _vector-index-of [vec x]
  (or (srfi-43/vector-index (fn [y] (= x y)) vec)
      -1))

(defn _vector-last-index-of [vec x]
  (or (srfi-43/vector-index-right (fn [y] (= x y)) vec)
      -1))
