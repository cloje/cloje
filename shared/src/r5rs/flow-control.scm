;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(define-syntax comment
  (syntax-rules ()
    ((comment . anything)
     nil)))


(define-syntax let
  (syntax-rules ()
    ((let [form ...] body ...)
     (_even-form-guard (form ...)
      (_let [form ...] body ...)
      (_raise-syntax-error 'let "must have an even number of forms in binding vector.")))))

(define-syntax _let
  (syntax-rules ()
    ((_let [] body ...)
     (do body ...))
    ((_let [var value more ...] body ...)
     (scheme/let ((var value))
       (_let [more ...]
         body ...)))))


(define-syntax do
  (syntax-rules ()
    ((do)
     nil)
    ((do expr ...)
     (scheme/begin expr ...))))


(define-syntax if
  (syntax-rules ()
    ((if condition true-branch false-branch)
     (scheme/if (host-boolean condition) true-branch false-branch))
    ((if condition true-branch)
     (if condition true-branch nil))
    ((if otherwise ...)
     (_raise-arity-error 'if (_normalize-arity '(2 3)) '(otherwise ...)))))

(define-syntax if-not
  (syntax-rules ()
    ((if-not condition false-branch true-branch)
     (scheme/if (not condition) false-branch true-branch))
    ((if-not condition false-branch)
     (if-not condition false-branch nil))
    ((if-not otherwise ...)
     (_raise-arity-error 'if-not (_normalize-arity '(2 3)) '(otherwise ...)))))


(define-syntax when
  (syntax-rules ()
    ((when condition expr ...)
     (if condition (do expr ...)))
    ((when)
     (_raise-arity-error 'when (_arity-at-least 1) '()))))

(define-syntax when-not
  (syntax-rules ()
    ((when-not condition expr ...)
     (if-not condition (do expr ...)))
    ((when-not)
     (_raise-arity-error 'when-not (_arity-at-least 1) '()))))



(define-syntax cond
  (syntax-rules ()
    ((cond form ...)
     (_even-form-guard (form ...)
      (_cond form ...)
      (_raise-syntax-error 'cond "must have an even number of forms")))))

(define-syntax _cond
  (syntax-rules ()
    ((_cond) nil)
    ((_cond x y more ...)
     (if x y (_cond more ...)))))



(define-syntax condp
  (syntax-rules ()
    ((condp pred expr clause ...)
     (let [var expr]
       (_condp pred var clause ...)))
    ((condp otherwise ...)
     (_raise-arity-error 'condp (_arity-at-least 2) '(otherwise ...)))))

(define-syntax _condp
  (syntax-rules ()
    ((_condp pred var test-expr '#:>> result-fn clause ...)
     (let [var2 (pred var test-expr)]
       (if var2
        (result-fn var2)
        (_condp pred var clause ...))))
    ((_condp pred var test-expr result-expr clause ...)
     (if (pred var test-expr)
         result-expr
         (_condp pred var clause ...)))
    ((_condp pred var default-expr)
     default-expr)
    ((_condp pred var)
     (_raise-generic-error 'condp (_format "No matching clause: ~s" var)))))



(define-syntax case
  (syntax-rules ()
    ((case e clause ...)
     (let [var e]
       (_case var clause ...)))
    ((case)
     (_raise-arity-error 'case (_arity-at-least 1) '()))))

(define-syntax _case
  (syntax-rules ()
    ((_case var test-constant result-expr more ...)
     (if (_case-check var test-constant)
         result-expr
         (_case var more ...)))
    ((_case var default-expr)
     default-expr)
    ((_case var)
     (_raise-generic-error 'case (_format "No matching clause: ~s" var)))))

(define-syntax _case-check
  (syntax-rules ()
    ((_case-check var (test-constant ...))
     (or (= var 'test-constant) ...))
    ((_case-check var test-constant)
     (= var 'test-constant))))



(define-syntax if-let
  (syntax-rules ()
    ((if-let [binding-form test-expr] then)
     (if-let [binding-form test-expr] then nil))
    ((if-let [binding-form test-expr] then else)
     (let [var test-expr]
       (if var
           (let [binding-form var]
             then)
           else)))
    ((if-let [too many binding forms ...] then else ...)
     (_raise-syntax-error 'if-let "requires exactly 2 forms in binding vector"))
    ((if-let otherwise ...)
     (_raise-arity-error 'if (_normalize-arity '(2 3)) '(otherwise ...)))))
