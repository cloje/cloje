;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn _hash-map= [hm1 hm2]
  (and (scheme/= (_hash-map-count hm1)
                 (_hash-map-count hm2))
       (srfi-1/every
        (fn [key]
            (and (_hash-map-has-key? hm2 key)
                 (= (_hash-map-get hm1 key nil)
                    (_hash-map-get hm2 key nil))))
        (_hash-map-keys hm1))))


(defn _hash-map-seq [hm]
  (_hash-map-map hm (fn [key value] (vector key value))))


(defn _hash-map-into [hm kv-vecs]
  (_hash-map-assoc
   hm
   (srfi-1/fold
    (fn [kv-vec accum]
        (if (and (vector? kv-vec) (= 2 (count kv-vec)))
            (srfi-1/cons* (nth kv-vec 0) (nth kv-vec 1) accum)
            (_raise-type-error 'into "vector length 2" kv-vec)))
    '()
    kv-vecs)))
