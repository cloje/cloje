# Contributing to Cloje

There are many ways to help out with Cloje. Some are easy, some are
hard. Some require a lot of experience, some require little. All
contributions are valuable and appreciated.

If you have any questions about contributing to Cloje, please email a
project maintainer:

- John Croisant: john+cloje@croisant.net


## How you can help

Some example of how you can help out:

- Tell your friends about Cloje! :hearts:
- File a
  ["thank-you" issue](https://gitlab.com/cloje/cloje/issues?label_name=thank-you&state=all)
  or send an email to thank the authors for their work. Let us know
  why you appreciate Cloje, or what you have used it for. Hearing from
  you helps fuel our motivation to continue developing Cloje.
- File a
  ["missing-feature" issue](https://gitlab.com/cloje/cloje/issues?label_name=missing-feature)
  for any worthwhile Clojure features that Cloje doesn't have yet.
- File a
  ["discrepancy" issue](https://gitlab.com/cloje/cloje/issues?label_name=discrepancy)
  for any difference in behavior between Clojure and Cloje that is
  not already recorded in
  [docs/discrepancies.md](https://gitlab.com/cloje/cloje/blob/main/docs/discrepancies.md).
- Add a comment on any issues that affect to you, such as missing
  features that you strongly desire, or bugs/discrepancies that
  affects your code.
- Improve test coverage for existing features. See the code in
  [shared/tests/](https://gitlab.com/cloje/cloje/blob/main/shared/tests/)
  for examples of how to add tests. Please focus your effort on
  scenarios that are not already well-covered by existing tests.
- Help implement missing features and fix discrepancies on current
  host languages. Check the
  ["beginner-friendly" issues](https://gitlab.com/cloje/cloje/issues?label_name=beginner-friendly)
  for a good place to start.
- Help [port Cloje to Common Lisp](https://gitlab.com/cloje/cloje/issues/39)
  or add support for more Scheme implementations.

To avoid stepping on toes, please contact the authors (via email or by
creating/commenting on an issue) before you start on a major task.


## Code of Conduct

In order to foster a healthy, safe, and inclusive community and
learning environment, all participants in this project are expected to
abide by the Cloje Contributor Code of Conduct, described in
[CODE_OF_CONDUCT.md](https://gitlab.com/cloje/cloje/blob/main/CODE_OF_CONDUCT.md).


## Clean-room implementation

Cloje is a "clean-room implementation". It has been created without
studying or consulting the source code of Clojure's implementation.
Instead, Cloje is reverse engineered based on:

- Observable run-time behavior of the official Clojure implementation
  (without using the `source` function, of course!)
- The official public documentation of Clojure's API and behavior
  (e.g. docstrings, the website, and the wiki)
- Unofficial documentation and descriptions of Clojure's behavior,
  such as books, presentations, blog posts, and tutorials created by
  members of the community
- High-level descriptions and educational resources (e.g. papers and
  tutorials) about algorithms and data structures that are used by
  Clojure (e.g. hash array mapped tries)

The main reason why Cloje is a clean-room implementation, is that we
have more fun and learn more this way. There is great educational
value and satisfaction in struggling to figure out how something works
by carefully observing it, then finally building your own working
version of it.

The second reason is that carefully observing Clojure's behavior
reveals a lot of useful information about Clojure's gaps, bugs,
inconsistencies, hidden assumptions, and undocumented behavior. We
catch a lot of things that we probably wouldn't notice if we were
simply porting code from one language to another. This information can
be used to make Cloje and Clojure better.

Because Cloje is a clean-room implementation, we **do not accept
source code or patches from anyone who has studied or worked on the
source code of Clojure's implementation** (or derivatives such as
ClojureCLR or ClojureScript). However, exceptions may be made on a
case-by-case basis, for example if it has been a long time since you
studied the Clojure implementation, or if your code contribution is
for a totally different feature than what you studied. If you would
like to contribute code to Cloje, but have studied the Clojure
implementation source code, please contact a Cloje project maintainer.

Other types of contributions, such as documentation, mentorship, or
community outreach, are very welcome regardless of whether or not you
have studied the Clojure implementation source code.
