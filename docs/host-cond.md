# host-cond

Cloje defines the `host-cond` macro, which allows you to execute
different code depending on which Cloje host language is being used.
It exists in order to:

- Allow developers to work around situations where different host
  languages have incompatible behavior (e.g. regexps).
- Aid in the development of portable libraries that can be used on any
  host language.

Please be aware that although you may use host-specific *functions and
macros* within matching clauses of a `host-cond` expression, you must
not use host-specific *syntax* within *any* clause of a `host-cond`
expression. See the
[Limitation: Host-specific Syntax](#limitation-host-specific-syntax)
section, below, for more information and workarounds.

## Basic usage example

```clojure
(host-cond
 (chicken
  (printf "You are using CHICKEN~n"))
 (racket
  (displayln "You are using Racket"))
 (scheme
  (display "You are using a Scheme")
  (newline))
 (else
  (println "You are using some other host language")))
```

Note that only the first matching clause is used. So for example, on
Racket this code will print "You are using Racket.", but will *not*
print "You are using a Scheme.", even though both `racket` and
`scheme` are matching host identifiers for Racket.

## Description

The `host-cond` macro accepts one or more clauses of the form
`(<host requirement> <form> ...)`. It expands to the body form(s) of
the first matching clause. A clause matches if:

* The host requirement is one of the matching host identifier for the
  current host language (see
  [Matching host identifiers](#matching-host-identifiers), below); or
* The host requirement is a logical formula that is true for the
  current host language (see [Logical formulas](#logical-formulas),
  below); or
* The host requirement is the word `else`.

Bodies from clauses other than the first matching clause are
discarded at macro-expansion time.

If no matching clause is found, a syntax error is raised at
macro-expansion time.

(The syntax and semantics of `host-cond` are inspired by `cond-expand`
from [SRFI-0](http://srfi.schemers.org/srfi-0/srfi-0.html).)

## Matching host identifiers

* CHICKEN: `chicken`, `scheme`
* Racket: `racket`, `scheme`

## Logical formulas

Host requirements can be logical formulas using `and` / `or` / `not`.
Nesting is allowed. For example:

```clojure
(host-cond
 ((or chicken racket)
  (println "You are using either CHICKEN or Racket"))
 ((and scheme (not chicken))
  (println "You are using a Scheme other than CHICKEN"))
 ((not scheme)
  (println "You are using a non-Scheme host language"))
 (else
  (println "You are using some other host language")))
```

## Zero-form, single-form, and multiple-form bodies

The body of a `host-cond` clause can have zero or more forms in it:

```clojure
(host-cond
 ;; Zero forms:
 (chicken)
 ;; Single form:
 (racket
  (println "Form one"))
 ;; Multiple forms:
 (else
  (println "Form one")
  (println "Form two")
  (println "...")))
```

* If the body of the matching clause has zero forms, `host-cond`
  expands to `nil`.
* If the body has exactly one form, `host-cond` expands to that form.
* If the body has more than one form, `host-cond` expands to a
  construct equivalent to Clojure's `do` (i.e. it evaluates each form
  in order, and returns the value of the last form).


## Limitation: Host-specific Syntax

Although you may use host-specific *functions and macros* within
matching clauses of a `host-cond` expression, you must not use
host-specific *syntax* within *any* clause of a `host-cond`
expression. Doing so will cause a syntax error on hosts that do not
support that syntax.

This is because `host-cond` is expanded at macro-expansion time, which
occurs *after* the entire expression has been read. This means that
every host language will attempt to read every clause of the
`host-cond` expression before deciding which clause to execute.

For example, CHICKEN has a host-specific syntax for blobs, `#${1234}`,
which Racket cannot (by default) read. Likewise, Racket has a
host-specific syntax for regexps, `#rx"pattern"`, which CHICKEN cannot
(by default) read. With that in mind, consider this code:

```clojure
;;; DO NOT DO THIS
(host-cond
 (chicken
   (blob->string #${636c6f6a65}))
 (racket
   (regexp-match #rx"cloje" "cloje"))
 (else))
```

Even though `#${636c6f6a65}` only occurs within a `chicken` clause,
Racket will throw a syntax error when it encounters the unrecognized
syntax while reading the `host-cond` expression. Likewise, CHICKEN
will throw a syntax error when it encounters the unrecognized
`#rx"cloje"` syntax.

Several workarounds are available for dealing with host-specific
syntax. See the files in the
[demos/host-cond/](https://gitlab.com/cloje/cloje/blob/main/demos/host-cond)
directory for working examples of these workarounds.

### Workaround 1: Avoid host-specific syntax

The best workaround is simply to avoid using host-specific syntax,
when possible. For example, the code above could be rewritten to
change `#${636c6f6a65}` to `(string->blob "cloje")` and `#rx"cloje"`
to `(regexp "cloje")`:

```clojure
(host-cond
 (chicken
   (blob->string (string->blob "cloje")))
 (racket
   (regexp-match (regexp "cloje") "cloje"))
 (else))
```

Note that `blob->string` and `string->blob` are CHICKEN-specific
functions, and `regexp-match` and `regexp` are Racket-specific
functions. But because they are only used within the appropriate host
clauses, this example would not cause an error on any host. No
host-specific syntax is used, so each host is able to read the entire
`host-cond` form. The `host-cond` macro is then expanded, and only the
matching clause's body is executed.

### Workaround 2: Read/eval a string

If it is not possible to rewrite your code to avoid host-specific
syntax, another possible workaround is to put that code in a string,
then `read` and (if necessary) `eval` the string inside the
`host-cond` clause.

```clojure
(host-cond
 (chicken
  (blob->string
   (eval (with-in-str "#${636c6f6a65}" (read)))))
 (racket
  (regexp-match
   (eval (with-in-str "#rx\"cloje\"" (read)))
   "cloje")
 (else))
```

Note that for these two particular syntaxes, `eval` was not strictly
necessary. But, some syntaxes need to be `eval`-ed, so the above
example uses `eval` to show how to do so.

If you find yourself using this workaround a lot, you may want to
define a helper macro to make your code more concise:

```clojure
;;; Helper macro. Usage: (eval-str "foo")
(host-cond
 (scheme
  (define-syntax eval-str
    (syntax-rules ()
      ((eval-str s)
       (eval (with-in-str s (read))))))))
```

Then use the helper macro like so:

```clojure
(host-cond
 (chicken
  (blob->string (eval-str "#${636c6f6a65}")))
 (racket
  (regexp-match (eval-str "#rx\"cloje\"") "cloje"))
 (else))
```

On some hosts (e.g. CHICKEN), code that is `eval`-ed at
run-time is much slower than code that was compiled ahead of time.
Therefore you should try to limit the use of this workaround to only
the smallest necessary parts of your code.


### Workaround 3: Load a file

If none of the above workarounds will suffice, another workaround is
to move the host-specific syntax to a separate file, then use `load`
inside the `host-cond` clause. For example:

```clojure
(host-cond
 (chicken
  (load "chicken-specific-code.scm"))
 (racket
  (load "racket-specific-code.rkt"))
 (else))
```

However, you should consider this workaround a last resort, because:

- Loading code from a file creates additional potential security
  risks, e.g. the file might be surreptitiously modified by other
  users to inject malicious code.

- `load` requires the file to exist at run time, so it would be
  impossible to compile your code to a stand-alone executable or
  library, and your program would fail if the file were deleted.

- On some hosts (e.g. CHICKEN), code that is `load`-ed or `eval`-ed at
  run-time is much slower than code that was compiled ahead of time.

If you are wondering why to use `load` instead of `include`, the
answer is that on some hosts (e.g. CHICKEN), `include` is resolved
*before* macro-expansion time. In other words, CHICKEN would try to
`include` the contents of every file mentioned in every clause, *then*
expand the `host-cond` macro. In contrast, `load` is simply a
function, so the `host-cond` expression is expanded first, then only
the host-appropriate file is `load`-ed.
