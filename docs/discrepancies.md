# Discrepancies

This file records known discrepancies in behavior between Cloje and
Clojure. You can also find new, not-yet-recorded discrepancies and
missing features on the Cloje issue tracker:

- ["discrepancy" issues](https://gitlab.com/cloje/cloje/issues?label_name=discrepancy)
- ["missing-feature" issues](https://gitlab.com/cloje/cloje/issues?label_name=missing-feature)

If you find a discrepancy not recorded in this file, please file a
"discrepancy" issue so that it can be documented and a decision can be
made about what to do. If possible, please mention what host
language(s) you are using, and include a code sample that demonstrates
the expected behavior.



## Intend to Address

### Binding Forms (Destructuring)

Cloje does not yet support any kind of
[binding forms (destructuring)](http://clojure.org/special_forms#Special Forms--Binding Forms (Destructuring))
in `let` binding lists, `fn` parameter lists, or anywhere else.

There are two open "missing-feature" issues related to binding forms:

- [#13 Vector destructuring in let/fn/defn](https://gitlab.com/cloje/cloje/issues/13)
- [#14 Map destructuring in let/fn/defn](https://gitlab.com/cloje/cloje/issues/14)


### Documentation (Docstrings)

Cloje does not yet support docstrings or live documentation.

- `def` and `defn` accept, but silently discard, docstrings.
- `doc` and `find-doc` have not yet been implemented.


### Read Syntax

Cloje does not yet support certain Clojure read syntaxes:

- Chars: `\c`
    - On Scheme, you can use `#\c`, but this is not valid Clojure
      syntax, so your code would not run on Clojure.
    - A portable solution is `(first "c")`, because `first` returns a
      char when given a string. But, this is not very efficient.
    - Another portable solution is `(char 99)`, but this is not very
      readable and requires you know the UTF-8 code for the char.

- Keywords: `:key`
    - On Scheme, you can use `'#:key`, but this is not valid Clojure
      syntax, so your code would not run on Clojure.
    - A portable solution solution is `(keyword "key")`, but
      this is less concise and less efficient.
    - When writing a `cond` statement, a portable solution is to use
      the symbol `'else` (or any other truthy value) instead of the
      keyword `:else`.
    - CHICKEN optionally supports `:key` style keywords by doing
      `(keyword-style #:prefix)`
    - Note that Racket requires keyword literals to be quoted when
      used as values (i.e. `'#:key`, not merely `#:key`). CHICKEN
      allows but does not require you to quote keywords.

- Hash-maps: `{'a 1 'b 2}`
    - A portable solution is `(hash-map 'a 1 'b 2)`

- Sets: `#{1 2 3}`
    - A portable solution is `(hash-set 1 2 3)`

- Regexps: `#"pattern"`
    - A portable solution is `(re-pattern "pattern")`

- Vectors: `[1 2]`
    - A portable solution is `(vector 1 2)`

- Non-base-10 numbers: `2r1011`, `017`, `0xff`, `36rCRAZY`
    - On Scheme, you can use `#b1011` for binary, `#o17` for octal, and
      `#xff` for hex, but these are not valid Clojure syntax, so your
      code would not run on Clojure.
    - On CHICKEN and Racket, you can use `(string->number s radix)`,
      but this is not valid Clojure. CHICKEN supports arbitrarily
      large radixes, e.g. `(string->number "CRAZY" 36)`. Racket
      supports radixes up to 16.

- BigInts and BigDecimals: `7N`, `4.2M`
  - BigInts and BigDecimals are not implemented in Cloje. However,
    both CHICKEN and Racket support very large numbers (without the
    `N` or `M` suffix).

**Note:** Both CHICKEN and Racket by default consider square brackets
`[ ]` and curly braces `{ }` to be equivalent to parentheses `( )` .


### Print Syntax

Cloje does not currently print (write / display) all values in a way
that is consistent with Clojure. E.g. `(vector 1 2 3)` may be printed
as `'#(1 2 3)` or `#(1 2 3)` instead of `[1 2 3]`.

Additionally, some values may be printed differently depending on the
host language. E.g. `(hash-map 'a 1 'b 2)` may be printed on Racket as
`#<immutable-custom-hash>` but on CHICKEN as `#<hash-table (2)>`.

`true` may be printed as `#t` and `false` may be printed as `#f`.

Depending on the host language and settings, keywords might be printed
as `'#:key`, `#:key`, `:key`, or `key:` .

Some host languages omit the single-quote when writing a symbol or
quoted list, while others include it.


### Quoting true, false, and nil

In Clojure, `true`, `false`, and `nil` are immune to quoting (e.g.
`'true` means the same as `true`). Currently in Cloje, quoting `true`,
`false`, and `nil` results in symbols with those names. Note that this
also affects quoted lists containing `true`, `false`, or `nil`.

```clojure
;; Clojure: all true
;; Cloje: all false
(= true  'true  (quote true))
(= false 'false (quote false))
(= nil   'nil   (quote nil))

;; Clojure: all false
;; Cloje: all true
(= (symbol "true")  'true  (quote true))
(= (symbol "false") 'false (quote false))
(= (symbol "nil")   'nil   (quote nil))

;; Clojure: true
;; Cloje: false
(= '(true false nil)
   (list true false nil))

;; Clojure: false
;; Cloje: true
(= '(true false nil)
   (list (symbol "true") (symbol "false") (symbol "nil")))
```


### Sorted maps and sorted sets

Neither sorted maps nor sorted sets have been implemented in Cloje.


### case

Cloje's `case` currently does not support using a vector to match a
list. As a workaround, you can match a list by wrapping the test
constant in another layer of parens, as shown below.

```clojure
;; Clojure: true
;; Cloje: false
(case '(1 2)
  [1 2] true
  false)

;; Both Clojure and Cloje: true
(case '(1 2)
  ((1 2)) true
  false)
```


### def

- `def` defines a global variable, not a var (in the Clojure sense).
- Docstrings are currently ignored.
- attr-maps are not supported (i.e. they will break your code).
- `def` evaluates to an unspecified value (like Scheme's `define`),
  rather than a var.


### defn

- Docstrings are currently ignored.
- attr-maps are not supported (i.e. they will break your code).
- `defn` evaluates to an unspecified value (like Scheme's `define`),
  rather than a var.
- See also the discrepancies of `fn`, described below.


### fn

Cloje's `fn` syntax is currently very limited compared to Clojure.
Specifically, the following features have not yet been implemented:

- overloaded invoke methods (multiple arities)
- binding forms (destructuring)
- pre- and post-conditions
- named fns (names are currently ignored)
- recur


### .indexOf / .lastIndexOf

Cloje's `.indexOf` and `.lastIndexOf` (and their aliases `index-of`
and `last-index-of`) only accept two arguments: the collection to
search in, and the item (or substring) to search for. In Clojure, the
number (and types) of accepted args depends on the class of the first
argument.

For example, in Clojure `.indexOf` and `.lastIndexOf` can be called
with two strings and an integer (`fromIndex`). Cloje provides the
`string/index-of` and `string/last-index-of` functions to address that
specific use case.

In the future, Cloje's `.indexOf` and `.lastIndexOf` may accept a
variable number of arguments.


### let

Cloje's `let` syntax is currently limited compared to Clojure.
Specifically, the following features have not yet been implemented:

- `&` (rest-param)
- binding forms (destructuring)


### map

In Clojure, `map` returns a lazy seq. In Cloje, it currently returns a
normal list.


### re-seq

In Clojure, `re-seq` returns a lazy seq. In Cloje, it currently
returns a normal list.


### test/deftest

- `test/deftest` evaluates to an unspecified value (like Scheme's
  `define`), rather than a var.


### test/run-all-tests

In Clojure, you can pass a regular expression to `run-all-tests` to
select which namespaces to run tests for. Cloje's `run-all-tests`
does not accept a regular expression.



## Might Address

### Data Structure Performance Characteristics

Cloje has not yet implemented persistent immutable data structures
(lists, vectors, maps, sets). So, various data structure operations
will have different performance characteristics in Cloje, as compared
to Clojure. (Generally, that means Cloje will be slower and/or use
more memory when operating on large data structures.)

Related issues:

- [#15 Persistent immutable lists](https://gitlab.com/cloje/cloje/issues/15)
- [#16 Persistent immutable vectors](https://gitlab.com/cloje/cloje/issues/16)
- [#17 Persistent immutable hash-maps](https://gitlab.com/cloje/cloje/issues/17)


### Metadata

Cloje currently has no support for metadata.

- The `^{...}`, `^:foo`, and `^Foo` syntaxes for attaching metadata to
  an object have not been implemented, and attempting to use them
  **will break your code**.
- The `meta`, `with-meta`, etc. function have not been implemented.


### Namespaces

Cloje currently has no support for namespaces.

Features from various Clojure standard library namespaces are
currently defined in the main cloje module, but with a prefix:

- Features from clojure.set are prefixed with "set/". E.g.
  `clojure.set/union` is defined as `set/union`.
- Features from clojure.string are prefixed with "string/". E.g.
  `clojure.string/replace` is defined as `string/replace`.
- Features from clojure.test prefixed with "test/". E.g.
  `clojure.test/deftest` is defined as `test/deftest`.


### Regular expressions

Cloje relies on the host language's implementation of regular
expressions. Each host language has some differences in regexp syntax
and behavior. So, a regexp that works on Clojure may not work the same
way in Cloje, and a regexp that works on one host language may not
work the same way on another host language.

There are two open issues related to this discrepancy:

- [#19: Regular expressions compatibility guide](https://gitlab.com/cloje/cloje/issues/19)
- [#20: Abstract regular expression type](https://gitlab.com/cloje/cloje/issues/20)

As a workaround, you can use `host-cond` to vary the regexp definition
depending on the host language:

```clojure
(def my-regexp
  (host-cond
   (chicken (re-pattern "foo"))
   (racket  (re-pattern "bar"))
   (else    (re-pattern "baz"))))
```

For more information about `host-cond`, see
[docs/host-cond.md](https://gitlab.com/cloje/cloje/blob/main/docs/host-cond.md).


### case

In Clojure, `case` performs a constant-time lookup. For ease of
implementation, Cloje's `case` currently performs sequential checks
with `=`, which is less efficient, especially for large numbers of
checks.


### counted?

The Clojure documentation for `counted?` says: "Returns true if coll
implements count in constant time."

In Clojure, lists implement `count` in constant time, so Clojure's
`counted?` returns true for lists. In Cloje, lists implement `count`
in linear time, not constant time, so Cloje's `counted?` returns false
for lists. This may change if persistent immutable lists are added to
Cloje.

```clojure
;; Clojure: true
;; Cloje: false
(counted? (list 1 2 3))
```


### identical?

The Clojure documentation for `identical?` says: "Tests if 2 arguments
are the same object."

Unfortunately, there are situations where things are considered "the
same object" in Clojure but not "the same object" in Scheme/Lisp, or
vice versa, due to different implementation details.

#### String literals

In Clojure, string literals are interned, so two string literals with
the same contents are considered identical. In Cloje, this may or may
not be true, depending on how the host language handles string
literals.

Note that this only applies to string literals. Strings that are
created on-the-fly are considered not-identical in both Clojure or
Cloje.

```clojure
;; Clojure: true
;; Cloje: depends on host language
(identical? "a" "a")

;; Both Clojure and Cloje: true
(let [x "a"]
  (identical? x x))

;; Both Clojure and Cloje: false
(identical? (str "a" "b") (str "a" "b"))
```

#### Symbols

In Clojure, symbols are not interned, so two symbols with the same
name are not necessarily considered identical. In Cloje, symbols are
interned, so two symbols with the same name are considered identical.

Note: Some host languages support uninterned symbols. The behavior of
`identical?` on uninterned symbols is unspecified in Cloje.

Apparently, the reason Clojure symbols are not interned is because
they can accept metadata. This discrepancy may go away if metadata
support is added to Cloje.

```clojure
;; Clojure: false
;; Cloje: true
(identical? 'a 'a)

;; Both Clojure and Cloje: true
(let [x 'a]
  (identical? x x))
```



## Do Not Intend to Address

### set/difference

In Clojure, `clojure.set/difference` has undocumented behavior when
given collections that are not sets. In practice, it throws an error
if the first argument is not a set, but it allows the remaining
arguments to be any seqable collection, sequence, or nil. Also, if
`clojure.set/difference` is called with a single argument, it returns
that argument unchanged, even if that argument is not a set.

In Cloje, `set/difference` is documented to accept arguments of any
seqable collection type (list, vector, map, set, string), sequence
type, or nil. It returns a hash set, regardless of the types or number
of arguments given.

`clojure.set/difference` and `set/difference` have compatible behavior
when the first argument is a set, so this discrepancy can be worked
around by explicitly converting the first argument to a set.

```clojure
;; Clojure: throws ClassCastException
;; Cloje: true
(= (set/difference (list 1 2 3 4 5) (list 1 3) (vector 3 5))
   (hash-set 2 4))

;; Both Clojure and Cloje: true
(= (set/difference (set (list 1 2 3 4 5)) (list 1 3) (vector 3 5))
   (hash-set 2 4))
```

### set/intersection

In Clojure, `clojure.set/intersection` has undocumented behavior when
given collections that are not sets. In practice, it throws an error
if any argument is not a set. But, if `clojure.set/intersection` is
called with a single argument, it returns that argument unchanged,
even if that argument is not a set.

In Cloje, `set/intersection` is documented to accept arguments of any
seqable collection type (list, vector, map, set, string), sequence
type, or nil. It returns a hash set, regardless of the types or number
of arguments given.

`clojure.set/intersection` and `set/intersection` have compatible
behavior when given sets, so this discrepancy can be worked around by
explicitly converting all collections to sets.

```clojure
(def my-colls (list (vector 1 2 3 4 5) (hash-set 1 3 5) (list 0 1 2 3)))

;; Clojure: throws IllegalArgumentException
;; Cloje: true
(= (apply set/intersection my-colls)
   (hash-set 1 3))

;; Both Clojure and Cloje: true
(= (apply set/intersection (map set my-colls))
   (hash-set 1 3))
```

### set/select

In Clojure, the behavior of `clojure.set/select` is undocumented when
given an argument other than a set. In practice, it throws an error if
given anything other than a set. But, if given a seqable collection,
sequence, or nil, and if the test function returns true for every
element, `clojure.set/select` returns the collection/sequence/nil
unchanged, even if it is not a set.

In Cloje, `set/select` is documented to accept arguments of any
seqable collection type (list, vector, map, set, string), sequence
type, or nil. It returns a hash set, regardless of the type of
argument given.

`clojure.set/select` and `set/select` have compatible behavior when
given a set, so this discrepancy can be worked around by explicitly
converting the argument to a set.

```clojure
;; Clojure: (list 2 4)
;; Cloje: (hash-set 2 4)
(set/select even? (list 2 4))

;; Clojure: throws ClassCastException
;; Cloje: (hash-set 2 4)
(set/select even? (list 1 2 3 4))

;; Both Clojure and Cloje: true
(= (set/select even? (set (list 1 2 3 4)))
   (hash-set 2 4))
```

### set/subset? and set/superset?

In Clojure, `clojure.set/subset?` and `clojure.set/superset?` have
unexpected and undocumented behavior when given collections that are
not sets. In practice, `clojure.set/subset?` will work as long as the
first argument is a seqable collection, sequence, or nil, and the
second argument is a set. But if the second argument is not a set,
`clojure.set/subset?` will either throw an error or return an
unexpected result, depending on whether the type of the second
argument is supported by the `contains?` function.
`clojure.set/superset?` has the same behaviors, but reversed order of
arguments.

In Cloje, `set/subset?` and `set/superset?` are documented to accept
arguments of any seqable collection type (list, vector, map, set,
string), sequence type, or nil.

`clojure.set/subset?` and `set/subset?` have compatible behavior when
given sets, so this discrepancy can be worked around by explicitly
converting all arguments to sets. Likewise for `clojure.set/superset?`
and `set/superset?`.

```
;; Clojure: throws IllegalArgumentException
;; Cloje: true
(set/subset? (list 3 4) (list 3 4 5))

;; Both Clojure and Cloje: true
(set/subset? (set (list 3 4)) (set (list 3 4 5)))

;; Clojure: true (unexpected!)
;; Cloje: false
(set/subset? (list 0 1) (vector 3 4 5))

;; Both Clojure and Cloje: false
(set/subset? (set (list 0 1)) (set (vector 3 4 5)))
```

### set/union

In Clojure, `clojure.set/union` has unexpected and undocumented
behavior when given collections that are not sets. It may return a
collection that is not a set, and the collection may contain repeated
elements.

In Cloje, `set/union` is documented to accept arguments of any seqable
collection type (list, vector, map, set, string), sequence type, or
nil. It returns a hash set, regardless of the types of arguments
given.

If there are repeated elements (i.e. multiple elements that are the
same according to the `hash` and `=` functions) in the given
collections, exactly one of the repeated elements will appear in the
returned set. But, there is no guarantee of which of the repeated
elements will be used. This is deliberately left ambiguous to allow
for optimization, and for compatibility with Clojure.

As with `clojure.set/union` in Clojure, `set/union` in Cloje will
throw an error if any argument is a type other than a seqable
collection, a sequence, or nil.

`clojure.set/union` and `set/union` have compatible behavior when
given sets, so this discrepancy can be worked around by explicitly
converting all collections to sets.

```clojure
(def my-colls (list (hash-set 3 4) (vector 1 2 3 4) (list 1 2 3)))

;; Clojure: true
;; Cloje: false
(= (apply set/union my-colls)
   (vector 1 2 3 4 4 3 1 2 3))

;; Clojure: false
;; Cloje: true
(= (apply set/union my-colls)
   (hash-set 1 2 3 4))

;; Both Clojure and Cloje: true
(= (apply set/union (map set my-colls))
   (hash-set 1 2 3 4))
```
