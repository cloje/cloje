# Cloje Frequently Asked Questions

## How can I stop Cloje from replacing case/cond/do/let/etc.?

Clojure (and thus Cloje) has some special forms with the same name as
special forms in Scheme/Lisp. If you import Cloje as a whole, the host
language's special forms will be replaced (shadowed) in the current
module/context by Cloje's forms.

If you still want to be able to use the host language's special forms,
one easy way to prevent them from being replaced is to import the
Cloje forms with a prefix:

* In CHICKEN, instead of `(use cloje)`, do this:

    ```clojure
    (require-library cloje)
    (import (except cloje case cond do let))
    (import (prefix (only cloje case cond do let) cloje/))
    ```

* In Racket, instead of `(require cloje)`, do this:

    ```clojure
    (require (except-in cloje case cond do let))
    (require (prefix-in cloje/ (only-in cloje case cond do let)))
    ```

You can then use the Cloje forms as `cloje/case`, `cloje/cond`, etc.,
and use the host versions as their usual names:

```clojure
;;; Cloje form
(cloje/case x
  1 "one"
  2 "two"
  "other")

;;; Host form
(case x
  ((1) "one")
  ((2) "two")
  (else "other"))
```
