# Cloje Project Goals

This document describes Cloje's goals, philosophy, and development
approach.


## The Software Goal

The primary goal of the Cloje software is to bring the best aspects of
Clojure to Scheme/Lisp.

Cloje will be a layer of data structures, functions, macros, and
reader syntax on top of Scheme/Lisp. Importing all of Cloje will allow
you to write code in a language very similar to Clojure. Or if you
prefer, you will be able to pick and choose which pieces of Cloje to
import, so that you are still mostly writing code in Scheme/Lisp, but
with some added features from Clojure.

Cloje does not aim to be a 100% complete, faithful clone of Clojure.
The reasons for that are described in "Why does Cloje differ from
Clojure?", later in this document.


### Rationale

Clojure has many desirable features, such as those listed in the next
section. However, Clojure's dependency on the JVM makes it poorly
suited for a number of scenarios, such as:

- Utility programs that need to start up quickly
- Low-storage, low-memory systems and devices
- Systems and devices where the JVM is not available

Furthermore, there are many developers who cannot or do not wish to
depend on the JVM, for a variety of reasons.

Scheme and Lisp have a wide variety of implementations on a variety of
platforms, each implementation having its own special strengths. For
example, some implementations can compile to fast native code, or run
in low-memory environments with little overhead, or be easily embedded
in other programs as a scripting language.

Also, there are many Scheme/Lisp programmers who are generally
satisfied with their current language, or who have substantial
existing codebases written in it, but who wish to augment it with a
few useful features from Clojure.

By bringing the best aspects of Clojure to Scheme/Lisp, we can have
the desirable features of Clojure, while leveraging the portability
and individual strengths of the many available Scheme/Lisp
implementations.

Plus, it'll be fun and we'll learn a lot!


### What are the best aspects of Clojure?

The goal is intentionally vague about what "the best aspects of
Clojure" are. At the end of the day, "the best aspects" are whatever
we (users and contributors to Cloje) consider valuable enough to
implement.

Here are some examples of things that might be considered to be among
"the best aspects of Clojure", worth bringing to Scheme/Lisp:

- Concurrency support
- Efficient purely functional data structures
- Generic collection/sequence operations
- Reader syntax for data structures like maps and sets
- Various convenience macros
- Multiple arity functions
- Destructuring binding
- Lazy sequences
- Transducers
- Multimethods
- core.async
- core.logic

Many people would consider Java interop to be one of the best aspects
of Clojure. However, most Scheme/Lisp implementations are not based on
Java, so Java interop is unlikely to be implemented in Cloje.


### Why does Cloje differ from Clojure?

Cloje was originally intended to be as faithful and complete a clone
of Clojure as was possible. However, Clojure has some issues that make
it very difficult to clone faithfully:

- There is no formal specification
- The official documentation is ambiguous and incomplete
- There are many quirks and undocumented behaviors
- There are many gaps that it relies on Java interop to fill

These issues are explained in more detail in the
[Cloje 0.1 Post-Mortem](http://blog.jacius.info/2015/05/09/cloje-0-1-post-mortem/).
Because of these issues, Cloje will no longer attempt to emulate
Clojure faithfully.

Cloje will still strive to be API compatible with Clojure, within
reason. Common, valid (non-error) cases should have compatible
behavior. But we may fix bugs, quirks, internal inconsistencies, and
undocumented behaviors, rather than attempt to recreate them. We will
fill in Clojure's gaps with Clojure-style functions, rather than
imitating Java.

Differences between Clojure and Cloje are documented in
[docs/discrepancies.md](https://gitlab.com/cloje/cloje/blob/main/docs/discrepancies.md),
with workarounds when available. Cloje has some functions and macros
that do not exist in Clojure. They are documented in
[docs/additions.md](https://gitlab.com/cloje/cloje/blob/main/docs/additions.md)
