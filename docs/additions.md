# Additions

This file records extra functionality in Cloje compared to Clojure.


## Aliases

Cloje defines some aliases. In each case, both the alias and the
original name are available and exactly equivalent.

- `index-of` is an alias for `.indexOf`
  (see also `string/index-of`, below).
- `last-index-of` is an alias for `.lastIndexOf`
  (see also `string/last-index-of`, below).
- `string/starts-with?` is an alias for `.startsWith`
- `string/ends-with?` is an alias for `.endsWith`

Note: `contains?` is **not** an alias for `.contains`. They are
different functions with different behavior. `contains?` comes from
Clojure and `.contains` comes from Java.


## cloje-version and \*cloje-version\*

```clojure
(cloje-version)  ; => string
*cloje-version*  ; => hash-map
```

Cloje defines `cloje-version` and `*cloje-version*`, which express the
version number of Cloje being used. They are analogous to Clojure's
`clojure-version` and `*clojure-version*`, but describe Cloje's
version number, which has no relation to Clojure's version number.

`*cloje-version*` is a hash-map which contains the keys `:major`,
`:minor`, `:incremental`, and `:qualifier`. The values of `:major`,
`:minor`, and `:incremental` are always non-negative integers. The
value of `:qualifier` may either be nil or a string (e.g. `"BETA-1"`
or `"RC-4"`).

`cloje-version` is a function which returns the version number as a
string, such as `"0.1.0"` or `"1.2.3-RC-4"`.


## re-matcher?

```clojure
(re-matcher? x)
```

`re-matcher?` returns `true` if the given object is a regular
expression matcher (such as returned by `re-matcher`). Returns `false`
otherwise.


## re-pattern?

```clojure
(re-pattern? x)
```

`re-pattern?` returns `true` if the given object is a regular
expression pattern. Returns `false` otherwise.


## host-boolean

```clojure
(host-boolean x)  ; => host logical true or logical not-true
```

`host-boolean` implements Cloje's rules for logical true/not-true, but returns a value that is logical true/not-true *in the current host language*. In other words, `host-boolean` converts any value into a boolean suitable for use in host language code.

Specifically:

- If `host-boolean` is given `false` (as defined in Cloje), `nil` (as defined in Cloje), or any value that is logical not-true in the host language, it returns a value that is logical not-true in the host language.
- If `host-boolean` is given any other value, it returns a value that is logical true in the host language.

"Logical true in the current host lanugage" means the value would cause an `if` expression in the current host language to execute the "condition was true" branch. "Logical not-true in the current host language" means the value would cause an `if` expression in the host language to execute the "condition was not true" branch.

For example, on a Scheme host, `host-boolean` returns `#t` or `#f`. On a Common Lisp host, `host-boolean` would return `T` or `NIL`. Contrast with the `boolean` function, which returns `true` or `false` (as defined in Cloje), regardless of the host language.

You should use `host-boolean` if you are writing code *in the host language* and you wish to use precisely the same concept of logical true/not-true as Cloje. If you are using the functions and macros provided by Cloje (`if`, `and`, `or`, `not`, `every?`, etc.), you do not need to use `host-boolean`.


## host-cond

Cloje defines the `host-cond` macro to facilitate executing
different code depending on the Cloje host language. See
[docs/host-cond.md](https://gitlab.com/cloje/cloje/blob/main/docs/host-cond.md)
for more details.


## seqable?

```clojure
(seqable? x)
```

`seqable?` returns `true` if the given object can be coerced to a
sequence by passing it to `seq`, or if it is already a sequence.
Returns `false` otherwise.


## string/index-of

```clojure
(string/index-of s1 s2)
(string/index-of s1 s2 from-index)
```

`string/index-of` returns the index of the **first** occurrence of
substring `s2` within string `s1`. Returns `-1` if no occurrence is
found. It is analogous to `String.indexOf(String, int)` from Java.
Contrast with `string/last-index-of`, described below.

This function differs from the general-purpose `index-of` (aka
`.indexOf`) function: this function only accepts strings, and it
optionally accepts a third argument. `index-of` and `string/index-of`
behave exactly the same if called with two strings.

The optional third argument `from-index` specifies the index to start
searching from. In other words, any occurrences that begin *before*
`from-index` will be ignored. It defaults to 0 (zero).

Examples:

```clojure
(string/index-of "bananas" "na")   ; => 2
(string/index-of "bananas" "na" 0) ; equivalent to above
(string/index-of "bananas" "na" 3) ; => 4
(string/index-of "bananas" "")     ; => 0
```


## string/last-index-of

```clojure
(string/last-index-of s1 s2)
(string/last-index-of s1 s2 from-index)
```

`string/index-of` returns the index of the **last** occurrence of
substring `s2` within string `s1`. Returns `-1` if no occurrence is
found. It is analogous to `String.lastIndexOf(String, int)` from Java.
Contrast with `string/index-of`, described above.

This function differs from the general-purpose `last-index-of` (aka
`.lastIndexOf`) function: this function only accepts strings, and it
optionally accepts a third argument. `last-index-of` and
`string/last-index-of` behave exactly the same if called with two
strings.

The optional third argument `from-index` specifies the index to start
searching (backwards) from. In other words, any occurrences that begin
*after* `from-index` will be ignored. It defaults to the length of
`s1`.

Examples:

```clojure
(string/last-index-of "bananas" "na")   ; => 4
(string/last-index-of "bananas" "na" 7) ; equivalent to above
(string/last-index-of "bananas" "na" 3) ; => 2
(string/last-index-of "bananas" "")     ; => 7
```
