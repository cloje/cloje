;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def map? srfi-69/hash-table?)


;;; Adapt 'hash' to meet CHICKEN's expectations for a hash function.
(defn _hash [x bound]
  (scheme/modulo (hash x) bound))


(defn hash-map [& args]
  (srfi-69/alist->hash-table
   (_plist->alist args '())
   #:test =
   #:hash _hash))


(def _hash-map?         srfi-69/hash-table?)
(def _hash-map-get      srfi-69/hash-table-ref/default)
(def _hash-map-count    srfi-69/hash-table-size)
(def _hash-map-keys     srfi-69/hash-table-keys)
(def _hash-map-has-key? srfi-69/hash-table-exists?)
(def _hash-map-map      srfi-69/hash-table-map)


(defn _hash-map-assoc [hm key-vals]
  (let [new-hm (srfi-69/hash-table-copy hm)]
    (_hash-map-assoc-rec new-hm key-vals)))

(defn _hash-map-assoc-rec [new-hm key-vals]
  (if (scheme/null? key-vals)
      new-hm
      (let [key (scheme/car key-vals)
            val (scheme/cadr key-vals)
            more-key-vals (scheme/cddr key-vals)]
        (srfi-69/hash-table-set! new-hm key val)
        (_hash-map-assoc-rec new-hm more-key-vals))))
