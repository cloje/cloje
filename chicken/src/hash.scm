;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(def _eq-hash      srfi-69/eq?-hash)
(def _eqv-hash     srfi-69/eqv?-hash)
(def _equal-hash   srfi-69/equal?-hash)
(def _string-hash  srfi-13/string-hash)
