;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(host/define-record-type :clj-vseq
  (_clj-vseq v i)
  _clj-vseq?
  (v _clj-vseq-v)
  (i _clj-vseq-i))

(host/define-record-type :clj-sseq
  (_clj-sseq s i)
  _clj-sseq?
  (s _clj-sseq-s)
  (i _clj-sseq-i))
