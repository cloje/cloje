;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(define-syntax host-cond
  (syntax-rules (chicken scheme and or not else)
    ;; BEGIN CHICKEN-SPECIFIC STUFF
    ((host-cond (chicken form) more ...)
     form)
    ((host-cond (scheme form) more ...)
     form)
    ;; END CHICKEN-SPECIFIC STUFF

    ((host-cond (req) more ...)
     (host-cond (req nil) more ...))

    ((host-cond (req body1 body2 more-body ...) more ...)
     (host-cond (req (scheme/begin body1 body2 more-body ...)) more ...))

    ((host-cond ((not req) form) more ...)
     (host-cond (req (host-cond more ...)) (else form)))

    ((host-cond ((or) form) more ...)
     (host-cond more ...))
    ((host-cond ((or req more-reqs ...) form) more ...)
     (host-cond (req form) ((or more-reqs ...) form) more ...))

    ((host-cond ((and) form) more ...)
     form)
    ((host-cond ((and req more-reqs ...) form) more ...)
     (host-cond ((not req) (host-cond more ...))
                ((and more-reqs ...) form)
                more ...))

    ((host-cond (else form) more ...)
     form)
    ((host-cond (otherwise ...) more ...)
     (host-cond more ...))
    ((host-cond)
     (_raise-syntax-error
      'host-cond "No matching host-cond clause"))))
