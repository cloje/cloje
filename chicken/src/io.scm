;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


;;; These definitions are identical on both CHICKEN and Racket, but
;;; they are not R5RS, so they don't belong in shared/src/r5rs/io.scm.


(define-syntax with-in-str
  (syntax-rules ()
    ((with-in-str s body ...)
     (host/with-input-from-string
      s (scheme/lambda () nil body ...)))
    ((with-in-str otherwise ...)
     (_raise-arity-error
      'with-in-str (_arity-at-least 2) '(otherwise ...)))))


(define-syntax with-out-str
  (syntax-rules ()
    ((with-out-str body ...)
     (host/with-output-to-string
      (scheme/lambda () nil body ...)))))
