;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn _vector-copy [vec grow]
  (scheme/let* ((len     (+ (scheme/vector-length vec) grow))
                (new-vec (scheme/make-vector len)))
    (host/vector-copy! vec new-vec)
    new-vec))

(defn _vector-finalize [vec]
  vec)
