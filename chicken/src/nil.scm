;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(host/define-record-type nil (_make-nil) nil?)

(def nil (_make-nil))

(host/define-record-printer (nil x port)
  (scheme/display "nil" port))
