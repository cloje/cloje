;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(define-syntax _raise-syntax-error
  (syntax-rules ()
    ((_raise-syntax-error fn-name msg)
     (host/abort
      (host/make-composite-condition
       (host/make-property-condition
        'exn 'message
        (_format "(~a) Syntax error: ~a" fn-name msg))
       (host/make-property-condition 'syntax))))))


(define-syntax _arity-at-least
  (syntax-rules ()
    ((_arity-at-least n)
     (_format "at least ~a" n))))

(define-syntax _normalize-arity
  (syntax-rules ()
    ((_normalize-arity arity)
     (_format "arity one of: ~a" arity))))

(define-syntax _raise-arity-error
  (syntax-rules ()
    ((_raise-arity-error fn-name expectation actual)
     (host/abort
      (host/make-composite-condition
       (host/make-property-condition
        'exn 'message
        (_format "(~a) Wrong number of arguments:~n  Expected: ~a~n  Got: ~s"
                 fn-name expectation (scheme/length actual)))
       (host/make-property-condition 'arity))))))


(define-syntax _raise-type-error
  (syntax-rules ()
    ((_raise-type-error fn-name expectation actual)
     (host/abort
      (host/make-composite-condition
       (host/make-property-condition
        'exn 'message
        (_format "(~a) Expected type: ~a~n  Got: ~s"
                 fn-name expectation actual))
       (host/make-property-condition 'type))))))


;;; Intended to emulate Racket's raise-range-error.
(define-syntax _raise-bounds-error
  (syntax-rules ()
    ((_raise-bounds-error fn-name type-desc index-prefix index
                          coll lower upper alt-lower)
     (host/abort
      (host/make-composite-condition
       (host/make-property-condition
        'exn 'message
        (scheme/if alt-lower
            (scheme/if (scheme/< (scheme/- alt-lower 1) index upper)
                (_format "(~a) ~aindex is smaller than starting index:~n  ~aindex: ~s~n  starting index: ~s~n  valid range: [~a, ~a]~n  ~a: ~s"
                         fn-name index-prefix index-prefix index lower alt-lower upper type-desc coll)
                (_format "(~a) ~aindex is out of bounds:~n  ~aindex: ~s~n  starting index: ~s~n  valid range: [~a, ~a]~n  ~a: ~s"
                         fn-name index-prefix index-prefix index lower lower upper type-desc coll))
            (_format "(~a) ~aindex is out of bounds:~n  ~aindex: ~s~n  valid range: [~a, ~a]~n  ~a: ~s"
                     fn-name index-prefix index-prefix index lower upper type-desc coll)))
       (host/make-property-condition 'bounds))))))


(define-syntax _raise-generic-error
  (syntax-rules ()
    ((_raise-generic-error fn-name msg)
     (host/abort
      (host/make-property-condition
       'exn 'message
       (_format "(~a) ~a" fn-name msg))))))


(define-syntax _catch-errors
  (syntax-rules ()
    ((_catch-errors ((err-var) error-body ...) body ...)
     (host/condition-case
      (scheme/begin body ...)
      (err-var () error-body ...)))))
