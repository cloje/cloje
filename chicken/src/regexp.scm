;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(defn re-pattern [s]
  (if (string? s)
      (host/string->irregex s)
      (_raise-type-error 're-pattern "string?" s)))


(def re-pattern? host/irregex?)


(host/define-record-type re-matcher
  (_make-re-matcher pattern input start last-match)
  re-matcher?
  (pattern _re-matcher-pattern)
  (input _re-matcher-input)
  (start _re-matcher-start set-_re-matcher-start!)
  (last-match _re-matcher-last-match
              set-_re-matcher-last-match!))


(defn _re-match-ranges [match]
  (if match
    (map (fn [i] (scheme/cons
                  (host/irregex-match-start-index match i)
                  (host/irregex-match-end-index match i)))
         (srfi-1/iota (inc (_re-match-num-groups match))))
    nil))

(defn _re-match-num-groups [match]
  (host/irregex-match-num-submatches match))

(defn _re-matcher-next-start [matcher last-match]
  (host/irregex-match-end-index last-match))


(defn _re-find [pattern input start]
  (host/irregex-search pattern input start))


(defn _re-groups [last-match]
  (scheme/list->vector
   (map (fn [i] (or (host/irregex-match-substring last-match i)
                    nil))
        (srfi-1/iota (inc (_re-match-num-groups last-match))))))


(defn _re-matches [pattern input]
  (host/irregex-match pattern input))
