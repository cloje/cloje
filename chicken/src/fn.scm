;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


;; ;;; TODO:
;; ;;; - & rest
;; ;;; - :as all
;; ;;; - vector binding forms
;; ;;; - map binding forms
;; ;;; - pre- and post-exprs


;; (export fn)

;; (define-syntax fn
;;   (syntax-rules ()
;;     ((fn)
;;      (syntax-error "Invalid fn form"))

;;     ;; Multiple-arity fn
;;     ((fn ([param ...] expr ...) ...)
;;      (_case-lambda (_fn-arity (param ...) expr ...) ...))

;;     ;; Single-arity fn with no body (returns nil)
;;     ((fn [param ...])
;;      (lambda (param ...) nil))

;;     ;; Single-arity fn with body
;;     ((fn [param ...] expr ...)
;;      (lambda (param ...) expr ...))

;;     ;; Named single-arity fn (name is discarded)
;;     ((fn name [param ...] expr ...)
;;      (fn [param ...] expr ...))

;;     ;; Named multiple-arity fn (name is discarded)
;;     ((fn name ([param ...] expr ...) ...)
;;      (fn ([param ...] expr ...) ...))

;;     ((fn otherwise)
;;      (syntax-error "Invalid fn form"))))



;; ;;; Takes a form like
;; ;;;   (_case-lambda (_fn-arity ()) (_fn-arity (x) x))
;; ;;; And expands the _fn-arity macros, which would result in
;; ;;;   (_case-lambda (() nil) ((x) x))
;; ;;; Finally, turns that into
;; ;;;   (case-lambda (() nil) ((x) x))
;; (define-syntax _case-lambda
;;   (ir-macro-transformer
;;    (lambda (exp inject compare)
;;      (let ((clauses (cdr exp)))
;;        (cons 'case-lambda (map expand clauses))))))

;; (define-syntax _fn-arity
;;   (syntax-rules ()
;;     ;; No body (returns nil)
;;     ((_ [param ...])
;;      ((param ...) nil))
;;     ;; Normal mode
;;     ((_ [param ...] expr ...)
;;      ((param ...) expr ...))))
