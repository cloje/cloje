;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


;;; A hash set is implemented as a wrapper around a hash table, where
;;; the key and value for each entry is the member of the set.
;;; (Storing the member as both the key and the value allows us to
;;; easily return the correct object when performing a 'get'.)


(host/define-record-type hash-set
  (_make-hash-set table)
  _hash-set?
  (table _hash-set-table))

(host/define-record-printer (hash-set hs port)
  (_hash-set-print hs port))
