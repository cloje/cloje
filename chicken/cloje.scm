;;;
;;; This file is part of Cloje:
;;; A clone of Clojure built atop Scheme/Lisp.
;;;
;;; Copyright © 2015  John Croisant
;;;
;;; Cloje is free software offered under the terms of the
;;; Eclipse Public License (v 1.0). See LICENSE.txt for details.


(module cloje ()

(import
 (prefix (except scheme
                 ;; These are re-defined in numbers. Importing them
                 ;; twice causes a bunch of annoying warnings in csi.
                 +  -  *  /  =  >  <  >=  <=  eqv?  number->string
                 string->number  exp  log  sin  cos  tan  atan  acos
                 asin  expt  sqrt  quotient  modulo  remainder
                 numerator  denominator  abs  max  min  gcd  lcm
                 positive?  negative?  odd?  even?  zero?  exact?
                 inexact?  floor  ceiling  truncate  round
                 inexact->exact  exact->inexact  number?  complex?
                 real?  rational?  integer?  real-part  imag-part
                 magnitude
                 ;; These are re-defined in utf8
                 string-length  string-ref  string-set!  make-string
                 string  substring  string->list  list->string
                 string-fill!  write-char  read-char  display)
         scheme/)
 (prefix (except chicken
                 ;; These are re-defined in utf8
                 reverse-list->string  print  print*
                 ->string  conc  string-chop  string-split
                 string-translate  substring=?  substring-ci=?
                 substring-index  substring-index-ci
                 read-string  write-string  read-token)
         host/)
 (only scheme
       define  define-syntax  syntax-rules
       import  export
       quote  require-library)
 (only chicken
       include)
 (rename (only chicken case-lambda)
         (case-lambda srfi-16/case-lambda))
 (rename (only extras format)
         (format _format))
 (prefix ports host/)
 (prefix srfi-1 srfi-1/)
 (prefix (only chicken make-parameter parameterize)
         srfi-39/))


(require-library utf8)
(import (prefix
         (only utf8
               string-length  string-ref  string-set!  make-string
               string  substring  string->list  list->string
               string-fill!  write-char  read-char  display)
         scheme/))
(import (prefix
         (only utf8
               reverse-list->string  print  print*
               ->string  conc  string-chop  string-split
               string-translate  substring=?  substring-ci=?
               substring-index  substring-index-ci
               read-string  write-string  read-token)
         host/))


(require-library numbers)
(import (prefix numbers scheme/))

(require-library utf8-srfi-13)
(import (prefix utf8-srfi-13 srfi-13/))

(require-library utf8-srfi-14)
(import (prefix utf8-srfi-14 srfi-14/))

(require-library srfi-69)
(import (prefix srfi-69 srfi-69/))
;;; In CHICKEN, 'string-hash' and 'string-ci-hash' exist in srfi-69
;;; but not srfi-13.
(import (prefix (only srfi-69 string-hash string-ci-hash)
                srfi-13/))

(require-library vector-lib)
(import (prefix vector-lib srfi-43/))

(require-library irregex)
(import (prefix irregex host/))

(require-library srfi-27)
(import (prefix srfi-27 srfi-27/))


;; _raise-syntax-error  _raise-type-error  _raise-generic-error
;; _raise-arity-error  _arity-at-least  _normalize-arity
;; _catch-errors
(include "src/_error-helpers.scm")

;; _even-form-guard  _plist->alist
;; _assert-bounds-range  _assert-type
(include "../shared/src/r5rs/_helpers.scm")


(export host-cond)
(include "src/host-cond.scm")


(export def)
(include "../shared/src/r5rs/vars.scm")


(export defn fn fn?)
(include "../shared/src/r5rs/fn.scm")


(export  nil  nil?)
(include "src/nil.scm")


(export
 true  true?  false  false?
 host-boolean  boolean  not
 and  or)
(include "../shared/src/r5rs/boolean-logic.scm")


(export
 comment  let  do  if  if-not  when  when-not
 cond  condp  case  if-let)
(include "../shared/src/r5rs/flow-control.scm")


(export identical? = not=)
(include "../shared/src/r5rs/equality.scm")


(export
 +  -  /  *  inc  dec  quot  rem  mod
 <  <=  >  >=  max  min
 pos?  neg?  zero?  even?  odd?
 number?  integer?  float?
 rand  rand-int)
(include "../shared/src/r5rs/math.scm")




;; _eq-hash  _eqv-hash  _equal-hash  _string-hash
(include "src/hash.scm")
(export hash)
(include "../shared/src/r5rs/hash.scm")


(export list list? list*)
;; _list-nth
(include "../shared/src/r5rs/list.scm")


;; _vector-copy  _vector-finalize
(include "src/vector.scm")
(export vector vector? subvec)
;; _vector-assoc  _vector-get  _vector-index-of  _vector-last-index-of
(include "../shared/src/r5rs/vector.scm")


(export map? hash-map)
;; _hash-map?  _hash-map-get  _hash-map-count  _hash-map-keys
;; _hash-map-has-key?  _hash-map-map  _hash-map-assoc
(include "src/map.scm")
;; _hash-map=  _hash-map-seq  _hash-map-into
(include "../shared/src/r5rs/map.scm")


;; _make-hash-set  _hash-set?  _hash-set-table
(include "src/set.scm")
(export
 hash-set  set?  set  disj
 set/union  set/difference  set/intersection  set/select
 set/subset?  set/superset?)
;; _hash-set-print  _hash-set=  _hash-set-hash  _hash-set-count
;; _hash-set-keys  _hash-set-contains?  _hash-set-get  _hash-set-into
(include "../shared/src/r5rs/set.scm")


(export
 char?  string?  char  str  subs
 string/blank?
 string/capitalize  string/lower-case  string/upper-case
 string/index-of  string/last-index-of
 string/join  string/reverse
 string/starts-with?  .startsWith
 string/ends-with?  .endsWith
 string/trim  string/triml  string/trimr  string/trim-newline)
;; _string-get
(include "../shared/src/r5rs/string.scm")

(export
 string/replace  string/replace-first  string/re-quote-replacement)
(include "../shared/src/r5rs/string-replace.scm")


(export
 re-pattern  re-pattern?  re-matcher?)
;; _make-re-matcher  _re-matcher-pattern  _re-matcher-input
;; _re-matcher-start       set-_re-matcher-start!
;; _re-matcher-last-match  set-_re-matcher-last-match!
;; _re-match-ranges
;; _re-match-num-groups  _re-matcher-next-start
;; _re-find  _re-groups  _re-matches
(include "src/regexp.scm")

(export
 re-matcher  re-find  re-groups  re-matches  re-seq)
(include "../shared/src/r5rs/regexp.scm")


(export  gensym  symbol  symbol?  keyword  keyword?)
(include "../shared/src/r5rs/symbol.scm")


;; _clj-vseq  _clj-vseq?  _clj-vseq-v  _clj-vseq-i
;; _clj-sseq  _clj-sseq?  _clj-sseq-s  _clj-sseq-i
(include "src/seq-types.scm")
;; _clj-vseq-first  _clj-vseq-rest  _clj-vseq-next
;; _clj-sseq-first  _clj-sseq-rest  _clj-sseq-next
(include "../shared/src/r5rs/seq-types.scm")


(export
 coll?  associative?  counted?  sequential?  sorted?  reversible?
 assoc  count  distinct?  empty  empty?  every?  get
 not-any?  not-empty  not-every?  some
 contains?  .contains)
(include "../shared/src/r5rs/collections.scm")


(export
 seq  seq?  seqable?
 apply  conj  cons  ffirst  first  fnext  into  last
 map  next  nfirst  nnext  nth  peek  pop  reduce  rest
 index-of  .indexOf  last-index-of  .lastIndexOf)
(include "../shared/src/r5rs/sequences.scm")


(export with-in-str with-out-str)
(include "src/io.scm")

(export
 newline  print  println  print-str  println-str
 pr  prn  pr-str  prn-str)
(include "../shared/src/r5rs/io.scm")


(export
 test/*testing-contexts*  test/*testing-vars*
 test/deftest  test/testing  test/is
 test/run-all-tests  test/success?

 *_cloje-tests*
 *_cloje-test-tests-run*  *_cloje-test-passes*
 *_cloje-test-fails*  *_cloje-test-errors*
 _cloje-test-pass  _cloje-test-fail  _cloje-test-error)
(include "../shared/src/r5rs/test.scm")


(export *cloje-version* cloje-version)
(include "../shared/src/version.clj")

)
