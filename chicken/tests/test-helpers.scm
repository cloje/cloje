
;; Evaluates to true if the expr cause any error to be raised;
;; false if no error is raised.
(define-syntax test/error?
  (syntax-rules ()
    ((test/error? expr ...)
     (host/condition-case
      (scheme/begin expr ... false)
      (() true)))))

;; Evaluates to true if the expr cause an arity error to be raised;
;; false if no error is raised, or an error other than arity.
(define-syntax test/arity-error?
  (syntax-rules ()
    ((test/error? expr ...)
     (host/condition-case
      (scheme/begin expr ... false)
      ((arity) true)
      (() false)))))

;; Evaluates to true if the expr cause a bounds error to be raised;
;; false if no error is raised, or an error other than bonuds.
(define-syntax test/bounds-error?
  (syntax-rules ()
    ((test/error? expr ...)
     (host/condition-case
      (scheme/begin expr ... false)
      ((bounds) true)
      (() false)))))

;; Evaluates to true if the expr cause a syntax error to be raised;
;; false if no error is raised, or an error other than syntax.
(define-syntax test/syntax-error?
  (syntax-rules ()
    ((test/error? expr ...)
     (host/condition-case
      (scheme/begin expr ... false)
      ((syntax) true)
      (() false)))))

;; Evaluates to true if the expr cause an type error to be raised;
;; false if no error is raised, or an error other than type.
(define-syntax test/type-error?
  (syntax-rules ()
    ((test/error? expr ...)
     (host/condition-case
      (scheme/begin expr ... false)
      ((type) true)
      (() false)))))
