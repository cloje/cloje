
(deftest test-host-cond
  (is (test/syntax-error? (host-cond)))
  (is (nil? (host-cond (racket (/ 1 0)) (else nil))))
  (is (nil? (host-cond (java (/ 1 0)) (else nil))))

  (is (= 'something-else
         (host-cond
          (racket 'racket)
          (java 'java)
          (else 'something-else))))

  (is (true? (host-cond (chicken true))))
  (is (true? (host-cond (chicken true)
                        (racket (/ 1 0)))))
  (is (true? (host-cond (racket (/ 1 0))
                        (chicken true))))

  (is (nil? (host-cond (chicken) (scheme 'scheme))))

  (with-out-str
    (is (= 3 (host-cond (chicken (pr 1) (pr 2) 3)))))
  (is (= "12" (with-out-str
                (host-cond (chicken (pr 1) (pr 2) 3)))))

  (is (= 'chicken (host-cond (chicken 'chicken)
                             (scheme 'scheme))))
  (is (= 'scheme (host-cond (scheme 'scheme)
                            (chicken 'chicken))))

  (is (true? (host-cond ((or foo chicken) true))))

  (is (true? (host-cond ((and scheme chicken) true))))
  (is (nil? (host-cond ((and foo chicken) true) (else nil))))

  (is (true? (host-cond ((not foo) true))))

  (is (true? (host-cond ((or foo (and scheme (not racket))) true)))))
