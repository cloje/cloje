
(cond-expand
 (load-local-cloje
  (use posix)
  (change-directory "..")
  (load-relative "cloje.scm")
  (change-directory "tests"))
 (else
  (use cloje)))


(module chicken-cloje-tests ()

(import
 (prefix scheme scheme/)
 (prefix chicken host/)
 (only scheme
       define  define-syntax  syntax-rules
       import  export  require-library
       quote)
 (only chicken
       include  use)
 (prefix extras host/)
 (prefix ports host/))


(import (rename cloje
                (test/deftest deftest)
                (test/testing testing)
                (test/is is)))


(require-library srfi-1)
(import (prefix srfi-1 srfi-1/))

(require-library srfi-13)
(import (prefix srfi-13 srfi-13/))

(require-library srfi-69)
(import (prefix srfi-69 srfi-69/))


(include "test-helpers.scm")
(include "../../shared/tests/test-helpers.clj")


(include "host-cond-tests.clj")
(include "../../shared/tests/vars-tests.clj")
(include "../../shared/tests/fn-tests.clj")
(include "../../shared/tests/boolean-logic-tests.clj")
(include "../../shared/tests/flow-control-tests.clj")
(include "../../shared/tests/equality-tests.clj")
(include "../../shared/tests/math-tests.clj")
(include "../../shared/tests/hash-tests.clj")
(include "../../shared/tests/list-tests.clj")
(include "../../shared/tests/vector-tests.clj")
(include "../../shared/tests/map-tests.clj")
(include "../../shared/tests/set-tests.clj")
(include "../../shared/tests/string-tests.clj")
(include "../../shared/tests/string-replace-tests.clj")
(include "../../shared/tests/regexp-tests.clj")
(include "../../shared/tests/symbol-tests.clj")
(include "../../shared/tests/collections-tests.clj")
(include "../../shared/tests/sequences-tests.clj")
(include "../../shared/tests/io-tests.clj")
(include "../../shared/tests/version-tests.clj")


(if (test/success? (test/run-all-tests))
    (host/exit 0)
    (host/exit 1)))
